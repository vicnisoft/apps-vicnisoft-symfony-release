<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 7/15/16
 * Time: 4:27 PM
 */

namespace AppBundle\Models;


class Editor
{

    private $editor;

    /**
     * @return mixed
     */
    public function getEditor()
    {
        return $this->editor;
    }

    /**
     * @param mixed $editor
     */
    public function setEditor($editor)
    {
        $this->editor = $editor;
    }



}