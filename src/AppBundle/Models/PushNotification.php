<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 7/1/16
 * Time: 5:25 PM
 */

namespace AppBundle\Models;

use AppBundle\Entity\Projects;

class PushNotification
{

    /**
     * @var boolean
     *
     */

    private $deviceOnly = true;

    /**
     * @var Projects
     *
     */
    private $project;

    /**
     * @var String
     */
    private $deviceId;
    /**
     * @var String
     *
     */
    private $message;

    /**
     * @param $deviceId
     *
     */

    /**
     * @param $deviceOnly
     *
     */

    public function setDeviceOnly($deviceOnly){
        $this->deviceOnly = $deviceOnly;
    }

    /**
     * @return bool
     *
     */
    public function getDeviceOnly(){
        return $this->deviceOnly;
    }


    public function setDeviceId($deviceId){
        $this->deviceId = $deviceId;
    }

    /**
     * @return String
     *
     */
    public function getDeviceId(){
        return $this->deviceId;
    }

    /**
     * @param $message
     *
     */
    public function setMessage($message){
        $this->message = $message;
    }

    /**
     * @return String
     *
     */
    public function getMessage(){
        return $this->message;

    }


    /**
     * @param $project
     *
     */

    public function setProject($project){

        $this->project = $project;

    }

    /**
     * @return Projects
     *
     */

    public function getProject(){
        return $this->project;
    }

}