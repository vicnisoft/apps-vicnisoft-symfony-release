/**
 * Created by canngo on 5/31/16.
 */
/**
 * Created by canngo on 5/19/16.
 */

$(document).ready(function ($) {

    $('.btn-add-remove-api-key').click(function(){
        var inputElement = $('#edit_user_apiKey');
        if(inputElement){
            var apiKey = inputElement.val();
            if(apiKey){
                inputElement.val('');
                $(this).removeClass('btn-danger').addClass('btn-primary');
                $(this).children('span').first().removeClass('glyphicon-remove').addClass('glyphicon-plus');
            } else {
                var uuid = generateUUID(inputElement);
                inputElement.val(uuid);
                inputElement.removeAttr('disabled');
                $(this).removeClass('btn-primary').addClass('btn-danger');
                $(this).children('span').first().removeClass('glyphicon-plus').addClass('glyphicon-remove');
            }
        }
    });
});

function generateUUID(selector) {

    /*
    var d = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (d + Math.random()*16)%16 | 0;
        d = Math.floor(d/16);
        return (c=='x' ? r : (r&0x3|0x8)).toString(16);
    });
    return uuid;

    */

    $.ajax({
        type: 'GET',
        url: $(selector).attr('data'),
        data: $(selector).serialize(),
    })
        .done(function (data) {
            if(typeof data.form !== null){
                //$('#content-form-sample-size-calculator').html(data.form);
            }
            if(typeof data.result !== 'undefined') {
                //$('#sammple_size_calculator_sampleSize').val(data.result);
            }

        })
        .fail(function (jqXHR, textStatus, errorThrown) {
            if (typeof jqXHR.responseJSON !== 'undefined') {
                if(jqXHR.responseJSON.form !== null){
                    //$('#content-form-sample-size-calculator').html(jqXHR.responseJSON.form);
                }
                if(jqXHR.responseJSON.result !== null){
                    //$('#sammple_size_calculator_sampleSize').val(jqXHR.responseJSON.result);
                }
            }
        })
        .always(function () {
            //$('#sammple_size_calculator_submit').attr('disabled',false);
        });


};

