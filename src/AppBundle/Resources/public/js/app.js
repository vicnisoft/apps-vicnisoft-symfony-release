/**
 * Created by canngo on 6/2/16.
 */
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});


function setOrder(element) {
    var field = $(element).data("field");
    var order = $(element).data("order");
    $("#sortfield").val(field);
    $("#sortorder").val(order);
    $("#pageform").submit();
}

function setActive(element) {
    var active = ($(element).val() == 1) ? 0 : 1;
    $("#active").val(active);
    $("#pageform").submit();
}

function removeFilter(element) {
    $("#activefilter").val($(element).data("name"));
    if($(element).attr('data-value')){
        $("#activefiltervalue").val($(element).data("value"));
    }
    $("#pageform").submit();
}

function changeMultiple(element) {
    if($(element).val() === null) {
        var defaultValue = $(element).data("default");
        $(element).val(defaultValue);
    }
}

function setChildSelectFilters(element, children){
    var values = $(element).val();
    var childs = children.split(",");

    var valid = false;
    if(values !== null) {
        jQuery.each( values, function( i, val ) {
            if(val > 0){
                valid = true;
                return;
            }
        });
    }
    jQuery.each( childs, function( i, child) {
        var $child = $("#"+child);

        if($child.length){

            if($child.find('optgroup').length) {
                displayOptGroups(valid, $child, values);
            } else if($child.find('[data-parents]').length){
                displayOptions(valid, $child, values);
            }
        }
    });

}