<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 6/4/16
 * Time: 8:13 PM
 */

namespace AppBundle\Controller;


use AppBundle\Controller\Super\SuperEntityController;
use AppBundle\Entity\Projects;
use AppBundle\Form\EditProject;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ProjectController extends SuperEntityController
{

    /** @var  Projects  The project that is editing */
    protected $route = "manageProjects";
    private $project ;
    public function createAction(Request $request){

        $this->mode = SELF::MODE_CREATE;
        $this->project = new Projects();

        return $this->setAction($request);

    }

    public function editAction(Request $request, $id = null){

        if($id === null){
            throw new NotFoundHttpException('Project id is missing');
        }

        $this->project = $this->getDoctrine()->getRepository('AppBundle:Projects')->find($id);

        if($this->project === null){
            throw new NotFoundHttpException('This project id was not found');
        }

        $this->transformProjectIconStringToFile();
        $this->transformPemFileStringToFile();

        return $this->setAction($request);


    }

    public function uploadPhotosAction(Request $request){

        $data['page_title'] = 'Upload Photos';

        return $this->render('AppBundle:form:uploadPhotos.html.twig',
            array(
                'data' => $data,
            )
        );
    }

    /** Privates */

    private function setAction(Request $request){

        $data = $this->getData();
        $form = $this->renderPage($request, $data);

        if($form instanceof RedirectResponse){
            return $form;
        }

        return $this->render('AppBundle:form:editProject.html.twig',
            array(
                'form' => $form->createView(),
                'data' => $data,

            )
        );
    }


    private function renderPage(Request $request, $data){

        /** @Var EditProject */

        $form = $this->createForm(EditProject::class, $this->project, $this->getOptions());
        $form->handleRequest($request);

        if($form->get('cancel')->isClicked()){
            return $this->redirectToRoute($this->route);
        }

        if($form->isValid()){

            try {

                $fileLogo = $form['projectIcon']->getData();
                $this->transformProjectIconFileToString($fileLogo);

                $filePem = $form['pemUrlAspn']->getData();
                $this->transformPemFileToString($filePem);


                $this->save($request);

                if ($this->mode == SELF::MODE_CREATE) {
                    $this->container->get('session')->getFlashBag()->add(
                        'success',
                        "Project ". $this->project->getName()." created successfully."
                    );
                } else {
                    if ($this->mode == SELF::MODE_EDIT) {
                        $this->container->get('session')->getFlashBag()->add(
                            'success',
                            "Project ". $this->project->getName() ." updated successfully."
                        );
                    }
                }

                return $this->redirectToRoute($this->route);

            } catch (Exception $e) {
                $this->container->get('session')->getFlashBag()
                    ->add(
                        'error',
                        $e->getMessage()
                    );
                $data['error_messages'] = $e->getMessage();
            }
        }

        return $form;
    }

    /**
     * @return mixed
     *
     */

    private function getData(){

        $data['page_title'] = $this->mode == SELF::MODE_CREATE ? "Create Project" : "Edit Project";

        return $data;
    }

    /**
     * @return array
     *
     */
    private function getOptions() {
        $options = array(
        );

        return $options;
    }

    /**
     *
     *
     */

    private function transformProjectIconStringToFile() {
        if ($this->project->getProjectIcon() != '') {
            $dir = $this->container->getParameter('upload__images_dir');
            $path = $dir . $this->project->getProjectIcon();
            try {
                if(file_exists($path)){
                    $this->project->setProjectIcon(new File($path));
                } else {
                    $this->project->setProjectIcon('');

                }
            } catch (FileException $e) {
                $this->project->setProjectIcon('');
            }
        }
    }

    /**
     * @param File $logo
     *
     */

    private function transformProjectIconFileToString(File $logo = null) {

        if($logo){
            $extension = $logo->guessExtension();
            if (!$extension) {
                // extension cannot be guessed
                $extension = 'bin';
            }
            $dir = $this->getParameter('upload__images_dir');
            $fileName = $this->get('app.common')->generateFileName($this->project->getName(),'logo',$extension);
            $logo->move($dir, strtolower($fileName));

            $this->project->setProjectIcon(strtolower($fileName));
        }

    }


    /**
     *
     *
     */

    private function transformPemFileStringToFile() {
        if ($this->project->getPemUrlAspn() != '') {
            $dir = $this->container->getParameter('pem_dir');
            $path = $dir . $this->project->getPemUrlAspn();
            try {
                if(file_exists($path)){
                    $this->project->setPemUrlAspn(new File($path));
                } else {
                    $this->project->setPemUrlAspn('');

                }
            } catch (FileException $e) {
                $this->project->setPemUrlAspn('');
            }
        }
    }

    /**
     * @param File $logo
     *
     */

    private function transformPemFileToString(File $pemFile = null) {

        if($pemFile){

            $extension = 'pem';
            $dir = $this->getParameter('pem_dir');
            $fileName = $this->get('app.common')->generateFileName($this->project->getName(),'pem',$extension);
            $pemFile->move($dir, strtolower($fileName));

            $this->project->setPemUrlAspn($fileName);
        }

    }

    /**
     * @param Request $request
     * @throws \Exception
     */
    protected function save(Request $request) {

        $em = $this->getDoctrine()->getManager();
        try {
            $em->getConnection()->beginTransaction();

            if($this->mode === SELF::MODE_EDIT){

            } else {
                $this->project->setCreatedOn(time());
                $this->project->setRecycled(false);

            }

            $em->persist($this->project);
            $em->flush();
            $em->getConnection()->commit();

        } catch (Exception $e) {
            $em->getConnection()->rollback();
            throw $e;
        }
    }

}