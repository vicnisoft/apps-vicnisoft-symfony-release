<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 6/9/16
 * Time: 5:34 PM
 */

namespace AppBundle\Controller;

use AppBundle\Controller\Helpers\Queries\FBVideosQuery;
use AppBundle\Controller\Super\SuperEntityController;
use AppBundle\Entity\FbCategories;
use AppBundle\Entity\FbVideos;
use AppBundle\Form\EditFBCategory;
use AppBundle\Form\EditFBVideo;
use AppBundle\Service\Settings\SettingsManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Exception;

class FBVideoController extends SuperEntityController
{
    protected $route = "manageFbVideos";
    /** @var  FbVideos  The video that is editing */
    private $video ;

    public function createAction(Request $request){

        if (false === $this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            $this->redirectToRoute('login');
        }
        $this->redirectToRoute('login');


        $this->mode = SELF::MODE_CREATE;
        $this->video = new FbVideos();

        return $this->setAction($request);

    }

    /**
     * @param Request $request
     * @param null $id
     * @return \Symfony\Component\Form\Form|RedirectResponse|\Symfony\Component\HttpFoundation\Response
     *
     */

    public function editAction(Request $request, $id = null){

        if($id === null){
            throw new NotFoundHttpException('Video id is missing');
        }

        $this->video = $this->getDoctrine()->getRepository('AppBundle:FbVideos')->find($id);

        if($this->video === null){
            throw new NotFoundHttpException('This video id was not found');
        }

        return $this->setAction($request);


    }

    public function approveAction(Request $request, $id = null){

        if($id === null){
            throw new NotFoundHttpException('Video id is missing');
        }

        $video = $this->getDoctrine()->getRepository('AppBundle:FbVideos')->find($id);

        if($video === null){
            throw new NotFoundHttpException('This video id was not found');
        }

        $this->approveVideo($video);

        return $this->redirectToRoute($this->route);
    }

    /** Privates */

    private function setAction(Request $request){

        $data = $this->getData();
        $form = $this->renderPage($request, $data);

        if($form instanceof RedirectResponse){
            return $form;
        }

        return $this->render('AppBundle:form:editFBVideo.html.twig',
            array(
                'form' => $form->createView(),
                'data' => $data,

            )
        );
    }


    private function renderPage(Request $request, $data){

        /** @Var EditFBVideo */

        $form = $this->createForm(EditFBVideo::class, $this->video, $this->getOptions());
        $form->handleRequest($request);

        if($form->get('cancel')->isClicked()){
            return $this->redirectToRoute($this->route);
        }

        if($form->isValid()){

            try {
                $this->save($request);
                if ($this->mode == SELF::MODE_CREATE) {
                    $this->container->get('session')->getFlashBag()->add(
                        'success',
                        "Video ". $this->video->getName()." created successfully."
                    );
                } else {
                    if ($this->mode == SELF::MODE_EDIT) {
                        $this->container->get('session')->getFlashBag()->add(
                            'success',
                            "Video ". $this->video->getName() ." updated successfully."
                        );
                    }
                }

                return $this->redirectToRoute($this->route);

            } catch (Exception $e) {
                $this->container->get('session')->getFlashBag()
                    ->add(
                        'error',
                        $e->getMessage()
                    );
                $data['error_messages'] = $e->getMessage();
            }
        }

        return $form;
    }

    private function getData(){

        $data['page_title'] = "Create Football Video";

        return $data;
    }

    private function getOptions() {
        $categories = $this->getDoctrine()->getRepository('AppBundle:FbCategories')->findAll();
        $options = array(
            'categories' => $categories,
        );

        return $options;
    }


    /**
     * @param Request $request
     * @throws \Exception
     */
    protected function save(Request $request) {
        $em = $this->getDoctrine()->getManager();
        try {
            $em->getConnection()->beginTransaction();

            if($this->mode === SELF::MODE_EDIT){
                $this->video->setUpdatedOn(time());
                $this->video->setUpdatedBy($this->getUser());
            }else {
                $this->video->setCreatedBy($this->getUser());
                $this->video->setCreatedOn(time());
                $this->video->setActive(true);
            }

            $em->persist($this->video);
            $em->flush();
            $em->getConnection()->commit();

        } catch (Exception $e) {
            $em->getConnection()->rollback();
            throw $e;
        }
    }

    private function approveVideo(FbVideos $video){

        $em = $this->getDoctrine()->getManager();
        try {
            $em->getConnection()->beginTransaction();

            $video->setApproved(SettingsManager::APPROVAL_STATUS_APPROVED);
            $video->setUpdatedBy($this->getUser());
            $video->setUpdatedOn(time());

            $em->persist($video);
            $em->flush();
            $em->getConnection()->commit();

            $this->container->get('session')->getFlashBag()->add(
                'success',
                "Video ". $video->getName()." approved successfully."
            );

        } catch (Exception $e) {
            $em->getConnection()->rollback();

            $this->container->get('session')->getFlashBag()->add(
                'error',
                "Video ". $video->getName()." cannot approve."
            );
        }

    }

}