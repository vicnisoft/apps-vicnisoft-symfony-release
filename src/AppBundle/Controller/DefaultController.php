<?php

namespace AppBundle\Controller;

use AppBundle\Controller\Helpers\Queries\ApiUsersQuery;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $response = new JsonResponse();
        $response->setCharset("utf-8");
        $response->headers->set('Content-Type', 'application/json; charset=utf-8');


        $data = [
            'error' => "OK",
        ];

        $response->setData(
            $data
        );

        return $response;
    }

    /**
     * @Route("/api/v1/getUsers", name="get_users")
     */
    public function showAction(Request $request)
    {

        $response = new JsonResponse();
        $response->setCharset("utf-8");
        $response->headers->set('Content-Type', 'application/json; charset=utf-8');

        $apiUsersQuery = new ApiUsersQuery($this->getDoctrine());

        $users = $apiUsersQuery->buildQuery();


        $data = [
            'count' => count($users),
            'users' => $users,
        ];
//        $users = $this->getDoctrine()->getRepository('AppBundle:Users')->findAll();


        $response->setData(
            $data
        );

        return $response;
    }


}
