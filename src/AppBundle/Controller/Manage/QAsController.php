<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 8/8/16
 * Time: 2:23 PM
 */

namespace AppBundle\Controller\Manage;


use AppBundle\Controller\Helpers\Filters\QAFilters;
use AppBundle\Controller\Helpers\Queries\QAQuery;
use AppBundle\Controller\Helpers\Tools\UserInfos;
use AppBundle\Controller\Super\SuperManageController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Config\Definition\Exception\Exception;

class QAsController extends SuperManageController
{
    protected $route = 'manageQAs';
    protected $recycle_path = 'manageQAsRecycle';
    protected $create_path = 'createQA';
    protected $edit_path = 'editQA';
    protected $page_path = 'manageQAsPage';
    protected $dbname = 'questions_answers';


    public function showAction(Request $request){

        $this->init();
        $this->setFilters($request);
        $this->columns = $this->getColumns($request);
        return $this->renderPage($request);


    }

    protected function getDefaultColumns()
    {
        // TODO: Implement getDefaultColumns() method.
        return $this->get('settings')->getQAsDefaultColumns();
    }


    public function showListAction(Request $request){

        $this->init();
        return $this->renderPage($request);
    }

    public function renderPage(Request $request){

        $data = $this->getData($request);

        return $this->render('AppBundle:manage:questionsanswers.html.twig',
            array('data' => $data)
            );
    }

    protected function recycle($id, $active) {

        try{
            $em = $this->getDoctrine()->getManager();
            $qa = $this->getDoctrine()->getRepository('AppBundle:QuestionsAnswers')->find($id);
            $qa->setActive(!$active);

            $em->persist($qa);
            $em->flush();

        } catch(Exception $e){

            $recycled = ($active) ? 'recycle' : 'restore';
            $this->container->get('session')->getFlashBag()->add(
                'error',
                "Not able to ".$recycled." Q&A with id ".$id
            );

            return false;
        }

        $recycled = ($active) ? 'recycle' : 'restore';

        $this->container->get('session')->getFlashBag()->add(
            'success',
            "Q&A ".$qa->getId(). " ".$recycled." successfully"
        );

        return true;

    }

    private function getData(Request $request = null){

        $data['columns'] = $this->columns;
        $data['rows'] = $this->buildQuery();
        /** Filters */
        $data["filters"] = $this->filterManager->getFiltersBySections($this->filters);
        $data["useFilterSections"] = true;
        $data["activefilters"] = $this->active_filters;
        /** Paths */
        $data['create_path'] = $this->create_path;
        $data['edit_path'] = $this->edit_path;
        $data['page_path'] = $this->page_path;
        $data['show_list_path'] = 'showQAs';
        $data['active'] = true;
        // Recycle info
        $data["recyclepath"] = $this->recycle_path;
        $data["recyclename"] = 'id';
        $data["recycleinfo"] = 'question';
        $data["recycletype"] = 'Q&A';


        return $data;

    }

    protected function init(){
        $rights = array();
        /** @var QAFilters filterManager */
        $this->filterManager = new QAFilters($this->getDoctrine(), $rights, $this->get('settings'));
        $this->queryManager = new QAQuery($this->getDoctrine());
        $this->userInfos = new UserInfos($this->getUser());
    }

    protected function initFilters(){
        /** @var QAFilters filterManager */
        return $this->filterManager->setFilters($this->userInfos);
    }

    private function buildQuery(){
        /** @var QAQuery */
        return $this->queryManager->buildQuery($this->userInfos);

    }

}