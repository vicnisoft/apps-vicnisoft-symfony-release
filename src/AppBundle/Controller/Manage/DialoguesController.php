<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 6/5/16
 * Time: 5:48 PM
 */

namespace AppBundle\Controller\Manage;


use AppBundle\Controller\Helpers\Filters\DialoguesFilters;
use AppBundle\Controller\Helpers\Queries\DialoguesQuery;
use AppBundle\Controller\Helpers\Tools\UserInfos;
use AppBundle\Controller\Super\SuperManageController;
use Symfony\Component\HttpFoundation\Request;


class DialoguesController extends SuperManageController
{
    protected $recycle_path = 'manageDialoguesRecycle';
    protected $page_path = 'manageDialoguesPage';
    protected $route = 'manageDialogues';
    protected $create_path = 'createDialogue';
    private  $copy_path = "copyDialogue";
    protected $dbname = 'dialogues';


    public function showAction(Request $request){
        $this->init();
        $this->setFilters($request);
        $this->columns = $this->getColumns($request);
        return $this->renderPage($request);
    }

    protected function getDefaultColumns()
    {
        // TODO: Implement getDefaultColumns() method.
        return $this->get('settings')->getDialoguesDefaultColumns();
    }


    protected function recycle($id, $active) {

        try{
            $em = $this->getDoctrine()->getManager();
            $entity = $this->getDoctrine()->getRepository('AppBundle:Dialogues')->find($id);
            $entity->setActive(!$active);
            $entity->setUpdatedOn(time());
            $em->persist($entity);
            $em->flush();

        } catch(Exception $e){

            $recycled = ($active) ? 'recycle' : 'restore';
            $this->container->get('session')->getFlashBag()->add(
                'error',
                "Not able to ".$recycled." Dialogue with id ".$id
            );

            return false;
        }

        $recycled = ($active) ? 'recycle' : 'restore';

        $this->container->get('session')->getFlashBag()->add(
            'success',
            "Dialogue ".$entity->getName(). " ".$recycled." successfully"
        );


        return true;

    }

    protected function init()
    {
        $rights = array();
        $this->userInfos  = new UserInfos($this->getUser());
        $this->queryManager = new DialoguesQuery($this->getDoctrine());
        $this->filterManager = new DialoguesFilters($this->getDoctrine(), $rights, $this->get('settings'));
        // TODO: Implement init() method.
    }

    protected function renderPage(Request $request)
    {
        // TODO: Implement renderPage() method.
        return $this->render('AppBundle:manage:dialogues.html.twig', array(
            'data' => $this->getData(),
        ));
    }

    protected function initFilters()
    {
        // TODO: Implement initFilters() method.

        return $this->filterManager->setFilters($this->userInfos);

    }


    private function getData(){
        $data['page_title'] = 'Manage Dialogues';
        $data['columns'] = $this->columns;
        $data['rows'] = $this->buildQuery();
        $data['create_path'] = $this->create_path;
        $data['page_path'] = $this->page_path;
        $data['active'] = true;
        /** Filters */
        $data["filters"] = $this->filterManager->getFiltersBySections($this->filters);
        $data["useFilterSections"] = true;
        $data["activefilters"] = $this->active_filters;
        // Recycle info
        $data["recyclepath"] = $this->recycle_path;
        $data["recyclename"] = 'name';
        $data["recycleinfo"] = 'category_name';
        $data["recycletype"] = 'dialogue';
        $data["copy_path"] = $this->copy_path;

        return $data;
    }

    private function buildQuery(){

        return $this->queryManager->buildQuery($this->userInfos);
    }

}