<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 7/6/16
 * Time: 11:37 AM
 */

namespace AppBundle\Controller\Manage;


use AppBundle\Controller\Helpers\Filters\CategoriesFilters;
use AppBundle\Controller\Helpers\Tools\UserInfos;
use AppBundle\Controller\Super\SuperManageController;

use AppBundle\Controller\Helpers\Queries\CategoriesQuery;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;

class CategoriesController extends SuperManageController
{
    protected $recycle_path = 'manageCategoriesRecycle';
    protected $page_path = 'manageCategoriesPage';
    protected $route = 'manageCategories';
    protected $create_path = 'createCategory';
    protected $dbname = 'categories';

    public function showAction(Request $request){

        $this->init();
        $this->setFilters($request);
        $this->columns = $this->getColumns($request);
        return $this->renderPage($request);
    }

    protected function getDefaultColumns()
    {
        // TODO: Implement getDefaultColumns() method.
        return $this->get('settings')->getCategoriesDefaultColumns();
    }


    protected function recycle($id, $active) {

        try{
            $em = $this->getDoctrine()->getManager();
            $category = $this->getDoctrine()->getRepository('AppBundle:Categories')->find($id);
            $category->setActive(!$active);
            $category->setUpdatedOn(time());
            $em->persist($category);
            $em->flush();

        } catch(Exception $e){

            $recycled = ($active) ? 'recycle' : 'restore';
            $this->container->get('session')->getFlashBag()->add(
                'error',
                "Not able to ".$recycled." Category with id ".$id
            );

            return false;
        }

        $recycled = ($active) ? 'recycle' : 'restore';

        $this->container->get('session')->getFlashBag()->add(
            'success',
            "Category ".$category->getName(). " ".$recycled." successfully"
        );


        return true;

    }



    private function getData(){

        $data['columns'] = $this->columns;
        $data['rows'] = $this->buildQuery();
        $data['create_path'] = $this->create_path;
        $data['page_path'] = $this->page_path;
        $data['active'] = true;
        /** Filters */
        $data["filters"] = $this->filterManager->getFiltersBySections($this->filters);
        $data["useFilterSections"] = true;
        $data["activefilters"] = $this->active_filters;
        // Recycle info
        $data["recyclepath"] = $this->recycle_path;
        $data["recyclename"] = 'name';
        $data["recycleinfo"] = 'project_name';
        $data["recycletype"] = 'category';


        return $data;

    }

    protected function init()
    {
        // TODO: Implement init() method.
        $rights = array();
        $this->userInfos = new UserInfos($this->getUser());
        $this->queryManager = new CategoriesQuery($this->getDoctrine());
        $this->filterManager = new CategoriesFilters($this->getDoctrine(), $rights, $this->get('settings'));

    }

    protected function renderPage(Request $request)
    {
        // TODO: Implement renderPage() method.
        return $this->render('AppBundle:manage:categories.html.twig',
            array('data' => $this->getData()
            )
        );
    }

    protected function initFilters()
    {
        // TODO: Implement initFilters() method.
        return $this->filterManager->setFilters($this->userInfos);

    }


    private function buildQuery(){

        return $this->queryManager->buildQuery($this->userInfos);
    }

}