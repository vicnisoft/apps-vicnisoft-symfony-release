<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 7/1/16
 * Time: 9:52 AM
 */

namespace AppBundle\Controller\Manage;

use AppBundle\Controller\Helpers\Filters\DeviceFilters;
use AppBundle\Controller\Helpers\Queries\DevicesQuery;
use AppBundle\Controller\Super\SuperManageController;
use AppBundle\Entity\Devices;
use Symfony\Component\HttpFoundation\Request;
use Exception;
use AppBundle\Controller\Helpers\Tools\UserInfos;

class DevicesController extends SuperManageController
{
    protected $recycle_path = 'manageDevicesRecycle';
    protected $route = 'manageDevices';
    protected $create_path = 'createDevice';
    protected $page_path = 'manageDevicesPage';

    protected $dbname = 'devices';

    public function showAction(Request $request){

        $this->init();
        $this->setFilters($request);
        $this->columns = $this->getColumns($request);
        return $this->renderPage($request);
    }

    protected function recycle($id, $active) {

        try{
            $em = $this->getDoctrine()->getManager();
            $device = $this->getDoctrine()->getRepository('AppBundle:Devices')->find($id);
//            $device->setActive(!$active);

            $em->persist($device);
            $em->flush();

        } catch(Exception $e){

            $recycled = ($active) ? 'recycle' : 'restore';
            $this->container->get('session')->getFlashBag()->add(
                'error',
                "Not able to ".$recycled." Device with id ".$id
            );

            return false;
        }

        $recycled = ($active) ? 'recycle' : 'restore';

        $this->container->get('session')->getFlashBag()->add(
            'success',
            "Device ".$device->getDeviceId(). " ".$recycled." successfully"
        );


        return true;

    }

    private function getData(){

        $data['columns'] = $this->columns;
        $data['rows'] = $this->buildQuery();
        $data['create_path'] = $this->create_path;
        $data['page_path'] = $this->page_path;
        $data['active'] = true;
        /** Filters */
        $data["filters"] = $this->filterManager->getFiltersBySections($this->filters);
        $data["useFilterSections"] = true;
        $data["activefilters"] = $this->active_filters;
        // Recycle info
        $data["recyclepath"] = $this->recycle_path;

        $data["recyclename"] = 'project_name';
        $data["recycleinfo"] = 'username';
        $data["recycletype"] = 'device';

        $data['openpanel'] = $this->openpanel;
        $data["totalcolumns"] = $this->getTotalFirstLineColumns($this->columns);

        return $data;

    }

    protected function renderPage(Request $request)
    {
        // TODO: Implement renderPage() method.
        return $this->render('AppBundle:manage:devices.html.twig',
            array('data' => $this->getData()
            )
        );
    }


    protected function init(){
        $rights = array();
        /** @var DeviceFilters filterManager */
        $this->filterManager = new DeviceFilters($this->getDoctrine(), $rights, $this->get('settings'));
        $this->queryManager = new DevicesQuery($this->getDoctrine());
        $this->userInfos = new UserInfos($this->getUser());
    }

    protected function initFilters(){
        /** @var QAFilters filterManager */
        return $this->filterManager->setFilters($this->userInfos);
    }

    protected function getDefaultColumns()
    {
        // TODO: Implement getDefaultColumns() method.
        return $this->get('settings')->getDevicesDefaultColumns();

    }


    private function buildQuery(){
        $result =  $this->queryManager->buildQuery($this->userInfos);
        $groupResult = array();
        foreach($result as $row) {
            if(!isset($groupResult[$row['id']])){
                $groupResult[$row['id']] = array();
            }
            $groupResult[$row['id']][] = $row;
        }

        return $groupResult;
    }

}