<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 6/9/16
 * Time: 4:57 PM
 */

namespace AppBundle\Controller\Manage;


use AppBundle\Controller\Helpers\Queries\FBCategoriesQuery;
use AppBundle\Controller\Super\SuperManageController;

use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;

class FBCategoriesController extends SuperManageController
{

    protected $recycle_path = 'manageFbCategoriesRecycle';
    protected $route = 'manageFbCategories';
    protected $create_path = 'createFbCategory';

    public function showAction(Request $request){

        $this->init();
        return $this->render('AppBundle:manage:fbcategories.html.twig',
            array('data' => $this->getData()
            )
        );
    }

    protected function getDefaultColumns()
    {
        // TODO: Implement getDefaultColumns() method.
    }


    protected function recycle($id, $active) {

        try{
            $em = $this->getDoctrine()->getManager();
            $category = $this->getDoctrine()->getRepository('AppBundle:FbCategories')->find($id);
            $category->setActive(!$active);

            $em->persist($category);
            $em->flush();

        } catch(Exception $e){

            $recycled = ($active) ? 'recycle' : 'restore';
            $this->container->get('session')->getFlashBag()->add(
                'error',
                "Not able to ".$recycled." Category with id ".$id
            );

            return false;
        }

        $recycled = ($active) ? 'recycle' : 'restore';

        $this->container->get('session')->getFlashBag()->add(
            'success',
            "Category ".$category->getName(). " ".$recycled." successfully"
        );


        return true;

    }

    private function getData(){
        $data['columns'] = $this->get('settings')->getFBCategoriesDefaultColumns();
        $data['rows'] = $this->buildQuery();
        $data['create_path'] = $this->create_path;
        $data['active'] = true;
        // Recycle info
        $data["recyclepath"] = $this->recycle_path;
        $data["recyclename"] = 'name';
        $data["recycleinfo"] = 'description';
        $data["recycletype"] = 'fbcategory';


        return $data;

    }

    protected function init()
    {
        // TODO: Implement init() method.
    }

    protected function renderPage(Request $request)
    {
        // TODO: Implement renderPage() method.
    }

    protected function initFilters()
    {
        // TODO: Implement initFilters() method.
    }


    private function buildQuery(){
        $queryManager = new FBCategoriesQuery($this->getDoctrine());
        return $queryManager->buildQuery();
    }



}