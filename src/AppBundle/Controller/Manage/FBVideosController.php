<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 6/9/16
 * Time: 5:23 PM
 */

namespace AppBundle\Controller\Manage;



use AppBundle\Controller\Helpers\Queries\FBCategoriesQuery;
use AppBundle\Controller\Helpers\Queries\FBVideosQuery;
use AppBundle\Controller\Super\SuperManageController;

use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;

class FBVideosController extends SuperManageController
{

    protected $recycle_path = 'manageFbVideosRecycle';
    protected $route = 'manageFbVideos';
    protected $create_path = 'createFbVideo';

    public function showAction(Request $request){

        $this->init();
        return $this->render('AppBundle:manage:fbvideos.html.twig',
            array('data' => $this->getData()
            )
        );
    }

    protected function getDefaultColumns()
    {
        // TODO: Implement getDefaultColumns() method.
    }


    protected function recycle($id, $active) {

        try{
            $em = $this->getDoctrine()->getManager();
            $video = $this->getDoctrine()->getRepository('AppBundle:FbVideos')->find($id);
            $video->setActive(!$active);

            $em->persist($video);
            $em->flush();

        } catch(Exception $e){

            $recycled = ($active) ? 'recycle' : 'restore';
            $this->container->get('session')->getFlashBag()->add(
                'error',
                "Not able to ".$recycled." Video with id ".$id
            );

            return false;
        }

        $recycled = ($active) ? 'recycle' : 'restore';

        $this->container->get('session')->getFlashBag()->add(
            'success',
            "Video ".$video->getName(). " ".$recycled." successfully"
        );


        return true;

    }

    private function getData(){
        $data['columns'] = $this->get('settings')->getFBVideosDefaultColumns();
        $data['rows'] = $this->buildQuery();
        $data['create_path'] = $this->create_path;
        $data['active'] = true;
        // Recycle info
        $data["recyclepath"] = $this->recycle_path;
        $data["recyclename"] = 'name';
        $data["recycleinfo"] = 'short_description';
        $data["recycletype"] = 'fbvideo';


        return $data;

    }

    protected function init()
    {
        // TODO: Implement init() method.
    }

    protected function renderPage(Request $request)
    {
        // TODO: Implement renderPage() method.
    }

    protected function initFilters()
    {
        // TODO: Implement initFilters() method.
    }


    private function buildQuery(){
        $queryManager = new FBVideosQuery($this->getDoctrine());
        return $queryManager->buildQuery();
    }



}