<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 8/18/16
 * Time: 3:53 PM
 */

namespace AppBundle\Controller\Manage;


use AppBundle\Controller\Helpers\Filters\RequestsLogFilter;
use AppBundle\Controller\Helpers\Queries\RequestsLogQuery;
use AppBundle\Controller\Helpers\Tools\UserInfos;
use AppBundle\Controller\Super\SuperManageController;
use Symfony\Component\HttpFoundation\Request;

class RequestsLogsController extends SuperManageController
{

    protected $page_path = 'manageLogsPage';
    protected $route = 'manageLogs';
    protected $dbname = 'requests';

    public function showAction(Request $request){
        $this->denyAccessUnlessGranted('ROLE_CAN_ACCESS_ANALYTICS');
        $this->init();
        $this->setFilters($request);
        $this->columns = $this->getColumns($request);
        return $this->renderPage($request);
    }

    protected function getDefaultColumns()
    {
        // TODO: Implement getDefaultColumns() method.
        return $this->get('settings')->getLogsDefaultColumns();
    }


    protected function init()
    {
        // TODO: Implement init() method.
        $rights = array();
        $this->userInfos  = new UserInfos($this->getUser());
        $this->queryManager = new RequestsLogQuery($this->getDoctrine());
        $this->filterManager = new RequestsLogFilter($this->getDoctrine(), $rights, $this->get('settings'));

    }

    protected function renderPage(Request $request)
    {
        // TODO: Implement renderPage() method.
        return $this->render('AppBundle:manage:logs.html.twig', array(
            'data' => $this->getData(),
        ));
    }

    protected function initFilters()
    {
        // TODO: Implement initFilters() method.
        return $this->filterManager->setFilters($this->userInfos);

    }


    private function getData(){
        $data['page_title'] = 'Manage Logs';
        $columns = $this->columns;
        $data['columns'] = $columns;
        $data['rows'] = $this->buildQuery();
        $data['page_path'] = $this->page_path;
        $data['active'] = true;
        /** Filters */
        $data["filters"] = $this->filterManager->getFiltersBySections($this->filters);
        $data["useFilterSections"] = true;
        $data["activefilters"] = $this->active_filters;

        $data['openpanel'] = $this->openpanel;
        $data["totalcolumns"] = $this->getTotalFirstLineColumns($this->columns);

        return $data;
    }

    private function buildQuery(){

        $result = $this->queryManager->buildQuery($this->userInfos);

        $groupResult = array();
        foreach($result as $row) {
            if(!isset($groupResult[$row['user_id']])){
                $groupResult[$row['user_id']] = array();
            }
            $groupResult[$row['user_id']][] = $row;

        }

        return $groupResult;
    }


}