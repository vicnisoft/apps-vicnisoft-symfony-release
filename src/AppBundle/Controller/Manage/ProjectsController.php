<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 6/4/16
 * Time: 7:49 PM
 */

namespace AppBundle\Controller\Manage;


use AppBundle\Controller\Helpers\Filters\ProjectsFilters;
use AppBundle\Controller\Helpers\Queries\ProjectsQuery;
use AppBundle\Controller\Helpers\Tools\UserInfos;
use AppBundle\Controller\Super\SuperManageController;
use Symfony\Component\HttpFoundation\Request;

class ProjectsController extends SuperManageController
{
    protected $route = 'manageProjects';
    protected $create_path = 'createProject';
    protected $page_path = 'manageProjectsPage';
    protected $recycle_path = 'manageProjectsRecycle';
    protected $dbname = 'projects';

    public function showAction(Request $request){

        $this->init();

        $this->columns = $this->getColumns($request);
        return $this->renderPage($request);

    }

    protected function getDefaultColumns()
    {
        // TODO: Implement getDefaultColumns() method.
        return $this->get('settings')->getProjectsDefaultColumns();
    }


    private function getData(){

        $data['page_title'] = 'Manage Projects';
        $data['columns'] = $this->columns;
        $data['rows'] = $this->buildQuery();
        $data['create_path'] = $this->create_path;
        $data['page_path'] = $this->page_path;
        $data['recycle_path'] = $this->recycle_path;
        /** Filters */
        $data["filters"] = $this->filterManager->getFiltersBySections($this->filters);
        $data["useFilterSections"] = true;
        $data["activefilters"] = $this->active_filters;


        return $data;
    }

    protected function init()
    {
        // TODO: Implement init() method.
        $rights = array();
        $this->filterManager = new ProjectsFilters($this->getDoctrine(), $rights, $this->get('settings'));
        $this->userInfos = new UserInfos($this->getUser());
        $this->queryManager = new ProjectsQuery($this->getDoctrine());
    }

    protected function renderPage(Request $request)
    {
        // TODO: Implement renderPage() method.
        return $this->render('AppBundle:manage:projects.html.twig', array(
            'data' => $this->getData(),
        ));
    }

    protected function initFilters()
    {
        // TODO: Implement initFilters() method.
        return $this->filterManager->setFilters($this->userInfos);

    }


    private function buildQuery(){
        return $this->queryManager->buildQuery($this->userInfos);
    }
}