<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 6/3/16
 * Time: 12:47 AM
 */

namespace AppBundle\Controller\Manage;


use AppBundle\Controller\Helpers\Filters\UsersFilters;
use AppBundle\Controller\Helpers\Queries\UsersQuery;
use AppBundle\Controller\Helpers\Tools\UserInfos;
use AppBundle\Controller\Super\SuperManageController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;

class UsersController extends SuperManageController
{

    protected $recycle_path = 'manageUsersRecycle';
    protected $route = 'manageUsers';
    protected $create_path = 'createUser';
    protected $page_path = 'manageUsersPage';
    protected $dbname = 'users';

    public function showAction(Request $request){

        $this->denyAccessUnlessGranted('ROLE_CAN_ACCESS_USERS');
        $this->init();
        $this->columns = $this->getColumns($request);
        return $this->renderPage($request);
    }

    protected function getDefaultColumns()
    {
        // TODO: Implement getDefaultColumns() method.
        return $this->get('settings')->getUsersDefaultColumns();
    }


    protected function recycle($id, $active) {

        $this->denyAccessUnlessGranted('ROLE_CAN_ACCESS_USERS');

        try{
            $em = $this->getDoctrine()->getManager();
            $user = $this->getDoctrine()->getRepository('AppBundle:Users')->find($id);
            $user->setActive(!$active);

            $em->persist($user);
            $em->flush();

        } catch(Exception $e){

            $recycled = ($active) ? 'recycle' : 'restore';
            $this->container->get('session')->getFlashBag()->add(
                'error',
                "Not able to ".$recycled." User with id ".$id
                );

            return false;
        }

        $recycled = ($active) ? 'recycle' : 'restore';

        $this->container->get('session')->getFlashBag()->add(
            'success',
            "User ".$user->getUsername(). " ".$recycled." successfully"
            );


        return true;

    }

    private function getData(){
        $data['columns'] = $this->columns;
        $data['rows'] = $this->buildQuery();
        $data['create_path'] = $this->create_path;
        $data['page_path'] = $this->page_path;
        $data['active'] = true;
        /** Filters */
        $data["filters"] = $this->filterManager->getFiltersBySections($this->filters);
        $data["useFilterSections"] = true;
        $data["activefilters"] = $this->active_filters;

        // Recycle info
        $data["recyclepath"] = $this->recycle_path;
        $data["recyclename"] = 'username';
        $data["recycleinfo"] = 'group_name';
        $data["recycletype"] = 'user';


        return $data;

    }

    protected function init()
    {
        // TODO: Implement init() method.
        $rights = array();

        $this->filterManager = new UsersFilters($this->getDoctrine(), $rights, $this->get('settings'));
        $this->queryManager = new UsersQuery($this->getDoctrine());
        $this->userInfos = new UserInfos($this->getUser());

    }

    protected function renderPage(Request $request)
    {
        // TODO: Implement renderPage() method.
        return $this->render('AppBundle:manage:users.html.twig',
            array('data' => $this->getData()
            )
        );
    }

    protected function initFilters()
    {
        // TODO: Implement initFilters() method.
        return $this->filterManager->setFilters($this->userInfos);

    }



    private function buildQuery(){
        $queryManager = new UsersQuery($this->getDoctrine());
        return $queryManager->buildQuery($this->userInfos);
    }



}