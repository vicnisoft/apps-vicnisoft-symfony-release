<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 8/8/16
 * Time: 1:52 PM
 */

namespace AppBundle\Controller;


use AppBundle\Controller\Super\SuperEntityController;
use AppBundle\Form\EditQA;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\QuestionsAnswers;
use AppBundle\Entity\Users;
use Exception;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class QAController extends SuperEntityController
{
    /** @var  QuestionsAnswers */
    private $questionAnswer;

    protected $route = 'manageQAs';

    public function createAction(Request $request){

        $this->mode = SELF::MODE_CREATE;
        $this->questionAnswer = new QuestionsAnswers();
        return $this->setAction($request);

    }

    public function editAction(Request $request, $id = null){

        if($id === null){
            throw new NotFoundHttpException('This Q&A id is missing');
        }

        $this->questionAnswer = $this->getDoctrine()->getRepository('AppBundle:QuestionsAnswers')->find($id);
        if($this->questionAnswer === null){
            throw new  NotFoundHttpException('This Q&A id was not found');
        }
        $this->mode = SELF::MODE_EDIT;

        return $this->setAction($request);

    }

    private function setAction(Request $request){
        $data = $this->getData();
        $form = $this->renderPage($request, $data);

        if($form instanceof RedirectResponse){
            return $form;
        }

        return $this->render('AppBundle:form:editQA.html.twig',
            array(
                'form' => $form->createView(),
                'data' => $data,
            )
        );
    }

    private function renderPage(Request $request, $data){

        /** @Var EditQA */

        $form = $this->createForm(EditQA::class, $this->questionAnswer, $this->getOptions());
        $form->handleRequest($request);

        if($form->get('cancel')->isClicked()){
            return $this->redirectToRoute($this->route);
        }

        if($form->isValid()){

            try {
                $this->save($request);
                if ($this->mode == SELF::MODE_CREATE) {
                    $this->container->get('session')->getFlashBag()->add(
                        'success',
                        "Q&A ". $this->questionAnswer->getId()." has been created successfully."
                    );
                } else {
                    if ($this->mode == SELF::MODE_EDIT) {
                        $this->container->get('session')->getFlashBag()->add(
                            'success',
                            "Q&A ". $this->questionAnswer->getId() ." has been updated successfully."
                        );
                    }
                }

                return $this->redirectToRoute($this->route);

            } catch (Exception $e) {
                $this->container->get('session')->getFlashBag()
                    ->add(
                        'error',
                        $e->getMessage()
                    );
                $data['error_messages'] = $e->getMessage();
            }
        }

        return $form;
    }

    private function getData(){

        $data['page_title'] = $this->mode == SELF::MODE_CREATE ? "Create Q&A" : "Edit Q&A";
        return $data;
    }

    private function getOptions() {

        $options = array();
        /** @var Users $user */
        $user = $this->getUser();
        try{
            $options['project'] = $user->getProject();
        } catch (Exception $e){
            $options['project'] = null;
        }
        return $options;
    }


    protected function save(Request $request) {


        $em = $this->getDoctrine()->getManager();
        try {
            $em->getConnection()->beginTransaction();

            if($this->mode === SELF::MODE_EDIT){
                $this->questionAnswer->setUpdatedOn(time());
                if($this->getUser()->getProject()){
                    $this->questionAnswer->setProject($this->getUser()->getProject());
                }

            } else {
                if($this->getUser()->getProject()){
                    $this->questionAnswer->setProject($this->getUser()->getProject());
                }
                $this->questionAnswer->setCreatedOn(time());
                $this->questionAnswer->setUpdatedOn(time());
                $this->questionAnswer->setActive(true);
                $this->questionAnswer->setCreatedBy($this->getUser());
            }

            $em->persist($this->questionAnswer);
            $em->flush();
            $em->getConnection()->commit();
            if ($this->mode == SELF::MODE_CREATE) {
            }
        } catch (Exception $e) {
            $em->getConnection()->rollback();
            throw $e;
        }

    }

}