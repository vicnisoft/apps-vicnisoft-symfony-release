<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 5/26/16
 * Time: 9:53 PM
 */

namespace AppBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class SecurityController extends Controller
{

    /**
     * @param Request $request
     * @Route("login", name="login")
     */
    public function loginAction(Request $request){

        $helper = $this->get('security.authentication_utils');

        return $this->render(
            'security/login.html.twig',
            [
                'last_username' => $helper->getLastUsername(),
                'error' => $helper->getLastAuthenticationError(),
            ]
        );
    }

}