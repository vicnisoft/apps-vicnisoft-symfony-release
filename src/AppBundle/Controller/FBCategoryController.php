<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 6/9/16
 * Time: 4:41 PM
 */

namespace AppBundle\Controller;

use AppBundle\Controller\Super\SuperEntityController;
use AppBundle\Entity\FbCategories;
use AppBundle\Form\EditFBCategory;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Exception;

class FBCategoryController extends  SuperEntityController
{
    protected $route = "manageFbCategories";
    /** @var  FbCategories  The category that is editing */
    private $category ;

    public function createAction(Request $request){

        $this->mode = SELF::MODE_CREATE;
        $this->category = new FbCategories();
        $this->redirectToRoute('login');

        return $this->setAction($request);

    }

    public function editAction(Request $request, $id = null){

        if($id === null){
            throw new NotFoundHttpException('Category id is missing');
        }

        $this->category = $this->getDoctrine()->getRepository('AppBundle:FbCategories')->find($id);

        if($this->category === null){
            throw new NotFoundHttpException('This category id was not found');
        }

        return $this->setAction($request);


    }

    /** Privates */

    private function setAction(Request $request){

        $data = $this->getData();
        $form = $this->renderPage($request, $data);

        if($form instanceof RedirectResponse){
            return $form;
        }

        return $this->render('AppBundle:form:editFBCategory.html.twig',
            array(
                'form' => $form->createView(),
                'data' => $data,

            )
        );
    }


    private function renderPage(Request $request, $data){

        /** @Var EditFBCategory */

        $form = $this->createForm(EditFBCategory::class, $this->category, $this->getOptions());
        $form->handleRequest($request);

        if($form->get('cancel')->isClicked()){
            return $this->redirectToRoute($this->route);
        }

        if($form->isValid()){

            try {
                $this->save($request);
                if ($this->mode == SELF::MODE_CREATE) {
                    $this->container->get('session')->getFlashBag()->add(
                        'success',
                        "Category ". $this->category->getName()." created successfully."
                    );
                } else {
                    if ($this->mode == SELF::MODE_EDIT) {
                        $this->container->get('session')->getFlashBag()->add(
                            'success',
                            "Category ". $this->category->getName() ." updated successfully."
                        );
                    }
                }

                return $this->redirectToRoute($this->route);

            } catch (Exception $e) {
                $this->container->get('session')->getFlashBag()
                    ->add(
                        'error',
                        $e->getMessage()
                    );
                $data['error_messages'] = $e->getMessage();
            }
        }

        return $form;
    }

    private function getData(){

        $data['page_title'] = "Create Football Category";

        return $data;
    }

    private function getOptions() {
        $options = array(
        );

        return $options;
    }


    /**
     * @param Request $request
     * @throws \Exception
     */
    protected function save(Request $request) {
        $em = $this->getDoctrine()->getManager();
        try {
            $em->getConnection()->beginTransaction();
            if($this->mode == SELF::MODE_EDIT){
                $this->category->setUpdatedOn(time());
            } else {
                $this->category->setCreatedOn(time());
                $this->category->setActive(true);
            }
            $em->persist($this->category);
            $em->flush();
            $em->getConnection()->commit();

        } catch (Exception $e) {
            $em->getConnection()->rollback();
            throw $e;
        }
    }

}