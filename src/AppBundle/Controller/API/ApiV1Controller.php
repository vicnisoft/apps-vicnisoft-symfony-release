<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 5/26/16
 * Time: 6:49 PM
 */

namespace AppBundle\Controller\API;

use AppBundle\Controller\Helpers\Queries\ApiV1Query;
use AppBundle\Entity\Devices;
use AppBundle\Entity\DialoguesReports;
use AppBundle\Entity\FbVideos;
use AppBundle\Entity\FfDialogues;
use AppBundle\Entity\Users;
use AppBundle\Entity\UsersDevices;
use AppBundle\Service\Settings\SettingsManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Controller\Helpers\Queries\ApiUsersQuery;
use Exception;

class ApiV1Controller extends Controller
{

    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("login")
     *
     */

    public function login(Request $request){
        $username = $request->get('username');
        $password = $request->get('password');

        $user = $this->getDoctrine()->getRepository('AppBundle:Users')->findOneBy(array(
            'username' => $username,
        ));

        if($user) {
            $data['ApiKey'] = $user->getApiKey();
            return  $this->setResponse(null, $data, true);
        }

        return $this->setResponse('Username or Password is incorrect');

    }
    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("questionsAnswers")
     */

    public function getQuestionsAnswers(Request $request){

        $updatedOn = $request->get('updated_on');
        $createdOn = $request->get('created_on');
        $projectName = $request->get('project_name');

        if(is_null($projectName)){
            return $this->setResponse('project_name is required');
        }

        $queryManager = new ApiV1Query($this->getDoctrine());
        $result = $queryManager->getQuestionsAnswers($projectName,true, $updatedOn, $createdOn);
        $data = [
            "Count" => count($result),
            'QuestionsAnswers' => $result,
        ];
        return  $this->setResponse(null,$data, true);

    }


    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("reportDialogue")
     */

    public function reportDialogues(Request $request){

        $dialogueId = $request->get('dialogue_id');
        $reason = $request->get('reason');

        if(is_null($dialogueId)){
            return $this->setResponse('DialogueId is missing');
        }

        $dialogue = $this->getDoctrine()->getRepository('AppBundle:Dialogues')->find($dialogueId);

        if(is_null($dialogue)) {
            return $this->setResponse('Dialogue is missing');
        }

        $em = $this->getDoctrine()->getManager();

        $data = array();
        try {
            $em->getConnection()->beginTransaction();

            $dialogueReport = new DialoguesReports();
            $dialogueReport->setDialogue($dialogue);
            $dialogueReport->setUser($this->getUser());
            $dialogueReport->setReason($reason);

            $em->persist($dialogueReport);
            $em->flush();
            $em->getConnection()->commit();

            $data['DialogueReportId'] = $dialogueReport->getId();

        } catch(Exception $e){

            $em->getConnection()->rollback();
            return $this->setResponse('Error when saving report');

        }


        return  $this->setResponse(null,$data, true);

    }




    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("dialoguesByCategory")
     */

    public function getDialoguesByCategoryAction(Request $request){

        $updatedOn = $request->get('updated_on');
        $createdOn = $request->get('created_on');
        $projectName = $request->get('project_name');

        if(is_null($projectName)){
            return $this->setResponse('project_name is required');
        }

        $queryManager = new ApiV1Query($this->getDoctrine());
        $result = $queryManager->getDialoguesGroupByCategory($projectName,true, $updatedOn, $createdOn);
        $data = [
            "Count" => count($result),
            'Categories' => $result,
        ];
        return  $this->setResponse(null,$data, true);

    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("categories")
     */

    public function getCategoriesAction(Request $request){

        $updatedOn = $request->get('updated_on');
        $createdOn = $request->get('created_on');
        $projectName = $request->get('project_name');

        if(is_null($projectName)){
            return $this->setResponse('project_name is required');
        }

        $queryManager = new ApiV1Query($this->getDoctrine());
        $result = $queryManager->getCategories($projectName,true, $updatedOn, $createdOn);
        $data = [
            "Count" => count($result),
            'Categories' => $result,
        ];
        return  $this->setResponse(null,$data, true);

    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     * @Route("dialogues")
     */

    public function getDialoguesAction(Request $request){

        $updatedOn = $request->get('updated_on');
        $createdOn = $request->get('created_on');
        $projectName = $request->get('project_name');

        if(is_null($projectName)){
            return $this->setResponse('project_name is required');
        }

        $queryManager = new ApiV1Query($this->getDoctrine());
        $result = $queryManager->getDialogues($projectName,true, $updatedOn, $createdOn);
        $data = [
            "Count" => count($result),
            'Dialogues' => $result,
        ];
        return  $this->setResponse(null,$data, true);

    }


    /**
     * @param Request $request
     * @Route("users")
     */

    public function getUsersAction(Request $request){

        $response = new JsonResponse();
        $response->setCharset("utf-8");
        $response->headers->set('Content-Type', 'application/json; charset=utf-8');

        $queryManager = new ApiUsersQuery($this->getDoctrine());

        $users = $queryManager->getUsers();

        $data = [
            "count" => count($users),
            'users' => $users,
        ];

        $response->setData($data);

        return $response;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     * @Route("groups")
     */

    public function getGroupsAction(Request $request){

        $response = new JsonResponse();
        $response->setCharset("utf-8");
        $response->headers->set('Content-Type', 'application/json; charset=utf-8');

        $queryManager = new ApiUsersQuery($this->getDoctrine());

        $users = $queryManager->getGroups();

        $data = [
            "count" => count($users),
            'groups' => $users,
        ];

        $response->setData($data);

        return $response;
    }



    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     * @Route("createUser")
     */

    public function createUserAction(Request $request){

        $requiredFields = ['username', 'password', 'email'];
        $ipAddress = $request->getClientIp();
        $username = $request->get('username');
        $password = $request->get('password');
        $email = $request->get('email');
        $firstname = $request->get('firstname');
        $lastname = $request->get('lastname');
        $company = $request->get('company');
        $phone = $request->get('phone');
        $apiKey = $this->get('app.common')->generateApiKey();


        if(!$ipAddress){
            $msg = 'IP Address was not found';
            return $this->setResponse($msg);
        } elseif(!$apiKey){
            $msg = 'Apikey is invalided';
            return $this->setResponse($msg);
        }else{
            foreach($requiredFields as $field){
                if($request->get($field) === null){
                    $msg = "$field is required";
                    return $this->setResponse($msg);

                }
            }
        }

        $user = $this->getDoctrine()->getRepository('AppBundle:Users')->findOneBy(array(
            'username' => $username
        ));

        if($user){
            $msg = "$username is already existed";
            return $this->setResponse($msg);
        }

        $em = $this->getDoctrine()->getManager();

        $data = array();
        try{

            $em->getConnection()->beginTransaction();

            $user = new Users();
            $user->setIpAddress($ipAddress);
            $user->setUsername($username);
            $user->setPlainPassword($password);
            $user->setEmail($email);
            $user->setFirstName($firstname);
            $user->setLastName($lastname);
            $user->setCompany($company);
            $user->setPhone($phone);
            $user->setApiKey($apiKey);
            $user->setCreatedOn(time());
            $user->setActive(true);

            $em->persist($user);
            $em->flush();
            $em->getConnection()->commit();

            $data['Data'] = [
                'UserId' => $user->getId(),
                'ApiKey' => $user->getApiKey(),
            ];

        } catch(\Exception $e){

            $em->getConnection()->rollback();
            $msg = "Cannot create new user";
            return $this->setResponse($response, $msg);

        }
        return $this->setResponse(null, $data, true);

    }



    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("registerDevice")
     */

    public function registerDeviceAction(Request $request){

        $requiredFields = ['device_id', 'app_name'];
        $device_id = $request->get('device_id');
        $type = $request->get('type');
        $osVersion = $request->get('os_version');
        $localize = $request->get('localize');
        /** @var  $app_id is required */
        $app_name = $request->get('app_name');
        $apiKey = $request->get('api_key');
        /** @var  $deviceIdentifier using for push notification */
        $deviceIdentifier = $request->get('device_identifier');

        $buildVersion = $request->get('build_version');
        $releaseVersion = $request->get('app_version');


        $user = null;

        if($apiKey){
            $user = $this->getDoctrine()->getRepository("AppBundle:Users")->findOneBy(array(
                'apiKey' => $apiKey,
            ));
        }


        foreach($requiredFields as $field){
            if($request->get($field) === null){
                $msg = "$field is required";
                return $this->setResponse($msg);

            }
        }

        $data = array();

        $em = $this->getDoctrine()->getManager();

        try{
            $em->getConnection()->beginTransaction();

            $project = $this->getDoctrine()->getRepository('AppBundle:Projects')->findOneBy(array('name' => $app_name));

            if(!$project){
                return $this->setResponse("Application with name $app_name was not found");
            }

            $device = $this->getDoctrine()->getRepository('AppBundle:Devices')->findOneBy(array(
                'deviceId' => $device_id,
            ));

            /** If user already exists find userDevice include user
             *
             */
            if(!$user){
                $userDevice = $this->getDoctrine()->getRepository('AppBundle:UsersDevices')->findOneBy(array(
                    'device' => $device,
                    'project' => $project,
                ));
            } else {
                $userDevice = $this->getDoctrine()->getRepository('AppBundle:UsersDevices')->findOneBy(array(
                    'device' => $device,
                    'project' => $project,
                    'user' => $user,
                ));
            }
            /** if userDevice already exists , find user
             *  else create new Random user
             */
            if($userDevice){
                $user = $userDevice->getUser();
            }

            if(!$user){
                $user = $this->get("app.common")->generateRandomUser();
                $user->setIpAddress($request->getClientIp());
                $user->setProject($project);
                $em->persist($user);
                $em->flush();
            }
            /** If new Device */

            if(!$device){
                $device = new Devices();
                $device->setDeviceId($device_id);
                $device->setCreatedOn(time());
                $device->setLocalize($localize);
                $device->setOsVersion($osVersion);
                $device->setType($type);

                $em->persist($device);
                $userDevice = new UsersDevices();
                $userDevice->setActive(true);
                $userDevice->setDeviceIdentifiter($deviceIdentifier);
                $userDevice->setCount(1);
                $userDevice->setDevice($device);
                $userDevice->setLastActive(time());
                $userDevice->setProject($project);
                $userDevice->setUser($user);

                $em->persist($userDevice);
                $em->flush();

            } else {

                if($osVersion){
                    $device->setOsVersion($osVersion);
                }

                /** If new UserDevices */

                if(!$userDevice){
                    $userDevice = new UsersDevices();
                    $userDevice->setActive(true);
                    $userDevice->setCount(1);
                    $userDevice->setDeviceIdentifiter($deviceIdentifier);
                    $userDevice->setDevice($device);
                    $userDevice->setLastActive(time());
                    $userDevice->setProject($project);
                    $userDevice->setUser($user);
                    $userDevice->setBuildVersion($buildVersion);
                    $userDevice->setReleaseVersion($releaseVersion);
                } else {

                    if($releaseVersion){
                        $userDevice->setReleaseVersion($releaseVersion);
                    }
                    if($buildVersion){
                        $userDevice->setBuildVersion($buildVersion);
                    }
                    if($deviceIdentifier){
                        $userDevice->setDeviceIdentifiter($deviceIdentifier);
                    }
                    $userDevice->setLastActive(time());
                    $userDevice->setActive(true);
                    $userDevice->setCount($userDevice->getCount()+1);
                }
                $em->persist($device);
                $em->persist($userDevice);
                $em->flush();

            }

            $em->getConnection()->commit();

            $data['Data'] = [
                'ApiKey' => $user->getApiKey(),
                'UserId' => $user->getId(),
                'UserDeviceId' => $userDevice->getId(),
            ];

        } catch(\Exception $e){

            $em->getConnection()->rollback();
//            return $this->setResponse("Cannot register this device");
            return $this->setResponse($e->getMessage());
        }

        return $this->setResponse(null, $data, true);

    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("registerDeviceIdentifier")
     */

    public function registerDeviceIdentifier(Request $request){

        $requiredFields = ['user_device_id', 'device_identifier'];
        $user_device_id = $request->get('user_device_id');
        /** @var  $deviceIdentifier using for push notification */
        $deviceIdentifier = $request->get('device_identifier');


        foreach($requiredFields as $field){
            if($request->get($field) === null){
                $msg = "$field is required";
                return $this->setResponse($msg);

            }
        }

        $data = array();

        $em = $this->getDoctrine()->getManager();

        try{
            $em->getConnection()->beginTransaction();

            $userDevice = $this->getDoctrine()->getRepository("AppBundle:UsersDevices")->findOneBy(array(
                'id' => $user_device_id,
            ));

            if(!$userDevice){
                return $this->setResponse("UserDevice with id $user_device_id was not found");
            }

            $userDevice->setDeviceIdentifiter($deviceIdentifier);

            $em->persist($userDevice);

            $em->flush();

            $em->getConnection()->commit();

            $data['Data'] = [
                'DeviceIdentifier' => $userDevice->getDeviceIdentifiter(),
                'UserDeviceId' => $userDevice->getId(),
            ];

        } catch(\Exception $e){

            $em->getConnection()->rollback();
//            return $this->setResponse("Cannot register this device");
            return $this->setResponse($e->getMessage());
        }

        return $this->setResponse(null, $data, true);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */

    public function getApiKeyAction(Request $request){

        $data['Data'] = [
            'ApiKey' => $this->get('app.common')->generateApiKey(),
        ];

        return $this->setResponse(null, $data, true);

    }

    /**
     * @param null $message
     * @param array $data
     * @param bool|false $success
     * @return JsonResponse
     */

    private function setResponse($message = null, &$data = array(), $success = false ){

        $response = new JsonResponse();
        $response->setCharset("utf-8");
        $response->headers->set('Content-Type', 'application/json; charset=utf-8');
        $data['Success'] = $success;
        if($message){
            $data['ErrorMessage'] = $message;
        }
        try{
            $response->setData($data);
        } catch(\Exception $e){

        }

        return $response;
    }

    /**
     * @param FfDialogues $dialogue
     * @param array $data
     * @return bool
     *
     */

    private function saveDialogue(FfDialogues $dialogue, array $data, $fail = false){

        $em = $this->getDoctrine()->getManager();
        try{
            $em->getConnection()->beginTransaction();

            if($fail){
                $dialogue->setIconUrl($data['Image']['Uri']);
                $dialogue->setAudioUrl($data['AudioUri']);
                $dialogue->setContent($data['Content']);
                $dialogue->setApproved(SettingsManager::APPROVAL_STATUS_PENDING_APPROVAL);

            } else {
                $dialogue->setApproved(SettingsManager::APPROVAL_STATUS_PENDING_APPROVAL);
            }

            $em->persist($dialogue);
            $em->flush();
            $em->getConnection()->commit();
            return true;

        } catch(Exception $e){

            $em->getConnection()->rollback();
            return false;
        }
    }

    private function approveVideo(FbVideos $video){

        $em = $this->getDoctrine()->getManager();
        try {
            $em->getConnection()->beginTransaction();

            $video->setApproved(SettingsManager::APPROVAL_STATUS_APPROVED);
            $video->setUpdatedBy($this->getUser());
            $video->setUpdatedOn(time());

            $em->persist($video);
            $em->flush();
            $em->getConnection()->commit();
            return true;

        } catch (Exception $e) {
            $em->getConnection()->rollback();
            return false;
        }

    }

}