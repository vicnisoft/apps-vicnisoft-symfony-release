<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 6/5/16
 * Time: 11:07 PM
 */

namespace AppBundle\Controller;


use AppBundle\Controller\Super\SuperEntityController;
use AppBundle\Entity\Dialogues;
use AppBundle\Form\EditDialogue;
use AppBundle\Entity\Users;
use AppBundle\Service\Settings\SettingsManager;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DialogueController extends SuperEntityController
{

    protected $route = "manageDialogues";
    private  $copy_path = "copyDialogue";
    /** @var  Dialogues */
    private $dialogue;

    public function createAction(Request $request){

        $this->dialogue = new Dialogues();
        $this->mode = SELF::MODE_CREATE;
        return $this->setAction($request);
    }


    public function editAction(Request $request, $id = null, $category_id = null){

        if($id === null){
            throw new NotFoundHttpException('This id is missing');
        }

        $this->dialogue = $this->getDoctrine()->getRepository('AppBundle:Dialogues')->find($id);

        if($this->dialogue === null){
            throw new NotFoundHttpException('This dialogue id was not found');
        }

        $this->mode = SELF::MODE_EDIT;

        return $this->setAction($request);


    }

    /**
     * @param Request $request
     * @param null $id
     * @return RedirectResponse
     *
     */

    public function approveAction(Request $request, $id = null){

        if($id === null){
            throw new NotFoundHttpException('Dialogue id is missing');
        }

        $entity = $this->getDoctrine()->getRepository('AppBundle:Dialogues')->find($id);

        if($entity === null){
            throw new NotFoundHttpException('This dialogue id was not found');
        }

        $this->approveCategory($entity);

        return $this->redirectToRoute($this->route);
    }

    public function copyAction(Request $request, $id) {

        if($id === null){
            throw new NotFoundHttpException('Dialogue id is missing');
        }

        $entity = $this->getDoctrine()->getRepository('AppBundle:Dialogues')->find($id);

        if($entity === null){
            throw new NotFoundHttpException('This dialogue id was not found');
        }

        if($copy = $this->copyDialogue($entity)) {
            $this->container->get('session')->getFlashBag()->add(
                'success',
                "Dialogue ". $copy->getId()." has been created successfully."
            );
            return $this->redirectToRoute('editDialogue', array(
                'id' => $copy->getId(),
            ));
        } else {

            $this->container->get('session')->getFlashBag()->add(
                'error',
                "There is an error when clone dialogue ". $id." ."
            );

            return $this->redirectToRoute('editDialogue', array(
                'id' => $id,
            ));

        }
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\Form\Form|RedirectResponse|\Symfony\Component\HttpFoundation\Response
     *
     */

    private function setAction(Request $request){

        $data = $this->getData();
        $form = $this->renderPage($request, $data);

        if($form instanceof RedirectResponse){
            return $form;
        }

        return $this->render('AppBundle:form:editDialogue.html.twig',
            array(
                'form' => $form->createView(),
                'data' => $data,

            )
        );
    }

    private function renderPage(Request $request, $data){

        $form = $this->createForm(EditDialogue::class, $this->dialogue, $this->getOptions());
        $form->handleRequest($request);

        if($form->get('cancel')->isClicked()){
            return $this->redirectToRoute($this->route);        }

        if($form->isValid()){

            try {

                if($this->mode == SELF::MODE_CREATE && $this->checkExists()) {

                    $this->container->get('session')->getFlashBag()
                        ->add(
                            'error',
                            "Dialogue with name " . $this->dialogue->getName() . " exits in the category " . $this->dialogue->getCategory()->getName()
                        );

                } else {
                    $this->save($request);
                    if ($this->mode == SELF::MODE_CREATE) {
                        $this->container->get('session')->getFlashBag()->add(
                            'success',
                            "Dialogue ". $this->dialogue->getId()." has been created successfully."
                        );
                    } else {
                        if ($this->mode == SELF::MODE_EDIT) {
                            $this->container->get('session')->getFlashBag()->add(
                                'success',
                                "Dialogue ". $this->dialogue->getId() ." has been updated successfully."
                            );
                        }
                    }

                    return $this->redirectToRoute($this->route);
                }

            } catch (Exception $e) {
                $this->container->get('session')->getFlashBag()
                    ->add(
                        'error',
                        $e->getMessage()
                    );
                $data['error_messages'] = $e->getMessage();
            }
        }

        return $form;
    }

    private function getData(){

        $data['page_title'] = $this->mode == SELF::MODE_CREATE ? "Create Dialogue" : "Edit Dialogue";
        if($this->dialogue && $this->mode == SELF::MODE_EDIT) {
            $data['copy_path'] = $this->generateUrl($this->copy_path, array(
                'id' => $this->dialogue->getId(),
            ));
        }

        $data['options'] = $this->getOptions();

        return $data;
    }

    private function getOptions() {

        $options = array();

        /** @var Users $user */
        $user = $this->getUser();
        if($user->getProject() != null){
            $options['project'] = $user->getProject()->getId();

        } else{
            $options['project'] = false;
        }

        $options['type'] = $this->get('settings')->getDialogueTypes();
        $options['approval_status'] = array_flip($this->get('settings')->getApprovalStatus());

        return $options;
    }

    /**
     * @param Request $request
     * @throws \Exception
     */
    private function save(Request $request) {

        $em = $this->getDoctrine()->getManager();
        try {
            $em->getConnection()->beginTransaction();

            if($this->mode == SELF::MODE_CREATE){
                $project = $this->getUser()->getProject();
                if($project){
                    $this->dialogue->setProject($project);
                }
                $this->dialogue->setCreatedOn(time());
            } else {
                $this->dialogue->setUpdatedOn(time());
            }

            $em->persist($this->dialogue);
            $em->flush();
            $em->getConnection()->commit();

        } catch (Exception $e) {
            $em->getConnection()->rollback();
            throw $e;
        }
    }


    /**
     * @param Dialogues $dialogues
     * @return Dialogues|null
     *
     */
    private function copyDialogue(Dialogues $dialogues) {

        $em = $this->getDoctrine()->getManager();
        try {
            $em->getConnection()->beginTransaction();

            $copy = new Dialogues();
            $copy->setName($dialogues->getName(). " Copy");
            $copy->setCreatedOn(time());
            $copy->setActive($dialogues->getActive());
            $copy->setApproved(SettingsManager::APPROVAL_STATUS_NOT_COMPLETED);
            $copy->setCategory($dialogues->getCategory());
            $copy->setContent($dialogues->getContent());
            $copy->setContentUrl($dialogues->getContentUrl());
            $copy->setIconUrl($dialogues->getIconUrl());
            $copy->setMediaUrl($dialogues->getMediaUrl());
            $copy->setProject($dialogues->getProject());
            $copy->setType($dialogues->getType());
            $em->persist($copy);
            $em->flush();
            $em->getConnection()->commit();

            return $copy;

        } catch (Exception $e) {
            $em->getConnection()->rollback();
        }

        return null;

    }

    private function checkExists(){

        return $this->getDoctrine()->getRepository('AppBundle:Dialogues')->findOneBy(array(
            'name' => $this->dialogue->getName(),
            'category' => $this->dialogue->getCategory(),
        ));

    }

    private function approveCategory(Dialogues $entity){

        $em = $this->getDoctrine()->getManager();
        try {
            $em->getConnection()->beginTransaction();

            $entity->setApproved(SettingsManager::APPROVAL_STATUS_APPROVED);
            $entity->setUpdatedOn(time());

            $em->persist($entity);
            $em->flush();
            $em->getConnection()->commit();

            $this->container->get('session')->getFlashBag()->add(
                'success',
                "Dialogue ". $entity->getName()." approved successfully."
            );

        } catch (Exception $e) {
            $em->getConnection()->rollback();

            $this->container->get('session')->getFlashBag()->add(
                'error',
                "Dialogue ". $entity->getName()." cannot approve."
            );
        }

    }

}