<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 6/3/16
 * Time: 12:46 AM
 */

namespace AppBundle\Controller\Super;


use AppBundle\Entity\UsersColumns;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Tools\Filters\Filter;
use AppBundle\Tools\QueryHelpers\QueryManager;
use Symfony\Component\HttpFoundation\Session\Attribute\AttributeBag;
use Exception;

abstract class SuperManageController extends Controller
{
    protected $userInfos;
    protected $create_path = '';
    protected $recycle_path = '';
    protected $edit_path = '';
    protected $page_path = '';
    protected $filter_path;
    protected $route = '';
    protected $openpanel = 'fa-caret-down';
    /** @var  ManagerFilters */
    protected $filterManager;
    /** @var  QueryManager  */
    protected $queryManager;
    protected $filters = array();
    protected $active_filters = array();

    protected $dbname;

    protected $columns;

    protected abstract function getDefaultColumns();

    protected function recycle($id, $active) {
    }
    abstract protected  function init();


    abstract protected function renderPage(Request $request);

    public function recycleAction(Request $request) {


        $id = $request->get('recycleid');
        $active = $request->get('active');

        $this->recycle($id, $active);

        return $this->redirectToRoute($this->route);

    }

    protected function getColumns(Request $request) {
        $default_columns = $this->getDefaultColumns();
        $columns = $this->getColumnsFromUserColumns($this->dbname, $default_columns);
        return $columns;
    }


    protected function setColumns(Request $request) {
        $columns = $request->get("columns");
        $default_columns = $this->getDefaultColumns();
        if (isset($columns) && !empty($columns)) {
            $current_columns = array();
            foreach ($default_columns as $index => $column) {
                $current_columns[$index] = $column;
                $current_columns[$index]["display"] = (in_array($index, $columns)) ? true : false;
            }

            $this->setUserColumns($current_columns);
            $this->columns = $current_columns;
        } else {
            $this->columns = $this->getColumns($request);
        }

    }

    public function setPageAction(Request $request) {


        $this->init();

        if(!count($request->request)){
            return $this->redirectToRoute($this->route);
        }


//        $this->setPageSpecific($request);

//        $order = $request->get("order");
//        $field = $request->get("field");

        /*
        if (empty($order)) {
            $order = $this->getPageValuesInSession("order");
        } else {
            if($field == $this->getPageValuesInSession("orderfield")) {
                $order = ($order == QueryBuilderTool::ORDERASC) ? QueryBuilderTool::ORDERDESC : QueryBuilderTool::ORDERASC;
            }
        }

        $this->setPageValuesInSession("order", $order);
        $this->setPageValuesInSession("orderfield", $field);
        */

        // active page
        $active = $request->request->get("active");
        $this->setPageValuesInSession("active", $active);

        // open panel
        $this->openpanel = $request->request->get("openpanel");
        $this->setPageValuesInSession("openpanel", $this->openpanel);

//        $rows_per_page = $this->getAndSetFromRequestOrSession($request, "itemsperpage", $this->queryManager->getRowsPerPage());
//        $this->queryManager->setRowsPerPage($rows_per_page);

//        $orderTool = new OrderTool($field, $order);
//        $this->queryManager->setOrder($orderTool);

        // Reset the page if user applied filters
//        $page = ($request->request->get("filterbtn") !== null) ? 0 : $request->get("page");
//        $this->queryManager->setPage($page);
//        $this->setPageValuesInSession("page", $page);

        $this->setColumns($request);

        if($request->get('resetfilterbtn') ) {
            $this->clearFilters();
        } else {
            $this->setFilters($request);
        }

        return $this->renderPage($request);
    }

    protected function getTotalFirstLineColumns($columns) {
        $total = 0;
        foreach ($columns as $column) {
            if ((!isset($column['line']) || $column['line'] == 0) && $column['display']) {
                $total++;
            }
        }
        return $total;
    }




    /*********** Filters  ***************/

    abstract protected function initFilters();

    protected function clearFilters() {
        $this->queryManager->setPage(0);
        $this->filters = $this->initFilters();
        foreach ($this->filters as $filter) {
            $this->setFilterValueInSession($filter->getName(), $filter->getDefaultValue());
        }
        $this->active_filters = array();
    }


    protected function setFilterValueInSession($name, $value) {
        $this->initSessionAttribute();

        $base = $this->get('session')->get($this->dbname);
        /* $base AttributeBag */
        if (!$base->has('filter')) {
            $filters = new AttributeBag('filter');
            foreach ($this->filters as $filter) {
                /** @var $filter Filter */
                $filters->set($filter->getName(), $filter->getDefaultValue());
            }
            $base->set('filter', $filters);
        }

        $base->get('filter')->set($name, $value);
        $this->get('session')->set($this->dbname, $base);
    }

    protected function initSessionAttribute() {
        if (!$this->get('session')->has($this->dbname)) {
            $controller_attr = new AttributeBag($this->dbname);
            $this->get('session')->set($this->dbname, $controller_attr);
        }
    }

    protected function setPageValuesInSession($name, $value) {
        $this->initSessionAttribute();
        $this->get('session')->get($this->dbname)->set($name, $value);
    }

    /**
     * Replaces $this->get('session')->get($this->dbname)->get($name)
     *
     * If the value isn't set in session, will return default value.
     *
     * @param string $name The name to search for in this page's session
     * @param mixed $default_value Default value to return if passed name can't be found in session. Defaults to "".
     * @return mixed The value found, or default value if name can't be found.
     */
    protected function getPageValuesInSession($name, $default_value = "") {
        $value = $default_value;
        if ($this->get('session')->get($this->dbname) != null && $this->get('session')->get($this->dbname)->has($name) && $this->get('session')->get($this->dbname)->get($name) != null) {
            $value = $this->get('session')->get($this->dbname)->get($name);
        }
        return $value;
    }

    /**
     * This function calls getPageValuesInSession and then setPageValuesInSession with the result and returns it.
     *
     * @param string $name
     * @param mixed $default_value
     * @return mixed
     */
    protected function getAndSetPageValuesInSession($name, $default_value = "") {
        $value = $this->getPageValuesInSession($name, $default_value);
        $this->setPageValuesInSession($name, $value);
        return $value;
    }

    protected function setFilters(Request $request) {

        $this->filters = $this->initFilters();
        $resetfilter = $request->get('activefilter', null);
        $resetfiltervalue = $request->get('activefiltervalue', null);
        /* @var $filter Filter */
        foreach ($this->filters as $filter) {
            if($filter->getName() == $resetfilter && !$resetfiltervalue) {
                $value = $filter->getDefaultValue();
            } else {
                $value = $request->get($filter->getName(), null);
                // select filter with multiple values
                if($filter->getName() == $resetfilter && $resetfiltervalue && is_array($value)) {
                    $index = array_search($resetfiltervalue, $value);
                    array_splice($value, $index, 1);
                    if(!count($value) ){
                        $value = $filter->getDefaultValue();
                    }
                }
                if ($filter->getType() == Filter::TYPE_TEXT && gettype($value) != "string") {
                    $value = null; // To fix bug when we change the type of a filter from select to text field
                }
            }
            if (!isset($value) && $this->get('session')->has($this->dbname) && $this->get('session')->get($this->dbname)->has('filter')) {
                $value = $this->get('session')->get($this->dbname)->get('filter')->get($filter->getName());
            }
            if (isset($value)) {
                $this->setFilterValueInSession($filter->getName(), $value);
                if ($value != $filter->getDefaultValue()) {
                    $this->queryManager->addCondition($filter->setConditionValue($value));
                }
            }
        }
        $this->setActiveFilters();
    }

    protected function setActiveFilters() {
        $this->active_filters = array();
        /* @var $filter Filter */
        foreach($this->filters as $filter) {
            if($filter->getValue()!== null && $filter->getValue() != $filter->getDefaultValue() ) {
                $this->active_filters[$filter->getName()] = $filter;
            }
        }
    }


    protected function getColumnsFromUserColumns($dbname, $columns) {
        if (!empty($columns)) {
            $storeColumns = $this->getUserColumns($dbname);
            if (isset($storeColumns) && !empty($storeColumns)) {
                $array = json_decode($storeColumns, true);
                $new_columns = array();
                foreach ($columns as $id => $column) {
                    $new_columns[$id] = $column;
                    if (isset($array[$id])) {
                        $new_columns[$id]['display'] = $array[$id];
                    }
                }
                $columns = $new_columns;
            }
        }
        return $columns;
    }

    protected function setUserColumns($columns){
        $storeColumns = array();
        foreach ($columns as $index => $column) {
            $storeColumns[$index] = $column['display'];
        }

        $entity = strtoupper($this->dbname);
        $this->saveUserColumns($storeColumns,$entity);
    }

    private function saveUserColumns($columns, $entity){
        try {
            $em = $this->getDoctrine()->getManager();
            $userColumns = $this->getDoctrine()->getRepository('AppBundle:UsersColumns')->findOneBy(array("entity" => $entity,'user' => $this->getUser()));
            if(!isset($userColumns) ) {
                $userColumns = new UsersColumns();
                $userColumns->setEntity($entity);
                $userColumns->setUser($this->getUser());
            }

            $userColumns->setColumns(json_encode($columns) );
            $em->persist($userColumns);

            $em->flush();
        } catch (Exception $e) {

            return false;
        }

        return true;
    }

    private function getUserColumns($entity){
        $userColumns = $this->getDoctrine()->getRepository('AppBundle:UsersColumns')->findOneBy(array("entity" => $entity,'user' => $this->getUser()));
        if(isset($userColumns)){
            return $userColumns->getColumns();
        }
        return null;
    }
}