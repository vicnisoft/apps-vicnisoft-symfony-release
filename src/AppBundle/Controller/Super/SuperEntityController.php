<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 5/31/16
 * Time: 5:42 PM
 */

namespace AppBundle\Controller\Super;


use AppBundle\Entity\Photos;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SuperEntityController extends Controller
{

    const MODE_CREATE  =   'create';
    const MODE_EDIT    =   'edit';

    protected $mode;
    protected $route = '';



    protected function addNewPhoto(Request $request, $table, $id)
    {
        try {
            $em = $this->container->get("doctrine.orm.default_entity_manager");

            $document = new Photos();
            $media = $request->files->get('file');

            $document->setFile($media);
            $document->setPath($media->getPathName());
            $document->setName($media->getClientOriginalName());
            $document->setTargetId($id);
            $document->setTargetTable($table);

            $em->persist($document);
            $em->flush();
            return $document->getId();
        } catch(\Exception $e){

            return -1;
        }


    }
}