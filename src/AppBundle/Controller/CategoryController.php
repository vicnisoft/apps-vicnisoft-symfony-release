<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 7/6/16
 * Time: 11:36 AM
 */

namespace AppBundle\Controller;


use AppBundle\Controller\Super\SuperEntityController;
use AppBundle\Entity\Categories;
use AppBundle\Form\EditCategory;
use AppBundle\Entity\Users;
use AppBundle\Service\Settings\SettingsManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Exception;

class CategoryController extends SuperEntityController
{
    protected $route = "manageCategories";
    /** @var  Categories  The category that is editing */
    private $category ;

    public function createAction(Request $request){

        $this->mode = SELF::MODE_CREATE;
        $this->category = new Categories();
        return $this->setAction($request);

    }



    public function editAction(Request $request, $id = null){

        if($id === null){
            throw new NotFoundHttpException('Category id is missing');
        }

        $this->category = $this->getDoctrine()->getRepository('AppBundle:Categories')->find($id);

        if($this->category === null){
            throw new NotFoundHttpException('This category id was not found');
        }

        return $this->setAction($request);


    }

    public function approveAction(Request $request, $id = null){

        if($id === null){
            throw new NotFoundHttpException('Category id is missing');
        }

        $category = $this->getDoctrine()->getRepository('AppBundle:Categories')->find($id);

        if($category === null){
            throw new NotFoundHttpException('This category id was not found');
        }

        $this->approveCategory($category);

        return $this->redirectToRoute($this->route);
    }

    /** Privates */

    private function setAction(Request $request){

        $data = $this->getData();
        $form = $this->renderPage($request, $data);

        if($form instanceof RedirectResponse){
            return $form;
        }

        return $this->render('AppBundle:form:editCategory.html.twig',
            array(
                'form' => $form->createView(),
                'data' => $data,

            )
        );
    }


    private function renderPage(Request $request, $data){

        /** @Var EditFBCategory */

        $form = $this->createForm(EditCategory::class, $this->category, $this->getOptions());
        $form->handleRequest($request);

        if($form->get('cancel')->isClicked()){
            return $this->redirectToRoute($this->route);
        }

        if($form->isValid()){
            try {

                if($this->mode == SELF::MODE_CREATE && $this->checkExists()) {

                    $projectName = "";
                    if($project = $this->getUser()->getProject()) {
                        $projectName = $project->getName();
                    }

                    $this->container->get('session')->getFlashBag()
                        ->add(
                            'error',
                            "Category with name " . $this->category->getName() . " exits in the project " . $projectName
                        );

                } else {
                    $this->save($request);
                    if ($this->mode == SELF::MODE_CREATE) {
                        $this->container->get('session')->getFlashBag()->add(
                            'success',
                            "Category ". $this->category->getName()." created successfully."
                        );
                    } else {
                        if ($this->mode == SELF::MODE_EDIT) {
                            $this->container->get('session')->getFlashBag()->add(
                                'success',
                                "Category ". $this->category->getName() ." updated successfully."
                            );
                        }
                    }

                    return $this->redirectToRoute($this->route);
                }

            } catch (Exception $e) {
                $this->container->get('session')->getFlashBag()
                    ->add(
                        'error',
                        $e->getMessage()
                    );
                $data['error_messages'] = $e->getMessage();
            }
        }

        return $form;
    }

    private function getData(){

        $data['page_title'] = $this->mode == SELF::MODE_CREATE ? "Create Category" : "Edit Category";
        $data['options'] = $this->getOptions();
        return $data;
    }

    private function getOptions() {

        $options = array();
        if($this->mode === SELF::MODE_CREATE){
            $hasProject = $this->getUser()->getProject() ? true : false;
            $options['user_has_project'] = $hasProject;
        } else {
            $options['user_has_project'] = false;

        }
        $options['approval_status'] = array_flip($this->get('settings')->getApprovalStatus());

        return $options;
    }


    /**
     * @param Request $request
     * @throws Exception
     */
    protected function save(Request $request) {

        $em = $this->getDoctrine()->getManager();
        try {
            $em->getConnection()->beginTransaction();
            if($this->mode == SELF::MODE_CREATE){
                $project = $this->getUser()->getProject();
                if($project){
                    $this->category->setProject($project);
                }
                $this->category->setCreatedOn(time());
            } else {
                $this->category->setUpdatedOn(time());
            }

            $em->persist($this->category);
            $em->flush();
            $em->getConnection()->commit();

        } catch (Exception $e) {
            $em->getConnection()->rollback();
            throw $e;
        }
    }

    private function checkExists(){

        return $this->getDoctrine()->getRepository('AppBundle:Categories')->findOneBy(array(
           'name' => $this->category->getName(),
            'project' => $this->getUser()->getProject(),
        ));

    }

    private function approveCategory(Categories $entity){

        $em = $this->getDoctrine()->getManager();
        try {
            $em->getConnection()->beginTransaction();

            $entity->setApproved(SettingsManager::APPROVAL_STATUS_APPROVED);
            $entity->setUpdatedOn(time());
            $em->persist($entity);
            $em->flush();
            $em->getConnection()->commit();

            $this->container->get('session')->getFlashBag()->add(
                'success',
                "Category ". $entity->getName()." approved successfully."
            );

        } catch (Exception $e) {
            $em->getConnection()->rollback();

            $this->container->get('session')->getFlashBag()->add(
                'error',
                "Category ". $entity->getName()." cannot approve."
            );
        }

    }

}