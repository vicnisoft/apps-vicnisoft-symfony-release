<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 5/31/16
 * Time: 5:40 PM
 */

namespace AppBundle\Controller;


use AppBundle\Controller\Super\SuperEntityController;
use AppBundle\Entity\Roles;
use AppBundle\Entity\Users;
use AppBundle\Entity\UsersRoles;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Form\EditUser;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


class UserController extends SuperEntityController
{
    /** @var Users The user we are editing */
    private $user;

    /**
     * @Route("createUser", name="createUser")
     */

    public function createAction(Request $request){

//        $this->denyAccessUnlessGranted('ROLE_CAN_ACCESS_USERS');
        $this->mode = SELF::MODE_CREATE;
        $this->user = new Users();
        return $this->setAction($request);

    }

    public function editAction(Request $request, $id = null){

        if($this->getUser()->getUsername() != 'admin'){
            $this->denyAccessUnlessGranted('ROLE_CAN_ACCESS_USERS');
        }
        if($id === null){
            throw new NotFoundHttpException('This user id is missing');
        }

        $this->user = $this->getDoctrine()->getRepository('AppBundle:Users')->find($id);
        if($this->user === null){
            throw new  NotFoundHttpException('This user id was not found');
        }
        $this->mode = SELF::MODE_EDIT;

        $this->initRoles();

        return $this->setAction($request);

    }

    /** Privates */

    private function setAction(Request $request){

        $data = $this->getData($request);
        $form = $this->renderPage($request, $data);

        if($form instanceof RedirectResponse){
            return $form;
        }

        return $this->render('AppBundle:form:editUser.html.twig',
            array(
                'form' => $form->createView(),
                'data' => $data,

            )
        );
    }


    private function initRoles(){

        foreach($this->get('settings')->getAvailablesRoles() as $roleName){

            $em = $this->getDoctrine()->getManager();
            $role = $em->getRepository('AppBundle:Roles')->findOneBy(array(
                'name' => $roleName,
            ));

            if($role != null) continue;

            try {
                $em->getConnection()->beginTransaction();

                $role = new Roles();
                $role->setName($roleName);
                $role->setActive(true);
                $role->setCreatedOn(time());

                $em->persist($role);
                $em->flush();
                $em->getConnection()->commit();
            } catch (Exception $e) {
                $em->getConnection()->rollback();
            }
        }

    }

    private function renderPage(Request $request, $data){

        /** @Var EditUser */

        $form = $this->createForm(EditUser::class, $this->user, $this->getOptions($request));
        $form->handleRequest($request);

        if($form->get('cancel')->isClicked()){
            return $this->redirectToRoute("manageUsers");
        }

        if($form->isValid()){

            try {
                $this->save($request);
                if ($this->mode == SELF::MODE_CREATE) {
                    $this->container->get('session')->getFlashBag()->add(
                        'success',
                        "User ". $this->displayUser()." created successfully."
                    );
                } else {
                    if ($this->mode == SELF::MODE_EDIT) {
                        $this->container->get('session')->getFlashBag()->add(
                            'success',
                            "User ". $this->displayUser() ." updated successfully."
                        );
                    }
                }

                return $this->redirectToRoute('manageUsers');

            } catch (Exception $e) {
                $this->container->get('session')->getFlashBag()
                    ->add(
                        'error',
                        $e->getMessage()
                    );
                $data['error_messages'] = $e->getMessage();
            }
        }

        return $form;
    }

    private function getData(Request $request){

        $data['page_title'] = $this->mode == SELF::MODE_CREATE ? "Create User" : "Edit User";
        $data['options'] = $this->getOptions($request);
        return $data;
    }

    private function getOptions(Request $request) {

        $has_api_key = ( $this->user && $this->user->getApiKey());

        $api_key = $this->get('app.common')->generateApiKey();
        $options = array(
            'has_api_key' => $has_api_key,
            'api_key' => $api_key,
        );

        return $options;
    }


    /**
     * @param Request $request
     * @throws \Exception
     */
    protected function save(Request $request) {

        $em = $this->getDoctrine()->getManager();
        try {
            $em->getConnection()->beginTransaction();

            if($this->mode === SELF::MODE_EDIT){

            } else {
                $this->user->setIpAddress($request->getClientIp());
                $this->user->setCreatedOn(time());
                $this->user->setActive(true);
            }

            $em->persist($this->user);
            $em->flush();
            $em->getConnection()->commit();
            if ($this->mode == SELF::MODE_CREATE) {
                $this->sendWelcomeEmail($this->user);
            }
        } catch (Exception $e) {
            $em->getConnection()->rollback();
            throw $e;
        }
    }


    private function sendWelcomeEmail(Users $user) {
//        try {
//            // send notifications
//            /* @var $notificationManager NotificationManager */
//            $notificationManager = $this->get('notifications');
//            $notificationManager->sendWelcomeEmail($user);
//
//        } catch (Exception $e) {
//            $this->get('logger')->error("Failed to send welcome email", array("message" => $e->getMessage()));
//        }
    }

    private function displayUser() {
        // todo: link to single view
        return "{$this->user->getUsername()} <a href='mailto:{$this->user->getEmail()}'><{$this->user->getEmail()}></a>";
    }
}