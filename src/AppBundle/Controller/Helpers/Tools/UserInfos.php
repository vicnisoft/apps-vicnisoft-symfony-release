<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 8/12/16
 * Time: 2:43 PM
 */

namespace AppBundle\Controller\Helpers\Tools;
use AppBundle\Entity\Users;

class UserInfos
{
    private $projectIds = array();

    /** @var  Users */
    private $user ;

    /**
     * UserInfos constructor.
     * @param Users $user
     */
    public function __construct(Users $user)
    {
        $this->user = $user;
        if ($this->user->getProject()) {
            $this->projectIds = [$this->user->getProject()->getId()];
        }
    }

    /**
     * @return array
     */
    public function getProjectIds()
    {
        return $this->projectIds;
    }

    /**
     * @param array $projectIds
     */
    public function setProjectIds($projectIds)
    {
        $this->projectIds = $projectIds;
    }

    /**
     * @return Users
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param Users $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }





}