<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 8/12/16
 * Time: 4:28 PM
 */

namespace AppBundle\Controller\Helpers\Filters;


use AppBundle\Controller\Helpers\Tools\UserInfos;
use AppBundle\Tools\Filters\ManageFilters;
use AppBundle\Tools\Filters\Filter;
use AppBundle\Tools\Conditions\Condition;
use AppBundle\Tools\QueryHelpers\QueryBuilderTool;

class DeviceFilters extends ManageFilters
{

    public function setFilters(UserInfos $userInfos)
    {
        // TODO: Implement setFilters() method.
        $filters = array();
        /** @Desc("Defaults") */
        $default = 'Defaults';
        $this->addSection($default);

//		Username
        $condition = new Condition(Condition::INTEXT, 'username', 'u');
        $text = 'Username';
        $filter = new Filter(Filter::TYPE_TEXT, 'username', $text, '', $condition);
        $filter->setSection($default);
        $filters["username"] = $filter;

        // Count
        $condition = new Condition(Condition::GOREQUAL, 'count', 'ud');
        $text = 'Count';
        $filter = new Filter(Filter::TYPE_NUMBER, 'count', $text, '', $condition);
        $filter->setSection($default);
        $filters["count"] = $filter;

        // os_version
        $condition = new Condition(Condition::EQUAL, 'os_version', 'd');
        $text = 'OS Version';
        $filter = new Filter(Filter::TYPE_NUMBER, 'os_version', $text, '', $condition);
        $filter->setSection($default);
        $filters["os_version"] = $filter;
        // build_version
        $condition = new Condition(Condition::EQUAL, 'build_version', 'ud');
        $text = 'Build Version';
        $filter = new Filter(Filter::TYPE_TEXT, 'build_version', $text, '', $condition);
        $filter->setSection($default);
        $filters["build_version"] = $filter;
        // release_version
        $condition = new Condition(Condition::EQUAL, 'release_version', 'ud');
        $text = 'Release Version';
        $filter = new Filter(Filter::TYPE_TEXT, 'release_version', $text, '', $condition);
        $filter->setSection($default);
        $filters["release_version"] = $filter;

        // Project Name

        if(empty($userInfos->getProjectIds())) {
            $queryBuilder = new QueryBuilderTool();
            $queryBuilder->addToSelect("DISTINCT p.name as text, p.id as value");
            $queryBuilder->setFrom('projects', 'p');
            $queryBuilder->addOrderBy('name');

            $condition = new Condition(Condition::IN, 'id', 'p');
            $text = 'Project Name';
            $filter = new Filter(Filter::TYPE_SELECT, 'project_name', $text, array(0), $condition, $queryBuilder);
            $filter->setDefaultText($text);
            $filter->setSection($default);
            $filters["project_name"] = $filter;
        }

        // Get filters values
        $this->getFiltersValues($filters);
        return $filters;
    }
}