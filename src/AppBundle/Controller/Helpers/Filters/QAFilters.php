<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 8/11/16
 * Time: 9:19 PM
 */

namespace AppBundle\Controller\Helpers\Filters;

use AppBundle\Tools\Filters\Filter;
use AppBundle\Tools\Conditions\Condition;
use AppBundle\Tools\Filters\ManageFilters;
use AppBundle\Controller\Helpers\Tools\UserInfos;
use AppBundle\Tools\QueryHelpers\QueryBuilderTool;

class QAFilters extends ManageFilters
{
    public function setFilters(UserInfos $userInfos) {
        $filters = array();
        /** @Desc("Defaults") */
        $default = 'Defaults';
        $this->addSection($default);

//		Question
        $condition = new Condition(Condition::INTEXT, 'questions', 'qa');
        $text = 'Questions';
        $filter = new Filter(Filter::TYPE_TEXT, 'question', $text, '', $condition);
        $filter->setSection($default);
        $filters["question"] = $filter;

        //		Answer
        $condition = new Condition(Condition::INTEXT, 'answer', 'qa');
        $text = 'Answer';
        $filter = new Filter(Filter::TYPE_TEXT, 'answer', $text, '', $condition);
        $filter->setSection($default);
        $filters["answer"] = $filter;

        // Project Name

        if(empty($userInfos->getProjectIds())) {
            $queryBuilder = new QueryBuilderTool();
            $queryBuilder->addToSelect("DISTINCT p.name as text, p.id as value");
            $queryBuilder->setFrom('projects', 'p');
            $queryBuilder->addOrderBy('name');

            $condition = new Condition(Condition::IN, 'id', 'p');
            $text = 'Project Name';
            $filter = new Filter(Filter::TYPE_SELECT, 'project_name', $text, array(0), $condition, $queryBuilder);
            $filter->setDefaultText($text);
            $filter->setSection($default);
            $filters["project_name"] = $filter;
        }
        // Active

        $condition = new Condition(Condition::NOTEQUAL, 'active', 'qa');
        $text = 'Status';
        $filter = new Filter(Filter::TYPE_CHOICE, 'status', $text, -1 , $condition);
        $filter->setDefaultText("All Status");
        $options = array(
            array(
                "value" => "0",
                /** @Desc("Active") */
                "text" => "Active",
            ),
            array("value" => "1",
                /** @Desc("Recycled") */
                "text" => "Recycled",
            )
        );
        $filter->setSelectValues($options);
        $filters['status'] = $filter;

        // Get filters values
        $this->getFiltersValues($filters);
        return $filters;
    }

}