<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 10/3/16
 * Time: 10:47 PM
 */

namespace AppBundle\Controller\Helpers\Filters;



use AppBundle\Controller\Helpers\Tools\UserInfos;
use AppBundle\Tools\Filters\ManageFilters;
use AppBundle\Tools\Filters\Filter;
use AppBundle\Tools\Conditions\Condition;
use AppBundle\Tools\QueryHelpers\QueryBuilderTool;

class UsersFilters extends ManageFilters
{
    public function setFilters(UserInfos $userInfos)
    {
        // TODO: Implement setFilters() method.
        $filters = array();
        /** @Desc("Defaults") */
        $default = 'Defaults';
        $this->addSection($default);

//		Username
        $condition = new Condition(Condition::INTEXT, 'username', 'u');
        $text = 'Username';
        $filter = new Filter(Filter::TYPE_TEXT, 'username', $text, '', $condition);
        $filter->setSection($default);
        $filters["username"] = $filter;

        // Project Name

        if(empty($userInfos->getProjectIds())) {
            $queryBuilder = new QueryBuilderTool();
            $queryBuilder->addToSelect("DISTINCT p.name as text, p.id as value");
            $queryBuilder->setFrom('projects', 'p');
            $queryBuilder->addOrderBy('name');

            $condition = new Condition(Condition::IN, 'id', 'p');
            $text = 'Project Name';
            $filter = new Filter(Filter::TYPE_SELECT, 'project_name', $text, array(0), $condition, $queryBuilder);
            $filter->setDefaultText($text);
            $filter->setSection($default);
            $filters["project_name"] = $filter;
        }

        // Get filters values
        $this->getFiltersValues($filters);
        return $filters;
    }


}