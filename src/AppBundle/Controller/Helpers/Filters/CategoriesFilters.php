<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 8/12/16
 * Time: 5:09 PM
 */

namespace AppBundle\Controller\Helpers\Filters;


use AppBundle\Controller\Helpers\Tools\UserInfos;
use AppBundle\Tools\Filters\ManageFilters;
use AppBundle\Tools\Conditions\Condition;
use AppBundle\Tools\Filters\Filter;
use AppBundle\Tools\QueryHelpers\QueryBuilderTool;

class CategoriesFilters extends ManageFilters
{
    public function setFilters(UserInfos $userInfos)
    {
        // TODO: Implement setFilters() method.
        $filters = array();
        /** @Desc("Defaults") */
        $default = 'Defaults';
        $this->addSection($default);

        //		Name
        $condition = new Condition(Condition::INTEXT, 'name', 'c');
        $text = 'Name';
        $filter = new Filter(Filter::TYPE_TEXT, 'name', $text, '', $condition);
        $filter->setSection($default);
        $filters["name"] = $filter;

        //		Description
        $condition = new Condition(Condition::INTEXT, 'description', 'd');
        $text = 'Desc';
        $filter = new Filter(Filter::TYPE_TEXT, 'description', $text, '', $condition);
        $filter->setSection($default);
        $filters["description"] = $filter;

        // Project Name

        if(empty($userInfos->getProjectIds())) {
            $queryBuilder = new QueryBuilderTool();
            $queryBuilder->addToSelect("DISTINCT p.name as text, p.id as value");
            $queryBuilder->setFrom('projects', 'p');
            $queryBuilder->addOrderBy('name');

            $condition = new Condition(Condition::IN, 'id', 'p');
            $text = 'Project Name';
            $filter = new Filter(Filter::TYPE_SELECT, 'project_name', $text, array(0), $condition, $queryBuilder);
            $filter->setDefaultText($text);
            $filter->setSection($default);
            $filters["project_name"] = $filter;
        }

        // Approval Status

        $condition = new Condition(Condition::EQUAL, 'approved', 'c');
        $text = 'Approval Status';
        $filter = new Filter(Filter::TYPE_CHOICE, 'approval_status', $text, -1 , $condition);
        $filter->setDefaultText("Select status");
        $statues = $this->settings->getApprovalStatus();
        $options = array();
        foreach ($statues as $key => $value){
            $options[] = array(
                'value' => $key,
                'text' => $value,
            );
        }
        $filter->setValue('-1');
        $filter->setSelectValues($options);
        $filters['approval_status'] = $filter;


        // Get filters values
        $this->getFiltersValues($filters);
        return $filters;
    }


}