<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 8/8/16
 * Time: 2:28 PM
 */

namespace AppBundle\Controller\Helpers\Queries;


use AppBundle\Tools\Conditions\Condition;
use AppBundle\Tools\QueryHelpers\QueryManager;
use AppBundle\Tools\QueryHelpers\QueryBuilderTool;
use AppBundle\Controller\Helpers\Tools\UserInfos;

class QAQuery extends QueryManager
{

    public function buildQuery(UserInfos $userInfos = null){

        $sqlBuilder = new QueryBuilderTool();
        $select = "
            qa.id as id, questions as question, answer, qa.active as active , p.name as project_name
        ";
        $sqlBuilder->addToSelect($select);
        $sqlBuilder->setFrom('questions_answers', 'qa');
        $sqlBuilder->addJoins(QueryBuilderTool::LEFTJOIN,'projects','p','p.id = qa.project_id');

        if(!empty($userInfos->getProjectIds())){
            $condition = new Condition(Condition::IN, 'id','p', '', $userInfos->getProjectIds());
            $sqlBuilder->addCondition($condition);
        }

        foreach ($this->conditions as $condition) {
            $sqlBuilder->addCondition($condition);
        }
        return $this->executeQuery($sqlBuilder);


    }

    public function getQAs(){

        $sqlBuilder = new QueryBuilderTool();
        $select = "
            qa.id as id, questions as question, answer, qa.active as active , p.name as project_name
        ";
        $sqlBuilder->addToSelect($select);
        $sqlBuilder->setFrom('questions_answers', 'qa');
        $sqlBuilder->addJoins(QueryBuilderTool::LEFTJOIN,'projects','p','p.id = qa.project_id');
        return $this->executeQuery($sqlBuilder);
    }

}