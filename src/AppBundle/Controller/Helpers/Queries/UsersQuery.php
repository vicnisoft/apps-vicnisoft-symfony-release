<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 6/4/16
 * Time: 2:35 PM
 */

namespace AppBundle\Controller\Helpers\Queries;


use AppBundle\Tools\Conditions\Condition;
use AppBundle\Tools\QueryHelpers\QueryBuilderTool;
use AppBundle\Tools\QueryHelpers\QueryManager;
use AppBundle\Controller\Helpers\Tools\UserInfos;

class UsersQuery extends QueryManager
{


    public function buildQuery(UserInfos $userInfos = null){

        $sqlBuilder = new QueryBuilderTool();
        $select = "
            u.id as id, ip_address, username, email, from_unixtime(last_login) last_login, g.name as group_name, p.name as project_name
        ";
        $sqlBuilder->addToSelect($select);
        $sqlBuilder->setFrom('users', 'u');
        $sqlBuilder->addJoins(QueryBuilderTool::LEFTJOIN,'users_groups','ug','ug.user_id = u.id');
        $sqlBuilder->addJoins(QueryBuilderTool::LEFTJOIN,'groups','g','ug.group_id = g.id');
        $sqlBuilder->addJoins(QueryBuilderTool::LEFTJOIN,'projects','p','p.id = u.project_id');

        $condition = new Condition(Condition::EQUAL, 'active', 'u', '', 1);
        $sqlBuilder->addCondition($condition);

        if(!is_null($userInfos) && !empty($userInfos->getProjectIds())){
            $condition = new Condition(Condition::IN,'id', 'p', '', $userInfos->getProjectIds());
            $sqlBuilder->addCondition($condition);
        }

        foreach($this->conditions as $condition){
            $sqlBuilder->addCondition($condition);
        }

        return $this->executeQuery($sqlBuilder);

    }


}