<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 6/5/16
 * Time: 5:24 PM
 */

namespace AppBundle\Controller\Helpers\Queries;


use AppBundle\Tools\Conditions\Condition;
use AppBundle\Tools\QueryHelpers\QueryBuilderTool;
use AppBundle\Tools\QueryHelpers\QueryManager;
use AppBundle\Controller\Helpers\Tools\UserInfos;

class DialoguesQuery extends QueryManager
{


    public function buildQuery(UserInfos $userInfos = null){

        $queryBuilder = new QueryBuilderTool();
        $queryBuilder->addToSelect('d.id, d.name, d.content , d.icon_url, d.media_url, d.type as type,
        d.approved as approval_status, c.name as category_name, p.name as project_name,
        COUNT(dr.id) number_report
        ');
        $queryBuilder->setFrom('dialogues', 'd');
        $queryBuilder->addJoins(QueryBuilderTool::LEFTJOIN,'categories', 'c', 'c.id = d.category_id');
        $queryBuilder->addJoins(QueryBuilderTool::LEFTJOIN,'projects', 'p', 'p.id = d.project_id');
        $queryBuilder->addJoins(QueryBuilderTool::LEFTJOIN,'dialogues_reports', 'dr', 'dr.dialogue_id = d.id');
        $queryBuilder->addOrderBy('id', QueryBuilderTool::ORDERDESC);
        $queryBuilder->addGroupBy('id', 'd');
        $condition = new Condition(Condition::EQUAL, 'active', 'd', '', true);
        $queryBuilder->addCondition($condition);

        if(!is_null($userInfos) && !empty($userInfos->getProjectIds())){
            $condition = new Condition(Condition::IN,'id', 'p', '', $userInfos->getProjectIds());
            $queryBuilder->addCondition($condition);
        }

        foreach($this->conditions as $condition){
            $queryBuilder->addCondition($condition);
        }

        return $this->executeQuery($queryBuilder);
    }

}