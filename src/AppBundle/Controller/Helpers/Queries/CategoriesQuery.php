<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 6/4/16
 * Time: 7:58 PM
 */

namespace AppBundle\Controller\Helpers\Queries;


use AppBundle\Tools\QueryHelpers\QueryBuilderTool;
use AppBundle\Tools\QueryHelpers\QueryManager;
use AppBundle\Tools\Conditions\Condition;
use AppBundle\Controller\Helpers\Tools\UserInfos;
class CategoriesQuery extends QueryManager
{

    public function buildQuery(UserInfos $userInfos = null){

        $sqlBuilder = new QueryBuilderTool();
        $select = "
            c.id as id, c.name as name, c.description as description, p.name as project_name,
            c.icon_url as icon_url,  c.approved as approval_status, count(d.id) as total_dialogues

        ";
        $sqlBuilder->addToSelect($select);
        $sqlBuilder->setFrom('categories', 'c');
        $sqlBuilder->addJoins(QueryBuilderTool::LEFTJOIN, 'projects', 'p','p.id = c.project_id');
        $sqlBuilder->addJoins(QueryBuilderTool::LEFTJOIN, 'dialogues', 'd','d.category_id = c.id');
        $condition = new Condition(Condition::EQUAL, 'active', 'c', '', true);
        $sqlBuilder->addCondition($condition);
        $sqlBuilder->addGroupBy('id','c');

        if(!is_null($userInfos) && !empty($userInfos->getProjectIds())){
            $condition = new Condition(Condition::IN,'id', 'p', '', $userInfos->getProjectIds());
            $sqlBuilder->addCondition($condition);
        }

        foreach($this->conditions as $condition){
            $sqlBuilder->addCondition($condition);
        }

        return $this->executeQuery($sqlBuilder);

    }

    public function getCategories(){

        $queryBuilder = new QueryBuilderTool();
        $queryBuilder->addToSelect('*');
        $queryBuilder->setFrom('ff_categories','ffc');

        return $this->executeQuery($queryBuilder);
    }

}