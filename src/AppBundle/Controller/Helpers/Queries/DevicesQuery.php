<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 7/1/16
 * Time: 10:01 AM
 */

namespace AppBundle\Controller\Helpers\Queries;


use AppBundle\Tools\QueryHelpers\QueryManager;
use AppBundle\Tools\QueryHelpers\QueryBuilderTool;
use AppBundle\Tools\Conditions\Condition;
use AppBundle\Controller\Helpers\Tools\UserInfos;

class DevicesQuery extends QueryManager
{
    public function buildQuery(UserInfos $userInfos = null){
        $sqlBuilder = new QueryBuilderTool();
        $select = "
            ud.id as user_device_id, from_unixtime(ud.last_active) as last_active, ud.count as count,
            ud.device_identifiter as device_identifier, ud.release_version, ud.build_version,
            d.id as id, d.type as type, d.os_version as os_version, d.localize as localize, from_unixtime(d.created_on) as created_on,
            p.name as project_name, u.username as username, u.first_name as first_name, u.last_name as last_name
        ";
        $sqlBuilder->addToSelect($select);
        $sqlBuilder->setFrom('users_devices', 'ud');
        $sqlBuilder->addJoins(QueryBuilderTool::LEFTJOIN,'devices','d','d.id = ud.device_id');
        $sqlBuilder->addJoins(QueryBuilderTool::LEFTJOIN,'projects','p','p.id = ud.project_id');
        $sqlBuilder->addJoins(QueryBuilderTool::LEFTJOIN,'users','u','u.id = ud.user_id');
        $sqlBuilder->addGroupBy('id','ud');

        if(!is_null($userInfos) && !empty($userInfos->getProjectIds())){
            $condition = new Condition(Condition::IN,'id', 'p', '', $userInfos->getProjectIds());
            $sqlBuilder->addCondition($condition);
        }

        foreach($this->conditions as $condition){
            $sqlBuilder->addCondition($condition);
        }

        return $this->executeQuery($sqlBuilder);

    }

}