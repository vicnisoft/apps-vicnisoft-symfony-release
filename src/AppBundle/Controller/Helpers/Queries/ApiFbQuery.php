<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 6/10/16
 * Time: 2:05 PM
 */

namespace AppBundle\Controller\Helpers\Queries;


use AppBundle\Service\Settings\SettingsManager;
use AppBundle\Tools\QueryHelpers\ApiQuery;
use AppBundle\Tools\QueryHelpers\QueryBuilderTool;
use AppBundle\Tools\Conditions\Condition;

class ApiFbQuery extends ApiQuery
{

    /**
     * @param bool|true $nolimit
     * @param null $updatedOn
     * @param null $createdOn
     * @return array
     *
     */

    public function getFbVideos($nolimit = true, $updatedOn = null, $createdOn = null){

        $sqlBuilder = new QueryBuilderTool();
        $select = "
		    fb_v.id as id, fb_v.name, fb_v.short_description as short_description , fb_v.video_url, fb_v.type,
            fb_v.description, fb_v.created_on, fb_v.updated_on, fb_v.active, fb_v.approved, fb_v.media_url,
            GROUP_CONCAT(DISTINCT fb_c.name SEPARATOR ', ') AS categories
		";

        $sqlBuilder->addToSelect($select);
        $sqlBuilder->setFrom('fb_videos','fb_v');
        $sqlBuilder->addJoins(QueryBuilderTool::LEFTJOIN,'fb_categories_videos','fb_cv','fb_v.id = fb_cv.video_id');
        $sqlBuilder->addJoins(QueryBuilderTool::LEFTJOIN,'fb_categories','fb_c','fb_c.id = fb_cv.category_id');
        $sqlBuilder->addGroupBy('id','fb_v');
        if($updatedOn){
            $sqlBuilder->addIntCondition('updated_on','fb_v',$updatedOn,Condition::GREATER);
        }
        if($createdOn){
            $sqlBuilder->addIntCondition('created_on','fb_v',$createdOn,Condition::GREATER);
        }

        if(!$nolimit) {
            $sqlBuilder->setLimit(($this->page * $this->rows_per_page) . "," . $this->rows_per_page);
            $this->executeCountQuery();
            if ($this->total_rows == 0) {
                return array();
            }
        }

//        $sqlBuilder->addIntCondition('active','fb_v',1,Condition::EQUAL);
//        $sqlBuilder->addIntCondition('approved','fb_v',SettingsManager::APPROVAL_STATUS_APPROVED,Condition::EQUAL);


        return $this->executeQuery($sqlBuilder);
    }


    /**
     * @param bool|true $nolimit
     * @param null $updatedOn
     * @param null $createdOn
     * @return array
     *
     */

    public function getFbCategories($nolimit = true, $updatedOn = null, $createdOn = null){

        $sqlBuilder = new QueryBuilderTool();
        $select = "
		*
		";

        $sqlBuilder->addToSelect($select);
        $sqlBuilder->setFrom('fb_categories','fb_c');
        $sqlBuilder->setGroupBy('id','fb_c');
        if($updatedOn){
            $sqlBuilder->addIntCondition('updated_on','fb_c',$updatedOn,Condition::GREATER);
        }
        if($createdOn){
            $sqlBuilder->addIntCondition('created_on','fb_c',$createdOn,Condition::GREATER);
        }

        $sqlBuilder->addIntCondition('active','fb_c',1,Condition::EQUAL);


        if(!$nolimit) {
            $sqlBuilder->setLimit(($this->page * $this->rows_per_page) . "," . $this->rows_per_page);
            $this->executeCountQuery();
            if ($this->total_rows == 0) {
                return array();
            }
        }

        return $this->executeQuery($sqlBuilder);
    }

}