<?php

/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 5/26/16
 * Time: 6:07 PM
 */

namespace AppBundle\Controller\Helpers\Queries;

use AppBundle\Tools\QueryHelpers\ApiQuery;
use AppBundle\Tools\Conditions\Condition;
use AppBundle\Tools\QueryHelpers\QueryBuilderTool;

class ApiUsersQuery extends  ApiQuery
{

    public function getUsers($nolimit = true)
    {

        $sqlBuilder = new QueryBuilderTool();
        $select = "
		*
		";

        $sqlBuilder->addToSelect($select);
        $sqlBuilder->setFrom('users','u');
        $sqlBuilder->setGroupBy('id','u');

        if(!$nolimit) {
            $sqlBuilder->setLimit(($this->page * $this->rows_per_page) . "," . $this->rows_per_page);
            $this->executeCountQuery();
            if ($this->total_rows == 0) {
                return array();
            }
        }

        return $this->executeQuery($sqlBuilder);

    }

    public function getGroups($nolimit = true){

        $sqlBuilder = new QueryBuilderTool();
        $select = "
		*
		";

        $sqlBuilder->addToSelect($select);
        $sqlBuilder->setFrom('groups','gr');
        $sqlBuilder->setGroupBy('id','gr');

        if(!$nolimit) {
            $sqlBuilder->setLimit(($this->page * $this->rows_per_page) . "," . $this->rows_per_page);
            $this->executeCountQuery();
            if ($this->total_rows == 0) {
                return array();
            }
        }

        return $this->executeQuery($sqlBuilder);
    }

}