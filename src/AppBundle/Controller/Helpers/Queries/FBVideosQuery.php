<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 6/9/16
 * Time: 5:15 PM
 */

namespace AppBundle\Controller\Helpers\Queries;


use AppBundle\Tools\Conditions\Condition;
use AppBundle\Tools\QueryHelpers\QueryBuilderTool;
use AppBundle\Tools\QueryHelpers\QueryManager;
use AppBundle\Controller\Helpers\Tools\UserInfos;

class FBVideosQuery extends QueryManager
{
    public function buildQuery(UserInfos $userInfos = null){

        $sqlBuilder = new QueryBuilderTool();
        $select = "
            fb_v.id as id, fb_v.name, fb_v.short_description as short_description , fb_v.video_url, fb_v.type,
            fb_v.approved, fb_v.media_url,
            fb_v.created_on as created_on,
            GROUP_CONCAT(DISTINCT fb_c.name SEPARATOR ', ') AS categories
        ";
        $sqlBuilder->addToSelect($select);
        $sqlBuilder->setFrom('fb_videos', 'fb_v');
        $sqlBuilder->addJoins(QueryBuilderTool::LEFTJOIN,'fb_categories_videos','fb_cv','fb_v.id = fb_cv.video_id');
        $sqlBuilder->addJoins(QueryBuilderTool::LEFTJOIN,'fb_categories','fb_c','fb_c.id = fb_cv.category_id');
        $sqlBuilder->addGroupBy('id','fb_v');
        $condition = new Condition(Condition::EQUAL, 'active', 'fb_v', '', 1);
        $sqlBuilder->addCondition($condition);
        $sqlBuilder->addOrderBy('id', QueryBuilderTool::ORDERDESC);

        return $this->executeQuery($sqlBuilder);

    }

}