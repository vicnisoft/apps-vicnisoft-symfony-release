<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 6/9/16
 * Time: 5:04 PM
 */

namespace AppBundle\Controller\Helpers\Queries;


use AppBundle\Tools\Conditions\Condition;
use AppBundle\Tools\QueryHelpers\QueryBuilderTool;
use AppBundle\Tools\QueryHelpers\QueryManager;
use AppBundle\Controller\Helpers\Tools\UserInfos;
class FBCategoriesQuery extends QueryManager
{

    public function buildQuery(UserInfos $userInfos = null){

        $sqlBuilder = new QueryBuilderTool();
        $select = "
            id , name, description, created_on
        ";
        $sqlBuilder->addToSelect($select);
        $sqlBuilder->setFrom('fb_categories', 'fbc');
        $sqlBuilder->addOrderBy('created_on', QueryBuilderTool::ORDERDESC);
        $condition = new Condition(Condition::EQUAL, 'active', 'fbc', '', 1);
        $sqlBuilder->addCondition($condition);

        return $this->executeQuery($sqlBuilder);

    }
}