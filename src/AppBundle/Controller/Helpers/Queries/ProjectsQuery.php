<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 6/4/16
 * Time: 7:53 PM
 */

namespace AppBundle\Controller\Helpers\Queries;


use AppBundle\Tools\Conditions\Condition;
use AppBundle\Tools\QueryHelpers\QueryBuilderTool;
use AppBundle\Tools\QueryHelpers\QueryManager;
use AppBundle\Controller\Helpers\Tools\UserInfos;

class ProjectsQuery extends QueryManager
{
    public function buildQuery(UserInfos $userInfos = null){
        $queryBuilder = new QueryBuilderTool();
        $select = '*';
        $queryBuilder->addToSelect($select);
        $queryBuilder->setFrom('projects','p');

        foreach($this->conditions as $condition){
            $queryBuilder->addCondition($condition);
        }

        return $this->executeQuery($queryBuilder);
    }


}