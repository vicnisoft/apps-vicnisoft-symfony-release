<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 7/7/16
 * Time: 1:26 AM
 */

namespace AppBundle\Controller\Helpers\Queries;


use AppBundle\Service\Settings\SettingsManager;
use AppBundle\Tools\QueryHelpers\ApiQuery;
use AppBundle\Tools\QueryHelpers\QueryBuilderTool;
use AppBundle\Tools\Conditions\Condition;

class ApiV1Query extends ApiQuery
{

    public function getQuestionsAnswers($projectName = null, $nolimit = true, $updatedOn = null, $createdOn = null){

        $sqlBuilder = new QueryBuilderTool();

        $select = "qa.id , qa.questions as question , qa.answer, qa.created_on, qa.updated_on, qa.active as active
         ";

        $sqlBuilder->addToSelect($select);
        $sqlBuilder->setFrom('questions_answers','qa');
        if($updatedOn){
            $sqlBuilder->addIntCondition('updated_on','qa',$updatedOn,Condition::GREATER);
        }
        if($createdOn){
            $sqlBuilder->addIntCondition('created_on','qa',$createdOn,Condition::GREATER);
        }


        if(!is_null($projectName)){
            $sqlBuilder->addJoins(QueryBuilderTool::LEFTJOIN,'projects', 'p','p.id = qa.project_id');
            $condition = new Condition(Condition::EQUAL, 'name', 'p', '', $projectName);
            $sqlBuilder->addCondition($condition);
        }

        if(!$nolimit) {
            $sqlBuilder->setLimit(($this->page * $this->rows_per_page) . "," . $this->rows_per_page);
            $this->executeCountQuery();
            if ($this->total_rows == 0) {
                return array();
            }
        }

        return $this->executeQuery($sqlBuilder);
    }

    public function getDialoguesGroupByCategory($projectName = null, $nolimit = true, $updatedOn = null, $createdOn = null){

        $categories = $this->getCategories($projectName, $nolimit);

        $dialogues = $this->getDialogues($projectName, $nolimit, $updatedOn, $createdOn);

        if(is_null($categories) || is_null($dialogues) || count($categories) == 0 || count($dialogues) ==0){
            return array();
        } else {
            $data = array();
            foreach($categories as $cat) {

                $tmp = array();

                foreach($dialogues as $dia){
                    if($cat['id'] === $dia['category_id']){
                        $tmp[] = $dia;
                    }
                }
                if(count($tmp) == 0){
                    continue;
                }
                $cat['Dialogues'] = $tmp;
                $data[] = $cat;
            }

           return $data;

        }

    }

    public function getDialogues($projectName = null, $nolimit = true, $updatedOn = null, $createdOn = null){

        $sqlBuilder = new QueryBuilderTool();

        $select = "d.id , d.name , d.active, d.content, d.icon_url, d.media_url, d.type, d.created_on, d.updated_on,
         c.id as category_id";

        $sqlBuilder->addToSelect($select);
        $sqlBuilder->setFrom('dialogues','d');
        if($updatedOn){
            $sqlBuilder->addIntCondition('updated_on','d',$updatedOn,Condition::GREATER, QueryBuilderTool::CONNECTOR);
            $condition = new Condition(Condition::ISNULL, 'updated_on','d','','' );
            $sqlBuilder->addCondition($condition);
        }

        if($createdOn){
            $sqlBuilder->addIntCondition('created_on','d',$createdOn,Condition::GREATER);
        }

        $sqlBuilder->addIntCondition('approved','d', SettingsManager::APPROVAL_STATUS_APPROVED,Condition::EQUAL);

        $sqlBuilder->addJoins(QueryBuilderTool::LEFTJOIN,'categories', 'c','c.id = d.category_id');


        if(!is_null($projectName)){
            $sqlBuilder->addJoins(QueryBuilderTool::LEFTJOIN,'projects', 'p','p.id = d.project_id');
            $condition = new Condition(Condition::EQUAL, 'name', 'p', '', $projectName);
            $sqlBuilder->addCondition($condition);
        }

        if(!$nolimit) {
            $sqlBuilder->setLimit(($this->page * $this->rows_per_page) . "," . $this->rows_per_page);
            $this->executeCountQuery();
            if ($this->total_rows == 0) {
                return array();
            }
        }

        return $this->executeQuery($sqlBuilder);
    }

    /**
     * @param bool|true $nolimit
     * @param null $updatedOn
     * @param null $createdOn
     * @return array
     *
     */

    public function getCategories($projectName = null,$nolimit = true, $updatedOn = null, $createdOn = null){

        $sqlBuilder = new QueryBuilderTool();
        $select = "c.id, c.name, c.description, c.active, c.icon_url, c.created_on, c.updated_on, d.type ,count(d.id) as number_dialogues";

        $sqlBuilder->addToSelect($select);
        $sqlBuilder->setFrom('categories','c');
        if(!is_null($projectName)){
            $sqlBuilder->addJoins(QueryBuilderTool::LEFTJOIN,'projects', 'p','p.id = c.project_id');
            $condition = new Condition(Condition::EQUAL, 'name', 'p', '', $projectName);
            $sqlBuilder->addCondition($condition);
            $sqlBuilder->addGroupBy('id', 'p');
        }

        $sqlBuilder->addJoins(QueryBuilderTool::LEFTJOIN,'dialogues','d','d.category_id = c.id');

        $sqlBuilder->setGroupBy('id','c');
        if($updatedOn){
            $sqlBuilder->addIntCondition('updated_on','c',$updatedOn,Condition::GREATER, QueryBuilderTool::CONNECTOR);
            $condition = new Condition(Condition::ISNULL, 'updated_on','c','','' );
            $sqlBuilder->addCondition($condition);
        }
        if($createdOn){
            $sqlBuilder->addIntCondition('created_on','c',$createdOn,Condition::GREATER);
        }

        $sqlBuilder->addIntCondition('approved','c', SettingsManager::APPROVAL_STATUS_APPROVED,Condition::EQUAL);

        if(!$nolimit) {
            $sqlBuilder->setLimit(($this->page * $this->rows_per_page) . "," . $this->rows_per_page);
            $this->executeCountQuery();
            if ($this->total_rows == 0) {
                return array();
            }
        }

        return $this->executeQuery($sqlBuilder);
    }

}