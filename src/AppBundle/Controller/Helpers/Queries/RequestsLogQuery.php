<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 8/18/16
 * Time: 3:45 PM
 */

namespace AppBundle\Controller\Helpers\Queries;


use AppBundle\Controller\Helpers\Tools\UserInfos;
use AppBundle\Tools\QueryHelpers\QueryManager;
use AppBundle\Tools\QueryHelpers\QueryBuilderTool;
use AppBundle\Tools\Conditions\Condition;

class RequestsLogQuery extends QueryManager
{

    public function buildQuery(UserInfos $userInfos = null)
    {
        // TODO: Implement buildQuery() method.
        $sqlBuilder = new QueryBuilderTool();
        $select = "
        rq.http_user_agent, from_unixtime(rq.created_on) as created_on, rq.client_ip as client_ip, rq.id as request_id, COUNT(rq.id) as count,
        rq.country_name, rq.city, rq.time_zone,
            p.name as project_name, u.username as username, u.id as user_id
        ";
        $sqlBuilder->addToSelect($select);
        $sqlBuilder->setFrom('requests', 'rq');
        $sqlBuilder->addJoins(QueryBuilderTool::LEFTJOIN,'users','u','u.id = rq.user_id');
        $sqlBuilder->addJoins(QueryBuilderTool::LEFTJOIN,'projects','p','p.id = u.project_id');
        $sqlBuilder->addGroupBy('client_ip','rq');
        $sqlBuilder->addOrderBy('created_on', QueryBuilderTool::ORDERDESC);

        if(!is_null($userInfos) && !empty($userInfos->getProjectIds())){
            $condition = new Condition(Condition::IN,'id', 'p', '', $userInfos->getProjectIds());
            $sqlBuilder->addCondition($condition);
        }

        foreach($this->conditions as $condition){
            $sqlBuilder->addCondition($condition);
        }

        return $this->executeQuery($sqlBuilder);
    }
}