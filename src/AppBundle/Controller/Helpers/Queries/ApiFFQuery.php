<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 5/26/16
 * Time: 7:04 PM
 */

namespace AppBundle\Controller\Helpers\Queries;


use AppBundle\Tools\QueryHelpers\ApiQuery;
use AppBundle\Tools\QueryHelpers\QueryBuilderTool;
use AppBundle\Tools\Conditions\Condition;

class ApiFFQuery extends ApiQuery
{

    public function getCatetories($nolimit = true){

        $sqlBuilder = new QueryBuilderTool();
        $select = "
		*
		";

        $sqlBuilder->addToSelect($select);
        $sqlBuilder->setFrom('ff_categories','ffcat');
        $sqlBuilder->setGroupBy('id','ffcat');

        if(!$nolimit) {
            $sqlBuilder->setLimit(($this->page * $this->rows_per_page) . "," . $this->rows_per_page);
            $this->executeCountQuery();
            if ($this->total_rows == 0) {
                return array();
            }
        }

        return $this->executeQuery($sqlBuilder);

    }

    public function getDialogues($nolimit = true){

        $sqlBuilder = new QueryBuilderTool();
        $select = "
		*
		";

        $sqlBuilder->addToSelect($select);
        $sqlBuilder->setFrom('ff_dialogues','d');
        $sqlBuilder->setGroupBy('id','d');

        if(!$nolimit) {
            $sqlBuilder->setLimit(($this->page * $this->rows_per_page) . "," . $this->rows_per_page);
            $this->executeCountQuery();
            if ($this->total_rows == 0) {
                return array();
            }
        }

        return $this->executeQuery($sqlBuilder);

    }

}