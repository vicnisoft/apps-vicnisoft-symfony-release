<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 6/4/16
 * Time: 1:59 PM
 */

namespace AppBundle\Controller\Helpers\Queries;

use AppBundle\Tools\Conditions\Condition;
use AppBundle\Tools\QueryHelpers\QueryBuilderTool;
use AppBundle\Tools\QueryHelpers\QueryManager;
use AppBundle\Controller\Helpers\Tools\UserInfos;

class HomeQuery extends  QueryManager
{

    private $dbName = 'dev_apps';

    public function buildQuery(UserInfos $userInfos = null){

    }

    public function setDbName($dbName){
        $this->dbName = $dbName;
    }
    public function getEntitiesInfo(){

        $sqlBuilder = new QueryBuilderTool();
        $select = "table_name as name, TABLE_ROWS as row FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = ". $this->dbName;
        $sqlBuilder->addToSelect($select);
//        $sqlBuilder->setFrom('INFORMATION_SCHEMA.TABLES');
//        $sqlBuilder->addCondition(new Condition(Condition::EQUAL,'TABLE_SCHEMA',''))
        return $this->executeQuery($sqlBuilder);
    }
}