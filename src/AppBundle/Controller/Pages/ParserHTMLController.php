<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 8/21/16
 * Time: 11:40 AM
 */

namespace AppBundle\Controller\Pages;


use AppBundle\Models\ParserHTML\EslFast;
use AppBundle\Service\ToolsParserHTML\EnglishEslFast;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Form\ParserHTML\EditEslFast;
use Doctrine\ORM\QueryBuilder;



class ParserHTMLController extends Controller
{

    private $manage_dialogues_path = 'manageDialogues';

    private $path = 'parserElsFast';

    private $create_path = 'createDialogue';

    private $eslFast;
    /**
     * @param \AppBundle\Controller\Pages\Request $request
     * @return \Symfony\Component\Form\Form|Response
     */
    public function parserElsFastAction(Request $request){

        $this->eslFast = new EslFast();

        return $this->renderEslFastPage($request);

    }


    /**
     * @param Request $request
     *
     */


    private function renderEslFastPage(Request $request){

        $options = array();

        /** @var Users $user */
        $user = $this->getUser();
        if($user->getProject() != null){
            $options['project'] = $user->getProject()->getId();

        } else{
            $options['project'] = false;
        }

        $form = $this->createForm(EditEslFast::class, $this->eslFast, $options);
        $form->handleRequest($request);
        if($form->get('cancel')->isClicked()){
            return $this->redirectToRoute($this->manage_dialogues_path);
        }

        if($form->isValid()){
            try {

                $result = $this->get('english_esl_fast')->parser($this->eslFast);

                $this->container->get('session')->getFlashBag()->add(
                    'success',
                    "This " . $result['URL'] . " has been parsed successfully"
                );

                $data['page_title'] = 'Parser EslFast Result';
                $data['path'] = array(
                    'create_path' => $this->create_path,
                    'path' => $this->path,
                );

                $data['result'] = $result;

                return $this->render('AppBundle:pages/tools/parserhtmls:eslfast-parser-result.html.twig',array(
                    'data' => $data,
                ));

            } catch (Exception $e) {
                $this->container->get('session')->getFlashBag()
                    ->add(
                        'error',
                        $e->getMessage()
                    );
                $data['error_messages'] = $e->getMessage();
            }
        }


        if($form instanceof RedirectResponse){
            return $form;
        }

        $data['page_title'] = 'Parser Data From EslFast';

        return $this->render('AppBundle:pages/tools/parserhtmls:edit-eslfast.html.twig',
            array(
                'form' => $form->createView(),
                'data' => $data,
            )
        );
    }

}