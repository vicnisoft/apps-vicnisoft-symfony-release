<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 5/31/16
 * Time: 11:15 PM
 */

namespace AppBundle\Controller\Pages;


use AppBundle\Controller\Helpers\Queries\HomeQuery;
use AppBundle\Controller\Helpers\Tools\UserInfos;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Controller\Helpers\Queries\DashboardQuery;

class HomeController extends Controller
{
    /** @var  DashboardQuery */

    private $queryManager;
    private $userInfo;

    public function showAction(Request $request){

        $this->init();

        return $this->render('AppBundle:pages:home.html.twig',
            array(
                'data' => $this->getData(),
            )
        );
    }

    private function init(){

        $this->userInfo = new UserInfos($this->getUser());
        $this->queryManager = new DashboardQuery($this->getDoctrine());

    }

    /** Privates */

    private function getData(){

        $data['page_title'] = "Home";
        $data['items'] = array();

        $result = $this->queryManager->buildQuery($this->userInfo);
        if ($result){
            $data['notifications'] = $result;
        }

        return $data;
    }


}