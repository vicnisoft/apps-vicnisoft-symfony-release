<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 6/2/16
 * Time: 4:42 PM
 */

namespace AppBundle\Controller\Pages;


use AppBundle\Entity\FfCategories;
use AppBundle\Entity\FfDialogues;
use AppBundle\Form\EditPushNotification;
use AppBundle\Models\PushNotification;
use AppBundle\Service\Settings\SettingsManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ToolsController extends Controller
{
    const PUSH_PROJECT = 'PUSH_PROJECT';
    const PUSH_DEVICE = 'PUSH_DEVICE';

    const PUSH_ROUTE_PROJECT = 'manageProjects';
    const PUSH_ROUTE_DEVICE = 'manageDevices';


    private $pushMode = SELF::PUSH_DEVICE ;
    private $pushRoute = SELF::PUSH_ROUTE_DEVICE;


    /**
     * @param Request $request
     * @return Response
     *
     */

    public function indexAction(Request $request){

        $data['page_title'] = 'Tools';
        return $this->render('AppBundle:pages:tools.html.twig', array(
            'data' => $this->getData($data),
        ));
    }


    /**
     * @param Request $request
     * @param null $id
     * @return \Symfony\Component\Form\Form|RedirectResponse|Response
     * @throws NotFoundHttpException
     *
     */

    public function pushNotificationToDeviceAction(Request $request, $id = null){

        if($id === null){
            throw new NotFoundHttpException('User Device ID is missing');
        }

        $userDevice = $this->getDoctrine()->getRepository('AppBundle:UsersDevices')->findOneBy(array(
            'id' => $id,
        ));

        if($userDevice == null) {
            throw new NotFoundHttpException('User Device with ID was not found');
        }

        $pushNotificationModel = new PushNotification();
        $pushNotificationModel->setDeviceOnly(true);
        $pushNotificationModel->setDeviceId($userDevice->getDeviceIdentifiter());
        $pushNotificationModel->setProject($userDevice->getProject());

        $this->pushRoute = SELF::PUSH_ROUTE_DEVICE;
        $this->pushMode = SELF::PUSH_DEVICE;

        return $this->renderPushPage($request, $pushNotificationModel);

    }


    /**
     * @param Request $request
     * @param null $id
     * @return \Symfony\Component\Form\Form|RedirectResponse|Response
     * @throws NotFoundHttpException
     *
     */

    public function pushNotificationToProjectAction(Request $request, $id = null){

        if($id === null){
            throw new NotFoundHttpException("Project ID is missing");
        }

        $project = $this->getDoctrine()->getRepository('AppBundle:Projects')->findOneBy(array(
            'id' => $id,
        ));

        if($project == null) {
            throw new NotFoundHttpException('Project with ID was not found');
        }

        $pushNotificationModel = new PushNotification();
        $pushNotificationModel->setDeviceOnly(false);
        $pushNotificationModel->setProject($project);

        $this->pushRoute = SELF::PUSH_ROUTE_PROJECT;
        $this->pushMode = SELF::PUSH_PROJECT;

        return $this->renderPushPage($request, $pushNotificationModel);

    }


    /**
     * @param Request $request
     *
     */

    public function translatorAction(Request $request){

    }

    /**
     * @param Request $request
     * @return Response
     *
     */
    public function scheduleFetchFbVideoAction(Request $request){

        $result = $this->get('fb.hightlight_videos')->getVideos();

        if($result === true){
            $this->container->get('session')->getFlashBag()
                ->add(
                    'success',
                    'All videos have been saved'
                );
        } else {
            $this->container->get('session')->getFlashBag()
                ->add(
                    'error',
                    'Cannot get videos'
                );
        }
        $data['page_title'] = 'Get Football Hightlight Videos';
        return $this->render('AppBundle:pages:tools.html.twig', array(
            'data' => $this->getData($data),
        ));
    }


    public function testPushAction(Request $request){

        $this->get('push_notifications')->sendIosMessage();
        $data['page_title'] = 'Test Push';
        return $this->render('AppBundle:pages:tools.html.twig', array(
            'data' => $this->getData($data),
        ));
    }

    /**
     * @param Request $request
     * @return Response
     *
     */

    public function parserCategoryFrancaisFacileAction(Request $request){

        $result = $this->get('app.francais_facile')->getCategories('http://www.podcastfrancaisfacile.com/apprendre-le-francais/french-communication-dialogue-daily-life-listen-to-mp3.html');

        if(is_array($result)){

            $data['html'] = "There are ". count($result) . " result";


        } else {
            $data['html'] = $result;
        }
        $data['page_title'] = 'Get Categories';

        return $this->render('AppBundle:pages/tools:test.parser.html.twig', array(
            'data' => $this->getData($data),
        ));

    }

    /**
     * @param Request $request
     * @return Response
     *
     */

    public function parserDialogueFrancaisFacileAction(Request $request){

        $dialogue = $this->getDoctrine()->getRepository('AppBundle:FfDialogues')->findOneBy(array(
            'content' => '',
            'iconUrl' => '',
            'audioUrl' => '',
            'approved' => SettingsManager::APPROVAL_STATUS_NOT_COMPLETED,
        ));
        if($dialogue != null && $dialogue->getContentUrl() != null ){
            $result = $this->get('html_parser')->parserDialogies($dialogue->getContentUrl());
            $this->saveDialogue($dialogue, $result);
            $data['Dialogue'] = $result;

        }

        $data['page_title'] = 'Get Dialogues';

        return $this->render('AppBundle:pages:tools.html.twig', array(
            'data' => $this->getData($data),
        ));

    }

    /**
     * @param Request $request
     *
     */

    private function renderPushPage(Request $request, PushNotification $pushNotificationModel){

        $form = $this->createForm(EditPushNotification::class, $pushNotificationModel);
        $form->handleRequest($request);
        if($form->get('cancel')->isClicked()){
            return $this->redirectToRoute($this->pushRoute);
        }

        if($form->isValid()){

            try {

                $result = $this->get('push_notifications')->sendIosMessage($pushNotificationModel);

                $strResult = implode (", ", $result);

                $this->container->get('session')->getFlashBag()->add(
                    'success',
                    "Message ".$pushNotificationModel->getMessage()." has been sent to ". $strResult." successfully."
                );

                return $this->redirectToRoute($this->pushRoute);

            } catch (Exception $e) {
                $this->container->get('session')->getFlashBag()
                    ->add(
                        'error',
                        $e->getMessage()
                    );
                $data['error_messages'] = $e->getMessage();
            }
        }



        if($form instanceof RedirectResponse){
            return $form;
        }

        $data['page_title'] = 'Send Push Notifications';

        return $this->render('AppBundle:pages/tools:push.notifications.html.twig',
            array(
                'form' => $form->createView(),
                'data' => $data,

            )
        );
    }


    /**
     * @param Request $request
     * @return mixed
     *
     */

    private function getData(&$data){

        $data['tools'] = array(
            array(
                'name' => 'Get Categories',
                'uri' => 'parserCategories',
                'panel_class' => 'panel-green',
            ),
            array(
                'name' => 'Get Dialogue',
                'uri' => 'parserDialogue',
                'panel_class' => 'panel-red',
            ),
            array(
                'name' => 'Get Categories',
                'uri' => 'parserCategories',
                'panel_class' => 'btn-primary',
            ),
            array(
                'name' => 'Get Categories',
                'uri' => 'parserCategories',
                'panel_class' => 'btn-primary',
            ),
            array(
                'name' => 'Get Categories',
                'uri' => 'parserCategories',
                'panel_class' => 'btn-primary',
            ),
            array(
                'name' => 'Get Categories',
                'uri' => 'parserCategories',
                'panel_class' => 'btn-primary',
            ),


        );

        return $data;

    }

    private function saveDialogue(FfDialogues $dialogue, array $data){

        $em = $this->getDoctrine()->getManager();
        try{
            $em->getConnection()->beginTransaction();

            $dialogue->setIconUrl($data['Image']['Uri']);
            $dialogue->setAudioUrl($data['AudioUri']);
            $dialogue->setContent($data['Content']);
            $dialogue->setApproved(SettingsManager::APPROVAL_STATUS_PENDING_APPROVAL);

            $em->persist($dialogue);
            $em->flush();
            $em->getConnection()->commit();

            $this->container->get('session')->getFlashBag()
                ->add(
                    'success',
                    "Dialogue ". $dialogue->getId() ." has been saved"
                );

            return true;

        } catch(Exception $e){

            $em->getConnection()->rollback();
            $this->container->get('session')->getFlashBag()
                ->add(
                    'error',
                    $e->getMessage()
                );

            return false;
        }
    }

    /**
     * @param array $result
     * @return bool
     *
     */

    private function save(array $result){

        if(count($result) == 0){
            $this->container->get('session')->getFlashBag()
                ->add(
                    'error',
                    $e->getMessage()
                );

            return false;
        }

        $em = $this->getDoctrine()->getManager();
        try{
            $em->getConnection()->beginTransaction();

            foreach($result as $row){

                $category = $this->getDoctrine()->getRepository('AppBundle:FfCategories')->findOneBy(array(
                    'name' => $row['CategoryName'],
                ));

                if($category != null){
                    continue;
                }

                $category = new FfCategories();
                $category->setName($row['CategoryName']);
                $category->setIconUrl($row['CategoryIconUri']);
                $category->setCreatedOn(time());
                $category->setCreatedBy($this->getUser());

                $em->persist($category);
                $em->flush();
                foreach($row['Dialogues'] as $item){
                    $dialogue = new FfDialogues();
                    $dialogue->setName($item['Name']);
                    $dialogue->setContentUrl($item['ContentUrl']);
                    $dialogue->setCategory($category);
                    $dialogue->setContent('');
                    $dialogue->setIconUrl('');
                    $dialogue->setAudioUrl('');
                    $dialogue->setCreatedOn(time());
                    $dialogue->setCreatedBy($this->getUser());
                    $em->persist($dialogue);
                    $em->flush();
                }

            }

            $em->getConnection()->commit();

            $this->container->get('session')->getFlashBag()
                ->add(
                    'success',
                    'All dialogues have been saved'
                );

            return true;

        } catch(Exception $e){

            $em->getConnection()->rollback();
            $this->container->get('session')->getFlashBag()
                ->add(
                    'error',
                    $e->getMessage()
                );

            return false;
        }

    }
}