<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 8/23/16
 * Time: 10:56 AM
 */

namespace AppBundle\Controller\Pages;


use AppBundle\Entity\Photos;
use AppBundle\Service\Jobs\DownloadImage;
use AppBundle\Service\Jobs\DownloadMedia;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class JobsController extends Controller
{
    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("downloadDialogueMedia")
     */

    public function downloadDialogueMedia(Request $request){

        $result = $this->get('download.media')->download(DownloadMedia::DIALOGUE_TABLE);

        return new JsonResponse($result);

    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("downloadDialogueImage")
     */

    public function downloadDialogueImage(Request $request){

        $result = $this->get('download.image')->download(DownloadImage::DIALOGUE_TABLE);

        return new JsonResponse($result);

    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("ipLocationFinder")
     */

    public function ipLocationFinderAction(Request $request) {

        $rqID = $this->get('app.ip_location_finder')->findLastRequestWithNoIPAddressInfos();
        $response = new JsonResponse();
        $response->setCharset("utf-8");
        $response->headers->set('Content-Type', 'application/json; charset=utf-8');
        $data['Request_ID'] = $rqID;
        try{
            $response->setData($data);
        } catch(\Exception $e){
            $data['Msg'] = $e->getMessage();

        }

        return $response;

    }

    /**
     *
     * @Method({"GET", "POST"})
     * @Route("/ajax/snippet/image/send", name="ajax_snippet_image_send")
     */
    public function ajaxSnippetImageSendAction(Request $request)
    {
        $em = $this->container->get("doctrine.orm.default_entity_manager");

        $document = new Photos();
        $media = $request->files->get('file');

        $document->setFile($media);
        $document->setPath($media->getPathName());
        $document->setName($media->getClientOriginalName());
        $em->persist($document);
        $em->flush();

        //infos sur le document envoyé
        //var_dump($request->files->get('file'));die;
        return new JsonResponse(array('success' => true));
    }
}