<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 6/7/16
 * Time: 5:07 PM
 */

namespace AppBundle\Controller\Pages;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ImportController extends Controller
{

    const DATA_CATEGORIES = 'categories';
    const DATA_DIALOGUES = 'dialoges';

    public function importDataAction(Request $request){

        $type = $request->request->get('type');
        $jsonData = $request->request->get('json_data');

        if($type !== null && trim($jsonData) !== '')
        {
            $this->save($type, $jsonData);
        }

        $data = json_decode(utf8_encode($jsonData), false, 100);

       return $this->render('AppBundle:pages:import-data.html.twig', array(
            'data' => array(
                'request' => $request,
                'data' => $data,
            )
       ));

    }


    private function save($type, $jsonData){


    }





}