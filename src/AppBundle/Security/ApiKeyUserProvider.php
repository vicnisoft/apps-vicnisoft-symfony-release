<?php

/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 5/31/16
 * Time: 3:46 PM
 */

namespace AppBundle\Security;

use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use AppBundle\Entity\Users;

class ApiKeyUserProvider implements UserProviderInterface
{

    protected $doctrine;

    public function __construct( $doctrine){

        $this->doctrine = $doctrine;
    }

    public function getUserForApiKey($apiKey)
    {
       return $this->doctrine->getRepository('AppBundle:Users')->findOneByApiKey($apiKey);

    }


    public function loadUserByUsername($username)
    {
        return new Users();
    }

    public function refreshUser(UserInterface $user)
    {
        // this is used for storing authentication in the session
        // but in this example, the token is sent in each request,
        // so authentication can be stateless. Throwing this exception
        // is proper to make things stateless
        return $user;
//        throw new UnsupportedUserException();
    }

    public function supportsClass($class)
    {
        return 'AppBundle\Entity\Users' === $class;
    }
}
