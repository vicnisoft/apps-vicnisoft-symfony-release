<?php

/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 5/31/16
 * Time: 3:44 PM
 */

namespace AppBundle\Security;

use AppBundle\Entity\Requests;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\PreAuthenticatedToken;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\Security\Http\Authentication\SimplePreAuthenticatorInterface;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Users;

class ApiKeyAuthenticator implements SimplePreAuthenticatorInterface, AuthenticationFailureHandlerInterface, AuthenticationSuccessHandlerInterface
{
    protected $doctrine;

    /** @var  Users */

    private $user;

    public function __construct( $doctrine){

        $this->doctrine = $doctrine;
    }

    public function createToken(Request $request, $providerKey)
    {
        // look for an apikey query parameter

        $apiKey = $request->get('api_key');

        // or if you want to use an "api_key" header, then do something like this:
        // $apiKey = $request->headers->get('apikey');

        if (!$apiKey) {

            throw new AuthenticationException('No API Key found', 401);
        }

        return new PreAuthenticatedToken(
            'anon.',
            $apiKey,
            $providerKey
        );
    }

    public function authenticateToken(TokenInterface $token, UserProviderInterface $userProvider, $providerKey)
    {
        if (!$userProvider instanceof ApiKeyUserProvider) {
            throw new \InvalidArgumentException(
                sprintf(
                    'The user provider must be an instance of ApiKeyUserProvider (%s was given).',
                    get_class($userProvider)
                ),
                400
            );
        }

        $apiKey = $token->getCredentials();

        $user = $userProvider->getUserForApiKey($apiKey);

        $this->user = $user;

        if(!$user){
            throw new AuthenticationException('API Key does not exist', 401);
        }

        return new PreAuthenticatedToken(
            $user,
            $apiKey,
            $providerKey,
            $user->getRoles()
        );
    }

    public function supportsToken(TokenInterface $token, $providerKey)
    {
        return $token instanceof PreAuthenticatedToken && $token->getProviderKey() === $providerKey;
    }



    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {

        $response = new JsonResponse();
        $response->setCharset('utf-8');
        $response->headers->set('Content-Type', 'application/json; charset=utf-8');

        $response->setData([
            'Success' => false,
            'ErrorMessage' => $exception->getMessage(),
        ]);

        return $response;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token){

        $em = $this->doctrine->getManager();
        try{

            $em->getConnection()->beginTransaction();
            $rq = new Requests();
            $rq->setUser($this->user);
            $rq->setCreatedOn(time());
            $rq->setHttpUserAgent($_SERVER['HTTP_USER_AGENT']);
            $rqUri = $request->getRequestUri();
            $rq->setRequestUri($rqUri);
            $rqContent = $request->getContent();
            $rq->setContent($rqContent);
//            $query = $request->getQueryString();
            $clientIp = $request->getClientIp();
            $rq->setClientIp($clientIp);

            $em->persist($rq);
            $em->flush();
            $em->getConnection()->commit();


        } catch(\Exception $e){
            $em->getConnection()->rollback();
        }

    }

}