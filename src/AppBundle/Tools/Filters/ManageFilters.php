<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 8/11/16
 * Time: 10:03 PM
 */

namespace AppBundle\Tools\Filters;

use AppBundle\Controller\Helpers\Tools\UserInfos;
use Doctrine\ORM\EntityManager;
use AppBundle\Tools\Filters\Filter;
use AppBundle\Tools\QueryHelpers\QueryBuilderTool;
use Doctrine\DBAL\Statement;
use AppBundle\Service\Settings\SettingsManager;
use PDO;

abstract class ManageFilters
{
    /** @var EntityManager */
    protected $doctrine;

    protected $rights;

    protected $sections = array();
    /** @var SettingsManager */
    protected $settings = null;

    abstract public function setFilters(UserInfos $userInfos);


    public function __construct($doctrine, array $rights, $settings = null)
    {
        $this->doctrine = $doctrine;
        $this->rights = $rights;
        $this->settings = $settings;
    }

    protected function getFiltersValues($filters) {
        /** @var $filter Filter */
        foreach ($filters as $filter) {
            $builder = $filter->getQueryBuilder();
            if (isset($builder)) {
                $values = $this->executeQuery($builder);

                if($filter->getName() == 'roles') {
                    foreach($values as $key => $value) {
                        $values[$key]['text'] = $value['text'];
                    }
                }

                $filter->setSelectValues($values);
            }
        }
    }

    /**
     * Execute sql queries
     * @param QueryBuilderTool $sqlBuilder
     * @return array
     */
    protected function executeQuery($sqlBuilder) {
        $result = array();
        $sql = $sqlBuilder->getQuery();
        $pdo = $this->doctrine->getConnection();
        if (!empty($sql)) {
            /* @var $sth Statement */
            $sth = $pdo->prepare($sql);
            $sqlBuilder->bindParameters($sth);
            $sth->execute();
            $result = $sth->fetchAll(PDO::FETCH_ASSOC);
        }
        return $result;
    }

    protected function addSection($section) {
        $this->sections[] = $section;
    }

    protected function setSections($sections) {
        $this->sections = $sections;
    }

    public function getFiltersBySections($filters) {
        $default = 'Defaults';
        $othersSections = array();
        $filtersBySection = array();
        foreach($filters as $filter) {
            // set a default section if no section
            if($filter->getSection() == "") {
                $filter->setSection($default);
            }
            // if section was forgotten
            if(!in_array($filter->getSection(), $this->sections) && !in_array($filter->getSection(), $othersSections) ) {
                $othersSections[] = $filter->getSection();
            }
        }
        $this->sections = array_merge($this->sections, $othersSections);
        foreach($this->sections as $section) {
            if(!isset($filtersBySection[$section]) ){
                $filtersBySection[$section] = array();
            }
            foreach($filters as $filter) {
                if($filter->getSection() == $section) {
                    $filtersBySection[$section][] = $filter;
                }
            }
        }

        return $filtersBySection;
    }

}