<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Projects
 *
 * @ORM\Table(name="projects", indexes={@ORM\Index(name="index_created_by_id", columns={"created_by_id"})})
 * @ORM\Entity
 */
class Projects
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=false)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="created_on", type="integer", nullable=false)
     */
    private $createdOn;

    /**
     * @var string
     *
     * @ORM\Column(name="project_icon", type="string", length=255, nullable=true)
     */
    private $projectIcon;

    /**
     * @var boolean
     *
     * @ORM\Column(name="recycled", type="boolean", nullable=false)
     */
    private $recycled = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="appstore_url", type="string", length=255, nullable=true)
     */
    private $appstoreUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="googleplay_url", type="string", length=255, nullable=true)
     */
    private $googleplayUrl;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=false)
     */
    private $active = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="pem_url_aspn", type="string", length=255, nullable=true)
     */
    private $pemUrlAspn;

    /**
     * @var string
     *
     * @ORM\Column(name="pass_pharse_aspn", type="string", length=25, nullable=true)
     */
    private $passPharseAspn;

    /**
     * @var \Users
     *
     * @ORM\ManyToOne(targetEntity="Users")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="created_by_id", referencedColumnName="id")
     * })
     */
    private $createdBy;


    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $userDevices;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->userDevices = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Projects
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Projects
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set createdOn
     *
     * @param integer $createdOn
     *
     * @return Projects
     */
    public function setCreatedOn($createdOn)
    {
        $this->createdOn = $createdOn;

        return $this;
    }

    /**
     * Get createdOn
     *
     * @return integer
     */
    public function getCreatedOn()
    {
        return $this->createdOn;
    }

    /**
     * Set projectIcon
     *
     * @param string $projectIcon
     *
     * @return Projects
     */
    public function setProjectIcon($projectIcon)
    {
        $this->projectIcon = $projectIcon;

        return $this;
    }

    /**
     * Get projectIcon
     *
     * @return string
     */
    public function getProjectIcon()
    {
        return $this->projectIcon;
    }

    /**
     * Set recycled
     *
     * @param boolean $recycled
     *
     * @return Projects
     */
    public function setRecycled($recycled)
    {
        $this->recycled = $recycled;

        return $this;
    }

    /**
     * Get recycled
     *
     * @return boolean
     */
    public function getRecycled()
    {
        return $this->recycled;
    }

    /**
     * Set appstoreUrl
     *
     * @param string $appstoreUrl
     *
     * @return Projects
     */
    public function setAppstoreUrl($appstoreUrl)
    {
        $this->appstoreUrl = $appstoreUrl;

        return $this;
    }

    /**
     * Get appstoreUrl
     *
     * @return string
     */
    public function getAppstoreUrl()
    {
        return $this->appstoreUrl;
    }

    /**
     * Set googleplayUrl
     *
     * @param string $googleplayUrl
     *
     * @return Projects
     */
    public function setGoogleplayUrl($googleplayUrl)
    {
        $this->googleplayUrl = $googleplayUrl;

        return $this;
    }

    /**
     * Get googleplayUrl
     *
     * @return string
     */
    public function getGoogleplayUrl()
    {
        return $this->googleplayUrl;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Projects
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set pemUrlAspn
     *
     * @param string $pemUrlAspn
     *
     * @return Projects
     */
    public function setPemUrlAspn($pemUrlAspn)
    {
        $this->pemUrlAspn = $pemUrlAspn;

        return $this;
    }

    /**
     * Get pemUrlAspn
     *
     * @return string
     */
    public function getPemUrlAspn()
    {
        return $this->pemUrlAspn;
    }

    /**
     * Set passPharseAspn
     *
     * @param string $passPharseAspn
     *
     * @return Projects
     */
    public function setPassPharseAspn($passPharseAspn)
    {
        $this->passPharseAspn = $passPharseAspn;

        return $this;
    }

    /**
     * Get passPharseAspn
     *
     * @return string
     */
    public function getPassPharseAspn()
    {
        return $this->passPharseAspn;
    }

    /**
     * Add userDevice
     *
     * @param \AppBundle\Entity\UsersDevices $userDevice
     *
     * @return Projects
     */
    public function addUserDevice(\AppBundle\Entity\UsersDevices $userDevice)
    {
        $this->userDevices[] = $userDevice;

        return $this;
    }

    /**
     * Remove userDevice
     *
     * @param \AppBundle\Entity\UsersDevices $userDevice
     */
    public function removeUserDevice(\AppBundle\Entity\UsersDevices $userDevice)
    {
        $this->userDevices->removeElement($userDevice);
    }

    /**
     * Get userDevices
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserDevices()
    {
        return $this->userDevices;
    }

    /**
     * Set createdBy
     *
     * @param \AppBundle\Entity\Users $createdBy
     *
     * @return Projects
     */
    public function setCreatedBy(\AppBundle\Entity\Users $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \AppBundle\Entity\Users
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }
}
