<?php


namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DialoguesReports
 *
 * @ORM\Table(name="dialogues_reports", indexes={@ORM\Index(name="index_dialogues_reports_users", columns={"user_id"}), @ORM\Index(name="index_dialogues_reports_dialogues", columns={"dialogue_id"})})
 * @ORM\Entity
 */
class DialoguesReports
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="reason", type="string", length=512, nullable=true)
     */
    private $reason;

    /**
     * @var integer
     *
     * @ORM\Column(name="active", type="integer", nullable=true)
     */
    private $active = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="created_on", type="integer", nullable=true)
     */
    private $createdOn;

    /**
     * @var integer
     *
     * @ORM\Column(name="updated_on", type="integer", nullable=true)
     */
    private $updatedOn;

    /**
     * @var \Dialogues
     *
     * @ORM\ManyToOne(targetEntity="Dialogues")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="dialogue_id", referencedColumnName="id")
     * })
     */
    private $dialogue;

    /**
     * @var \Users
     *
     * @ORM\ManyToOne(targetEntity="Users")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * DialoguesReports constructor.
     */
    public function __construct()
    {
        $this->createdOn = time();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getReason()
    {
        return $this->reason;
    }

    /**
     * @param string $reason
     */
    public function setReason($reason)
    {
        $this->reason = $reason;
    }

    /**
     * @return int
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param int $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @return int
     */
    public function getCreatedOn()
    {
        return $this->createdOn;
    }

    /**
     * @param int $createdOn
     */
    public function setCreatedOn($createdOn)
    {
        $this->createdOn = $createdOn;
    }

    /**
     * @return int
     */
    public function getUpdatedOn()
    {
        return $this->updatedOn;
    }

    /**
     * @param int $updatedOn
     */
    public function setUpdatedOn($updatedOn)
    {
        $this->updatedOn = $updatedOn;
    }

    /**
     * @return \Dialogues
     */
    public function getDialogue()
    {
        return $this->dialogue;
    }

    /**
     * @param \Dialogues $dialogue
     */
    public function setDialogue($dialogue)
    {
        $this->dialogue = $dialogue;
    }

    /**
     * @return \Users
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param \Users $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }






}

