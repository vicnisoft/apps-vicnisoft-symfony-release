<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FbCategoriesVideos
 *
 * @ORM\Table(name="fb_categories_videos", indexes={@ORM\Index(name="index_fb_categories_videos_categories", columns={"category_id"}), @ORM\Index(name="index_fb_categories_videos_video", columns={"video_id"})})
 * @ORM\Entity
 */
class FbCategoriesVideos
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \FbCategories
     *
     * @ORM\ManyToOne(targetEntity="FbCategories")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     * })
     */
    private $category;

    /**
     * @var \FbVideos
     *
     * @ORM\ManyToOne(targetEntity="FbVideos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="video_id", referencedColumnName="id")
     * })
     */
    private $video;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set category
     *
     * @param \AppBundle\Entity\FbCategories $category
     *
     * @return FbCategoriesVideos
     */
    public function setCategory(\AppBundle\Entity\FbCategories $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \AppBundle\Entity\FbCategories
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set video
     *
     * @param \AppBundle\Entity\FbVideos $video
     *
     * @return FbCategoriesVideos
     */
    public function setVideo(\AppBundle\Entity\FbVideos $video = null)
    {
        $this->video = $video;

        return $this;
    }

    /**
     * Get video
     *
     * @return \AppBundle\Entity\FbVideos
     */
    public function getVideo()
    {
        return $this->video;
    }
}
