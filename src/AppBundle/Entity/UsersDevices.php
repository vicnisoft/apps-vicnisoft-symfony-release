<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UsersDevices
 *
 * @ORM\Table(name="users_devices", indexes={@ORM\Index(name="index_users_devices_user", columns={"user_id"}), @ORM\Index(name="index_users_devices_device", columns={"device_id"}), @ORM\Index(name="index_users_devices_projects", columns={"project_id"})})
 * @ORM\Entity
 */
class UsersDevices
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="last_active", type="integer", nullable=false)
     */
    private $lastActive;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=false)
     */
    private $active = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="count", type="integer", nullable=false)
     */
    private $count = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="release_version", type="string", length=25, nullable=true)
     */
    private $releaseVersion;

    /**
     * @var string
     *
     * @ORM\Column(name="build_version", type="string", length=25, nullable=true)
     */
    private $buildVersion;

    /**
     * @var string
     *
     * @ORM\Column(name="device_identifiter", type="string", length=255, nullable=true)
     */
    private $deviceIdentifiter;

    /**
     * @var \Devices
     *
     * @ORM\ManyToOne(targetEntity="Devices")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="device_id", referencedColumnName="id")
     * })
     */
    private $device;

    /**
     * @var \Projects
     *
     * @ORM\ManyToOne(targetEntity="Projects")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="project_id", referencedColumnName="id")
     * })
     */
    private $project;

    /**
     * @var \Users
     *
     * @ORM\ManyToOne(targetEntity="Users")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lastActive
     *
     * @param integer $lastActive
     *
     * @return UsersDevices
     */
    public function setLastActive($lastActive)
    {
        $this->lastActive = $lastActive;

        return $this;
    }

    /**
     * Get lastActive
     *
     * @return integer
     */
    public function getLastActive()
    {
        return $this->lastActive;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return UsersDevices
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set count
     *
     * @param integer $count
     *
     * @return UsersDevices
     */
    public function setCount($count)
    {
        $this->count = $count;

        return $this;
    }

    /**
     * Get count
     *
     * @return integer
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * Set deviceIdentifiter
     *
     * @param string $deviceIdentifiter
     *
     * @return UsersDevices
     */
    public function setDeviceIdentifiter($deviceIdentifiter)
    {
        $this->deviceIdentifiter = $deviceIdentifiter;

        return $this;
    }

    /**
     * Get deviceIdentifiter
     *
     * @return string
     */
    public function getDeviceIdentifiter()
    {
        return $this->deviceIdentifiter;
    }

    /**
     * Set device
     *
     * @param \AppBundle\Entity\Devices $device
     *
     * @return UsersDevices
     */
    public function setDevice(\AppBundle\Entity\Devices $device = null)
    {
        $this->device = $device;

        return $this;
    }

    /**
     * Get device
     *
     * @return \AppBundle\Entity\Devices
     */
    public function getDevice()
    {
        return $this->device;
    }

    /**
     * Set project
     *
     * @param \AppBundle\Entity\Projects $project
     *
     * @return UsersDevices
     */
    public function setProject(\AppBundle\Entity\Projects $project = null)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return \AppBundle\Entity\Projects
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * @return string
     */
    public function getReleaseVersion()
    {
        return $this->releaseVersion;
    }

    /**
     * @param string $releaseVersion
     */
    public function setReleaseVersion($releaseVersion)
    {
        $this->releaseVersion = $releaseVersion;
    }

    /**
     * @return string
     */
    public function getBuildVersion()
    {
        return $this->buildVersion;
    }

    /**
     * @param string $buildVersion
     */
    public function setBuildVersion($buildVersion)
    {
        $this->buildVersion = $buildVersion;
    }


    /**
     * Set user
     *
     * @param \AppBundle\Entity\Users $user
     *
     * @return UsersDevices
     */
    public function setUser(\AppBundle\Entity\Users $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\Users
     */
    public function getUser()
    {
        return $this->user;
    }
}
