<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 5/31/16
 * Time: 11:50 PM
 */

namespace AppBundle\Entity;


use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class UsersRepository extends EntityRepository implements UserLoaderInterface
{

    /** UserProviderInterface  */

    public function loadUserByUsername($username) {

        $user = $this->findOneBy(array("username" => $username));

        return $user;
    }

}