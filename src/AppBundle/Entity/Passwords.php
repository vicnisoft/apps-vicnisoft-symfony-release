<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 5/31/16
 * Time: 5:05 PM
 */

namespace AppBundle\Entity;

use Rollerworks\Bundle\PasswordStrengthBundle\Validator\Constraints as RollerworksPassword;

class Passwords {

    private $salt;

    /**
     * @RollerworksPassword\PasswordRequirements(requireLetters=true, requireNumbers=true, minLength=8)
     */
    private $plainpassword;


    protected $password;

    public function getPassword() {

    }

    public function getPlainPassword() {
        //return $this->getUsers()->getPassword();
        return $this->plainpassword;
    }

    public function setPlainPassword($password) {
        if(isset($password) && !empty($password) ) {
            $this->plainpassword = $password;
            $db_password = $this->getDBPassword($password);
            $this->setPassword($db_password);
        }
    }

    public function getDBPassword($password) {
        $salt = $this->genRandomPassword(32);
        $encrypted = ($salt) ? md5($password . $salt) : md5($password);
        return "$encrypted:$salt";
    }

    private function genRandomPassword($length = 8)
    {
        $salt = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $base = strlen($salt);
        $makepass = '';

        /*
         * Start with a cryptographic strength random string, then convert it to
         * a string with the numeric base of the salt.
         * Shift the base conversion on each character so the character
         * distribution is even, and randomize the start shift so it's not
         * predictable.
         */
        $random = openssl_random_pseudo_bytes($length + 1);
        $shift = ord($random[0]);
        for ($i = 1; $i <= $length; ++$i)
        {
            $makepass .= $salt[($shift + ord($random[$i])) % $base];
            $shift += ord($random[$i]);
        }

        return $makepass;
    }

    private function generateSalt($length = 8)
    {
        $salt = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $base = strlen($salt);
        $makepass = '';

        /*
         * Start with a cryptographic strength random string, then convert it to
         * a string with the numeric base of the salt.
         * Shift the base conversion on each character so the character
         * distribution is even, and randomize the start shift so it's not
         * predictable.
         */
        $random = openssl_random_pseudo_bytes($length + 1);
        $shift = ord($random[0]);
        for ($i = 1; $i <= $length; ++$i)
        {
            $makepass .= $salt[($shift + ord($random[$i])) % $base];
            $shift += ord($random[$i]);
        }

        return $makepass;
    }

    /**
     * @inheritDoc
     */
    public function getSalt()
    {
        $parts = explode(":", $this->password);
        $this->salt = (is_array($parts) && count($parts) > 1) ?  $parts[1] : null;
        return $this->salt;
    }
}

