<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DownloadImageJob
 *
 * @ORM\Table(name="download_image_job")
 * @ORM\Entity
 */
class DownloadImageJob
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="target_id", type="integer", nullable=false)
     */
    private $targetId;

    /**
     * @var string
     *
     * @ORM\Column(name="target_table", type="string", length=255, nullable=false)
     */
    private $targetTable;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="local_url", type="string", length=255, nullable=false)
     */
    private $localUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="remote_url", type="string", length=255, nullable=false)
     */
    private $remoteUrl;

    /**
     * @var integer
     *
     * @ORM\Column(name="active", type="integer", nullable=true)
     */
    private $active = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="created_on", type="integer", nullable=true)
     */
    private $createdOn;

    /**
     * @var integer
     *
     * @ORM\Column(name="updated_on", type="integer", nullable=true)
     */
    private $updatedOn;

    /**
     * DownloadImageJob constructor.
     * @param int $targetId
     * @param string $targetTable
     * @param string $remoteUrl
     */
    public function __construct($targetId, $targetTable, $remoteUrl)
    {
        $this->targetId = $targetId;
        $this->targetTable = $targetTable;
        $this->remoteUrl = $remoteUrl;
        $this->createdOn = time();
    }


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getTargetId()
    {
        return $this->targetId;
    }

    /**
     * @param int $targetId
     */
    public function setTargetId($targetId)
    {
        $this->targetId = $targetId;
    }

    /**
     * @return string
     */
    public function getTargetTable()
    {
        return $this->targetTable;
    }

    /**
     * @param string $targetTable
     */
    public function setTargetTable($targetTable)
    {
        $this->targetTable = $targetTable;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getLocalUrl()
    {
        return $this->localUrl;
    }

    /**
     * @param string $localUrl
     */
    public function setLocalUrl($localUrl)
    {
        $this->localUrl = $localUrl;
    }

    /**
     * @return string
     */
    public function getRemoteUrl()
    {
        return $this->remoteUrl;
    }

    /**
     * @param string $remoteUrl
     */
    public function setRemoteUrl($remoteUrl)
    {
        $this->remoteUrl = $remoteUrl;
    }

    /**
     * @return int
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param int $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @return int
     */
    public function getCreatedOn()
    {
        return $this->createdOn;
    }

    /**
     * @param int $createdOn
     */
    public function setCreatedOn($createdOn)
    {
        $this->createdOn = $createdOn;
    }

    /**
     * @return int
     */
    public function getUpdatedOn()
    {
        return $this->updatedOn;
    }

    /**
     * @param int $updatedOn
     */
    public function setUpdatedOn($updatedOn)
    {
        $this->updatedOn = $updatedOn;
    }



}

