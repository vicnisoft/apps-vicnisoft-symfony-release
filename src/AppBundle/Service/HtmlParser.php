<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 6/4/16
 * Time: 10:32 PM
 */

namespace AppBundle\Service;

use PHPHtmlParser\Dom;

class HtmlParser
{


    public function parserCategories($uri){
        $result = array();

        try {
            $dom = new Dom();
            $dom->loadFromUrl($uri);
//        $html = $dom->outerHtml;
            $groupsNode = $dom->find('.fusion-column-wrapper');
            foreach($groupsNode as $group){
                $childDom = new Dom();

                $childDom->loadFromUrl($group->innerHtml);
                $imgSrc = $childDom->find('img')[0]->getAttribute('src');
                $cateNodes = $childDom->find('strong');
                $cateName = $imgSrc;
                if($cateNodes != null && count($cateNodes) > 0){
                    $cateName = $cateNodes[0]->text;
                }

                $aNodes = $childDom->loadFromUrl($group->innerHtml)->find('a');

                $dialogues = array();
                foreach($aNodes as $aNode){
                    $dialogues[] = array(
                        'Name' => $aNode->text,
                        'ContentUrl' => $aNode->getAttribute('href'),
                    );
                }

                $result[] = array(
                    'CategoryName' => $cateName ,
                    'CategoryIconUri' => $imgSrc,
                    'Dialogues' => $dialogues,
                );

            }

        } catch(\Exception $e){

        }

        return $result;
    }


    public function parserDialogies($uri){

        $data = array();
        try {
            $dom = new Dom();
            $dom->loadFromUrl($uri);
            $title = $dom->find('.entry-title')[0];

            $data['Title'] = $title->text;
            $contentNode = $dom->find('.post-content');
            $childDom = new Dom();
            $childDom->loadFromUrl($contentNode->innerHtml);
            $imgNode = $childDom->find('img')[0];

            $imageThumb = array();
            if($imgNode !== null){
                $imageThumb['Uri'] = $imgNode->getAttribute('src');
                $imageThumb['Height'] = $imgNode->getAttribute('height');
                $imageThumb['Width'] = $imgNode->getAttribute('width');
            }
            $data['Image'] = $imageThumb;
            $audioNode = $childDom->find('embed')[0];
            if($audioNode !== null){
                $data['AudioUri'] = str_replace("audioUrl=","",$audioNode->getAttribute('flashvars'));
            }

            $html2Text = new Html2Text($contentNode->innerHtml);

            $data['Content'] = $html2Text->getText();
        } catch(\Exception $e){

        }


        return $data;
    }


}