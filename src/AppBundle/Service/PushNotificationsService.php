<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 6/8/16
 * Time: 8:48 AM
 */

namespace AppBundle\Service;


use AppBundle\Models\PushNotification;
use Doctrine\Bundle\DoctrineBundle\Registry;
use RMS\PushNotificationsBundle\Message\iOSMessage;
use Symfony\Component\DependencyInjection\ContainerInterface;

class PushNotificationsService
{

    private $container ;
    private $doctrine;

    public function __construct(ContainerInterface $container, Registry $doctrine){
        $this->container = $container;
        $this->doctrine = $doctrine;
    }

    public function sendIosMessage(PushNotification $notification){

        $pemFileName = $notification->getProject()->getPemUrlAspn();
        $passPharse = $notification->getProject()->getPassPharseAspn();
        $dir = $this->container->getParameter('pem_dir');
        $pemPath = $dir . $pemFileName;
//        $pemContent = file_get_contents($pemPath);

//        $this->container->get('rms_push_notifications')->setAPNSPemAsString($pemContent, $passPharse);
//        $pemPath2 = $this->container->getParameter('pem_path');

        $result = array();
        if($notification->getDeviceOnly()){
            if($this->sendSingleMessage($notification->getDeviceId(), $notification->getMessage(),$pemPath, $passPharse ) ){
                $result[] = $notification->getDeviceId();
            } else {
//                $result[] = $pemPath;
//                $result[] = $pemPath2;
                $result[] = "fail ". $notification->getDeviceId();
            }
        } else {

            $devices = $notification->getProject()->getUserDevices();
            foreach($devices as $device){
                if($this->sendSingleMessage($device->getDeviceIdentifiter(), $notification->getMessage(), $pemPath, $passPharse ) ) {
                    $result[] = $device->getDeviceIdentifiter();
                }
            }

        }

        return $result;

    }

    private function sendSingleMessage($identifier, $msg, $pemPath, $passPharse){
        try{
            $message = new iOSMessage();
            $message->setMessage($msg);
            $message->setDeviceIdentifier($identifier);
//            $pemPath = $this->container->getParameter('pem_path');
//            $passPharse = $this->container->getParameter('apsn_pwd');
            $pemContent = file_get_contents($pemPath);
            $this->container->get('rms_push_notifications')->setAPNSPemAsString($pemContent, $passPharse);
            $this->container->get('rms_push_notifications')->send($message);

        } catch(\Exception $e){
            return false;
        }

        return true;
    }
}