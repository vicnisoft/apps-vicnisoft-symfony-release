<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 6/21/16
 * Time: 1:34 PM
 */

namespace AppBundle\Service;



use AppBundle\Entity\FbSchedules;
use AppBundle\Entity\FbVideos;
use AppBundle\Service\Settings\SettingsManager;
use Doctrine\Bundle\DoctrineBundle\Registry;

class FootballHightlightVideos
{

    private $doctrine;
    private $videosPath;

    private $finds = [
        'Football Highlights Videos'
    ];

    private $replace = 'Video bóng đá Free';

    public function __construct(Registry $doctrine, $videoPath = ''){

        $this->doctrine = $doctrine;
        $this->videosPath = $videoPath;
    }

    private $serverUri = "https://graph.facebook.com/v2.6/1540303759612582/videos?fields=source%2Cdescription%2Ccontent_category%2Ccreated_time&limit=100&access_token=985784828172476|jXCh5PVxVtgTnVQuOpVOLD0Gu4w" ;

    public function getVideos(){

        $curl = curl_init($this->serverUri);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        curl_close($curl);
        return $this->parserResult($curl_response);

    }

    private function parserResult($result){

        if (($table = json_decode(utf8_encode($result), false, 100)) === NULL) {

            $this->saveSchedule(false);
            return false;

        } else {
            $ids = array();
            if($table->data) {
                foreach($table->data as $row){

                    $video = $this->saveVideo($row->id, $row->source, $row->description, $row->created_time);
                    if($video !== null){
                        $ids[] = $video->getId();
                        $downloadThread = new Download($this->videosPath);
                        $downloadThread->downloadVideo($video->getMediaUrl(),$video->getVideoUrl());
                    }
                }
                $success = $this->saveSchedule(true);

                return array(
                    'Success' => $success,
                    'Count' => count($ids),
                    'Ids' => $ids,
                );
            }

        }

        $this->saveSchedule(false);
        return false;
    }


    private function saveSchedule($success = true){

        $em = $this->doctrine->getManager();

        try{
            $em->getConnection()->beginTransaction();

            $schedule = new FbSchedules();

            $schedule->setServerName($this->serverUri);
            $schedule->setStatus($success);

            $em->persist($schedule);
            $em->flush();
            $em->getConnection()->commit();

            return true;

        } catch(Exception $e){

            $em->getConnection()->rollback();
            return false;
        }
    }

    private function saveVideo($id, $source, $description, $created_time){

        $em = $this->doctrine->getManager();

        try{

            $video = $this->doctrine->getRepository('AppBundle:FbVideos')->findOneBy(array(
                'name' => $id,
            ));
            $em->getConnection()->beginTransaction();
            if($video == null){
                $video = new FbVideos();
                $video->setName($id);
                $video->setUpdatedOn(time());
            }
            foreach($this->finds as $find){
                $description = str_replace($find,$this->replace, $description);
            }
            $video->setApproved(SettingsManager::APPROVAL_STATUS_APPROVED);
            $video->setShortDescription($description);
            $video->setActive(true);
            $video->setType(SettingsManager::VIDEO_TYPE_MP4);
            $video->setVideoUrl($source);
            $timestamp = strtotime($created_time);
            $video->setCreatedOn($timestamp);

            $mediaUrl = $id . '_' . $timestamp . '.mp4';

            $video->setMediaUrl($mediaUrl);

            $em->persist($video);
            $em->flush();
            $em->getConnection()->commit();

            return $video;

        } catch(Exception $e){

            $em->getConnection()->rollback();
            return null;
        }
    }
}


class Download {

    private $path ;

    public function __construct($path){

        $this->path = $path;
    }

    function downloadVideo($fileName = '', $url = null){

        $filePath = $this->path . "/" . $fileName ;
        if(file_exists($filePath) == false){
            file_put_contents($filePath, file_get_contents($url));
        }

    }
}