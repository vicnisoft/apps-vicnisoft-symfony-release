<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 6/2/16
 * Time: 4:10 PM
 */

namespace AppBundle\Service;

use Buzz\Browser;
use MatthiasNoback\MicrosoftOAuth\AccessTokenProvider;
use MatthiasNoback\MicrosoftTranslator\MicrosoftTranslator;
use MatthiasNoback\MicrosoftTranslator\ApiCall\Response\TranslationMatch;

use MatthiasNoback\MicrosoftOAuth\AccessTokenCache;
use Doctrine\Common\Cache\ArrayCache;

class MicrosoftTranslator
{
    private $translator;
    const  CLIENT_ID = 'VnsTranslator';
    const  CLIENT_SECRET = 'zI5zWSrtsAg8NNo+9QRuWQWmKAY7xWArlkhQ3KmFAm4=';

    /**
     * https://github.com/matthiasnoback/microsoft-translator
     *
     */

    public function __construct(){
        $browser = new Browser();
        $accessTokenProvider = new AccessTokenProvider($browser, SELF::CLIENT_ID, SELF::CLIENT_SECRET);
        /***
         *
         * Each call to the translator service is preceded by a call to Microsoft's OAuth server.
         * Each access token however, may be cached for 10 minutes, so you should also use the built-in AccessTokenCache
         */
        $cache = new ArrayCache();
        $accessTokenCache = new AccessTokenCache($cache);
        $accessTokenProvider->setCache($accessTokenCache);

        $this->translator = new MicrosoftTranslator($browser, $accessTokenProvider);

    }


    public function translate($intputString, $toLangue = 'vi' , $fromLangue = 'en'){
        return  $this->translator->translate($intputString, $toLangue, $fromLangue);
    }

    public function translateMultiple($intputString, $toLangue = 'vi' , $fromLangue = 'en'){

        $matches = $this->translator->getTranslations($intputString, $toLangue, $fromLangue);

        $result = array();
        /** @var  TranslationMatch $match */
        foreach ($matches as $match) {
            // $match is an instance of MatthiasNoback\MicrosoftTranslator\ApiCall\TranslationMatch
            $degree = $match->getDegree();
            $translatedText = $match->getTranslatedText();
            $result['degree'] = $degree;
            $result['translatedText'] = $translatedText;
        }

        return $result;
    }

}