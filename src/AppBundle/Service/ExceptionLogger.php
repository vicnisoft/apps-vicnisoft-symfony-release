<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 6/15/16
 * Time: 4:36 PM
 */

namespace AppBundle\Service;

use Monolog\Logger;

class ExceptionLogger
{
    /** @var Logger  */

    private $logger;

    /**
     * @param Logger $logger
     *
     */

    public function __construct(Logger $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param array $messages
     *
     */
    public function debug(array $messages){

        foreach($messages as $key => $msg){
            $this->logger->debug($key." ".$msg);
        }

    }
    public function error($message){
        $this->logger->error($message);
    }
}
