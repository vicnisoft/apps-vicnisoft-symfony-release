<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 8/23/16
 * Time: 10:21 AM
 */

namespace AppBundle\Service\Jobs;


class DownloadTask
{
    private $path ;

    public function __construct($path){

        $this->path = $path;
    }

    function download($fileName = '', $url = null){

        if($url == null) {
            return null;
        }

        $filePath = $this->path . "/" . time() ."_". $fileName ;
        if(file_exists($filePath) == false){
            file_put_contents($filePath, file_get_contents($url));
        }

        return $filePath;

    }

}