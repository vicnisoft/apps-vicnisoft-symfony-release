<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 8/23/16
 * Time: 10:02 AM
 */

namespace AppBundle\Service\Jobs;


use AppBundle\Entity\Dialogues;
use AppBundle\Entity\DownloadImageJob;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\QueryBuilder;

class DownloadImage
{

    const DIALOGUE_TABLE = 'Dialogues';


    /** @var   */
    private $doctrine;

    /** @var string */

    private $rootPath;

    /**
     * DownloadImage constructor.
     */
    public function __construct(Registry $doctrine, $rootPath)
    {
        $this->doctrine = $doctrine;
        $this->rootPath = $rootPath;

    }


    public function download($targetTable){

        $returnMessage = "Download ". $targetTable;

        $repository = $this->doctrine
            ->getRepository('AppBundle:DownloadImageJob');
        $query = $repository->createQueryBuilder('dij')
            ->where('dij.targetTable = :targetTable')
            ->setParameter('targetTable', $targetTable)
            ->orderBy('dij.id', 'DESC')
            ->getQuery();

        $imagesJobs = $query->getResult();

        if($targetTable == SELF::DIALOGUE_TABLE) {

            $maxId = -1;

            if(is_array($imagesJobs) && count($imagesJobs) > 0) {
                $maxId = $imagesJobs[0]->getTargetId();
            }

            $repository = $this->doctrine
                ->getRepository('AppBundle:Dialogues');

            /** @var QueryBuilder $query */

            $qb = $repository->createQueryBuilder('d')
                ->setParameter('maxId', $maxId)
                ->orderBy('d.id', 'ASC');


            $qb->add('where',
                $qb->expr()->andX(
                    $qb->expr()->gt('d.id', ':maxId'),
                    $qb->expr()->isNotNull('d.iconUrl')
                )
            );

            $query = $qb->getQuery();

            $dialogues = $query->setMaxResults(1)->getOneOrNullResult();

            if($dialogues) {
                return $this->downloadDialogueImage($dialogues);
            }

        }

        return $returnMessage;

    }


    private function downloadDialogueImage(Dialogues $dialogues){

        $resultId = -1;
        $imageURL = $dialogues->getIconUrl();
        $splits = explode('/', trim($imageURL));
        if(count($splits) > 0) {
            $fileName = $splits[count($splits) - 1];

            $download = new DownloadTask($this->rootPath);
            $path = $download->download($fileName, $imageURL);
            if($path == null) {
                $path = '';
            }

            try {
                $imageJob = new DownloadImageJob($dialogues->getId(), SELF::DIALOGUE_TABLE, $imageURL);
                $imageJob->setLocalUrl($path);
                $imageJob->setName($fileName);

                $this->doctrine->getManager()->persist($imageJob);
                $this->doctrine->getManager()->flush();

                $resultId = $imageJob->getId();

            } catch (\Exception $e){

                return $e->getMessage();
            }
        }


        return $resultId;

    }
}