<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 8/23/16
 * Time: 4:22 PM
 */

namespace AppBundle\Service\Jobs;



use AppBundle\Entity\Requests;
use Doctrine\Bundle\DoctrineBundle\Registry;


class IPLocationFinder
{

    private  $doctrine;

    public function __construct(Registry $doctrine){

        $this->doctrine = $doctrine;
    }

    public function findLastRequestWithNoIPAddressInfos(){

        $rqId = -1;
        if ($log = $this->doctrine->getRepository("AppBundle:Requests")->findOneBy(array(
            'ipChecked' => null,
        )) ) {

            $rqId = $log->getId();
            $em = $this->doctrine->getManager();
            try {
                $em->getConnection()->beginTransaction();
                $log->setIpChecked(true);
                $this->findIPWithRequests($log);
                $em->persist($log);
                $em->flush();
                $em->getConnection()->commit();

            } catch(\Exception $e){
                $em->getConnection()->rollback();
            }
        }
        return $rqId;
    }

    public function findIPWithRequests(Requests &$requests) {

        if($ipAddress = $requests->getClientIp()) {

            $log = $this->doctrine->getRepository("AppBundle:Requests")->findOneBy(array(
                'clientIp' => $ipAddress,
                'ipChecked' => true,
            ));

            if($log  && $log->getCountryName() ) {
                $requests->setCountryCode($log->getCountryCode());
                $requests->setCountryName($log->getCountryName());
                $requests->setRegionCode($log->getRegionCode());
                $requests->setRegionName($log->getRegionName());
                $requests->setCity($log->getCity());
                $requests->setTimeZone($log->getTimeZone());
                $requests->setLatitude($log->getLatitude());
                $requests->setLongitude($log->getLongitude());
                return;
            }

            $serviceURL = "freegeoip.net/json/".$ipAddress;
            $curl = curl_init($serviceURL);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($curl);
            curl_close($curl);

            try {
                if ($json = json_decode(utf8_encode($response), false, 100)){
                    $requests->setCountryCode($json->country_code);
                    $requests->setCountryName($json->country_name);
                    $requests->setRegionCode($json->region_code);
                    $requests->setRegionName($json->region_name);
                    $requests->setCity($json->city);
                    $requests->setTimeZone($json->time_zone);
                    $requests->setLatitude($json->latitude);
                    $requests->setLongitude($json->longitude);

                }
            } catch (\Exception $e){

            }

        }

    }
}