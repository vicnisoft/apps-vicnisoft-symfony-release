<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 7/6/16
 * Time: 2:41 PM
 */

namespace AppBundle\Service;

use AppBundle\Entity\Dialogues;
use AppBundle\Entity\Categories;
use AppBundle\Entity\Users;
use AppBundle\Service\Settings\SettingsManager;
use PHPHtmlParser\Dom;
use Exception;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class FrancaisFacile
{

    private $doctrine;
    private $mediaPath;

    private $tokenStore;

    public function __construct(Registry $doctrine, $mediaPath = '', TokenStorage $tokenStore)
    {

        $this->doctrine = $doctrine;
        $this->mediaPath = $mediaPath;
        $this->tokenStore = $tokenStore;
    }

    public function getCategories($uri, $type = SettingsManager::MEDIA_TYPE_AUDIO){


        if($type == SettingsManager::MEDIA_TYPE_TEXT){

            return $this->getTextCategories($uri);
        }


        $result = array();

        try {
            $dom = new Dom();
            $dom->loadFromUrl($uri);

            $groupsNode = $dom->find('.fusion-column-wrapper');

            foreach($groupsNode as $group){

                try{
                    $childDom = new Dom();

                    $childDom->load($group->innerHtml);

                    $imgSrc = $childDom->find('img')[0]->getAttribute('src');
                    $cateName = $imgSrc;
                    /**
                    $cateNodes = $childDom->find('strong');

                    if($cateNodes != null && count($cateNodes) > 0){
                        $cateName = $cateNodes[0]->text;
                    }
                     **/
                    $category = $this->createCategory($cateName, $imgSrc);

                    $aNodes = $childDom->load($group->innerHtml)->find('a');

                    $dialogues = array();

                    foreach($aNodes as $aNode){
                            $name =  $aNode->text;
                            $contentURL = $aNode->getAttribute('href');

                        $dialogue = $this->createDialogue($category, $name, $contentURL, $type);

//                        $this->getDialogues($dialogue);

                        if($dialogue){
                            $dialogues[] = array(
                                'Id' => $dialogue->getId(),
                                'Name' => $dialogue->getName(),
//                                'MediaURL' => $dialogue->getMediaUrl(),
                            ) ;
                        }
                    }

                    $result[] = array(
                        'CategoryName' => $category ? $category->getName() : "#NULL" ,
                        'CategoryID' => $category->getId(),
                        'Dialogues' => $dialogues,
                    );

                } catch (Exception $e){

                    return $e->getMessage();
                }


            }

        } catch(Exception $e){

            return "Exception: ". $e->getMessage();
        }

        return $result;
    }


    public function getDialogues(Dialogues $dialogue){

        $data = array();
        try {
            $dom = new Dom();
            $dom->loadFromUrl($dialogue->getContentUrl());
            $title = $dom->find('.entry-title')[0];

            $name = $title->text;

            $contentNode = $dom->find('.post-content');

            $remove   = $dom->find('.et_social_inline');
            $remove->delete();
            unset($remove);

            $remove   = $dom->find('.et_social_inline_bottom');
            $remove->delete();
            unset($remove);

            $remove   = $dom->find('.fusion-accordian');
            $remove->delete();
            unset($remove);

            $remove   = $dom->find('.fusion-youtube');
            $remove->delete();
            unset($remove);

            $removeTagA   = $dom->find('a');
            foreach($removeTagA as $a){
                $a->delete();
                unset($a);
            }

            $childDom = new Dom();
            $childDom->load($contentNode->innerHtml);
            $imgNode = $childDom->find('img')[0];

            $imageURL = null;
            if($imgNode !== null){
                $imageURL = $imgNode->getAttribute('src');
            }
            $audioNode = $childDom->find('embed')[0];

            $mediaURL = null;
            if($audioNode !== null){
                $mediaURL = str_replace("audioUrl=","",$audioNode->getAttribute('flashvars'));
            }

            $html2Text = new Html2Text($contentNode->innerHtml);

            $content = $html2Text->getText();

            $this->updateDialogue($dialogue, $name, $imageURL, $mediaURL, $content);

        } catch(Exception $e){

        }


        return $data;
    }


    private function getTextCategories($uri){

        $result = array();

        try {
            $dom = new Dom();
            $dom->loadFromUrl($uri);

            $groupsNode = $dom->find('.fusion-column-wrapper');


            foreach($groupsNode as $group){

                try{
                    $childDom = new Dom();

                    $childDom->load($group->innerHtml);

                    $listP = $childDom->find('p');
                    $dialogues = array();
                    $category = null;

                    foreach($listP as $p){

                        $anImg = $p->find('img');

                        if($anImg) {
                            if($dialogues  && $category){
                                $result[] = array(
                                    'CategoryName' => $category ? $category->getName() : "#NULL" ,
                                    'CategoryID' => $category->getId(),
                                    'Dialogues' => $dialogues,
                                );

                                return $result;

                                unset($dialogues);
                                $dialogues = array();

                            }

                            $anImgSrc = $anImg->getAttribute('src');
                            $imgSrc = $anImgSrc;
                            $cateName = $imgSrc;
                            $category = $this->createCategory($cateName, $imgSrc);


                        } else if($category){
                            $aNode = $p->find('a');
                            if($aNode){
                                $name =  $aNode->text;
                                $contentURL = $aNode->getAttribute('href');
                                $dialogue = $this->createDialogue($category, $name, $contentURL, SettingsManager::MEDIA_TYPE_TEXT);

                                if($dialogue){
                                    $dialogues[] = array(
                                        'Id' => $dialogue->getId(),
                                        'Name' => $dialogue->getName(),
                                    ) ;
                                }
                            }

                        }

                    }

                } catch (Exception $e){

//                    return $result;

                    return $e->getMessage();
                }


            }

        } catch(Exception $e){

            return "Exception: ". $e->getMessage();
        }

        return $result;
    }

    private function createCategory($name, $iconURL){

        /** @var Users $user */
        $user = $this->tokenStore->getToken()->getUser();

        $name = ucfirst(html_entity_decode($name));
        $project = null;

        if($user){
            $project = $user->getProject();
        }

        $em = $this->doctrine->getManager();
        try{
            $em->getConnection()->beginTransaction();

            $category = $this->doctrine->getRepository('AppBundle:Categories')->findOneBy(array(
                'name' => $name,
                'project' => $project,
                'approved' => SettingsManager::APPROVAL_STATUS_NOT_COMPLETED,
            ));

            if($category == null){
                $category = new Categories();

            }
            $category->setName($name);
            $category->setIconUrl($iconURL);
            $category->setProject($project);
            $category->setCreatedOn(time());

            $em->persist($category);
            $em->flush();

            $em->getConnection()->commit();

            return $category;

        } catch(Exception $e){

            $em->getConnection()->rollback();
            return $e->getMessage();

        }

        return null;

    }

    private function createDialogue(Categories $category, $name, $contentURL, $type = SettingsManager::MEDIA_TYPE_AUDIO){

        /** @var Users $user */
        $user = $this->tokenStore->getToken()->getUser();

        $name = ucfirst(html_entity_decode($name));

        $project = null;
        if($user){
            $project = $user->getProject();
        }

        $em = $this->doctrine->getManager();
        try{
            $em->getConnection()->beginTransaction();

            $entity = $this->doctrine->getRepository('AppBundle:Dialogues')->findOneBy(array(
                'name' => $name,
                'category' => $category,
                'approved' => SettingsManager::APPROVAL_STATUS_NOT_COMPLETED,
            ));

            if($entity == null){
                $entity = new Dialogues();
            }

            $entity->setCreatedOn(time());
            $entity->setCategory($category);
            $entity->setName($name);
            $entity->setContentUrl($contentURL);
            $entity->setProject($project);
            $entity->setType($type);

            $em->persist($entity);
            $em->flush();

            $em->getConnection()->commit();

            return $entity;

        } catch(Exception $e){

            $em->getConnection()->rollback();
            return $e->getMessage();
        }

        return null;
    }

    /**
     * @param Dialogues $entity
     * @param $name
     * @param $iconURL
     * @param $mediaURL
     * @param $content
     * @return Dialogues|null|string
     *
     */

    private function updateDialogue(Dialogues $entity, $name ,$iconURL, $mediaURL, $content){

        $name = html_entity_decode($name);
        $content = html_entity_decode($content);
        $em = $this->doctrine->getManager();
        try{
            $em->getConnection()->beginTransaction();

            $entity->setName($name);
            $entity->setIconUrl($iconURL);
            $entity->setMediaUrl($mediaURL);
            $entity->setContent($content);
            $entity->setApproved(SettingsManager::APPROVAL_STATUS_PENDING_APPROVAL);
            $entity->setUpdatedOn(time());

            $em->persist($entity);
            $em->flush();

            $em->getConnection()->commit();

            return $entity;

        } catch(Exception $e){

            $em->getConnection()->rollback();
            return $e->getMessage();
        }

        return null;
    }
}