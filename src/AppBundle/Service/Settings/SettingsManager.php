<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 6/4/16
 * Time: 2:45 PM
 */

namespace AppBundle\Service\Settings;


class SettingsManager
{

    const APPROVAL_STATUS_NOT_COMPLETED = 0;
    const APPROVAL_STATUS_PENDING_APPROVAL = 1;
    const APPROVAL_STATUS_APPROVED = 2;

    const VIDEO_TYPE_MP4 = 'mp4';
    const VIDEO_TYPE_YOUTUTE = 'youtube';
    const VIDEO_TYPE_VIMEO = 'vimeo';

    const MEDIA_TYPE_AUDIO = 'audio';
    const MEDIA_TYPE_YOUTUTE = 'youtube';
    const MEDIA_TYPE_VIDEO = 'video';
    const MEDIA_TYPE_TEXT = 'text';

    public function getAvailablesRoles(){

        return array(
            'ROLE_CAN_ACCESS_PROJECTS',
            'ROLE_CAN_ACCESS_USERS',
            'ROLE_CAN_ACCESS_ANALYTICS',
        );
    }
    public function getVideoTypes(){

        return [
            'Mp4' => SELF::VIDEO_TYPE_MP4,
            'Youtube' => SELF::VIDEO_TYPE_YOUTUTE,
            'Vimeo' => SELF::VIDEO_TYPE_VIMEO,
            '#NULL' => '',
        ];

    }

    public function getDialogueTypes(){

        return [
            'Audio' => SELF::MEDIA_TYPE_AUDIO,
            'Video' => SELF::MEDIA_TYPE_VIDEO,
            'Youtube' => SELF::MEDIA_TYPE_YOUTUTE,
            'Text' => SELF::MEDIA_TYPE_TEXT,
            '#NULL' => '',
        ];

    }

    public function getApprovalStatus(){
        return array(
            SELF::APPROVAL_STATUS_NOT_COMPLETED => 'Not completed',
            SELF::APPROVAL_STATUS_PENDING_APPROVAL => 'Pending approval',
            SELF::APPROVAL_STATUS_APPROVED => 'Approved',
        );
    }

    public function getUsersDefaultColumns(){

        return $columns = array(
            'id' => array(
                'display' => true,
                'name' => 'ID #',
            ),
             'ip_address' => array(
                'display' => true,
                'name' => 'IP Address',
            ),
             'username' => array(
                'display' => true,
                'name' => 'Username',
            ),
             'email' => array(
                'display' => true,
                'name' => 'Email',
            ),
             'last_login' => array(
                'display' => true,
                'name' => 'Last login',
            ),
             'group_name' => array(
                'display' => true,
                'name' => 'Group Name',
            ),
                'project_name' => array(
                'display' => true,
                'name' => 'Project Name',
            ),

        );

    }

    public function getProjectsDefaultColumns(){

        return $columns = array(
            'id' => array(
                'display' => true,
                'name' => 'ID #',
            ),
            'project_icon' => array(
                'display' => true,
                'name' => 'Icon',
            ),
            'name' => array(
                'display' => true,
                'name' => 'Name',
            ),
            'pem_url_aspn' => array(
                'display' => true,
                'name' => 'PEM URL',
            ),
            'pass_pharse_aspn' => array(
                'display' => true,
                'name' => 'Pass pharse ASPN',
            ),

        );

    }

    public function getDevicesDefaultColumns(){

        return $columns = array(
            'id' => array(
                'display' => true,
                'name' => 'ID #',
                'line' => 0,
            ),
            'type' => array(
                'display' => true,
                'name' => 'Type',
                'line' => 0,

            ),
            'os_version' => array(
                'display' => true,
                'name' => 'OS Version',
                'line' => 0,
            ),
            'localize' => array(
                'display' => true,
                'name' => 'Localize',
                'line' => 0,
            ),
            'created_on' => array(
                'display' => true,
                'name' => 'Created On',
                'line' => 0,
            ),
            'project_name' => array(
                'display' => true,
                'name' => 'Project Name',
                'line' => 1,
            ),
            'username' => array(
                'display' => true,
                'name' => 'Username',
                'line' => 1,
            ),
            'device_identifier' => array(
                'display' => true,
                'name' => 'Device Identifier',
                'line' => 1,
            ),
            'first_name' => array(
                'display' => true,
                'name' => 'First Name',
                'line' => 1,
            ),
            'last_name' => array(
                'display' => true,
                'name' => 'Project Name',
                'line' => 1,
            ),
            'count' => array(
                'display' => true,
                'name' => 'Count',
                'line' => 1,
            ),
            'last_active' => array(
                'display' => true,
                'name' => 'Last Active',
                'line' => 1,
            ),
            'release_version' => array(
                'display' => true,
                'name' => 'Release Version',
                'line' => 1,
            ),
            'build_version' => array(
                'display' => true,
                'name' => 'Build Version',
                'line' => 1,
            ),

        );

    }

    public function getLogsDefaultColumns(){

        return $columns = array(
            'user_id' => array(
                'display' => true,
                'name' => 'User ID #',
                'line' => 0,
            ),
            'client_ip' => array(
                'display' => true,
                'name' => 'Client IP',
                'line' => 0,
            ),
            'project_name' => array(
                'display' => true,
                'name' => 'Project',
                'line' => 0,
            ),
            'country_name' => array(
                'display' => true,
                'name' => 'Country',
                'line' => 0,
            ),
            'count' => array(
                'display' => true,
                'name' => 'Count',
                'line' => 0,
            ),
            'created_on' => array(
                'display' => true,
                'name' => 'Created On',
                'line' => 0,
            ),
            'request_id' => array(
                'display' => true,
                'name' => 'ID #',
                'line' => 1,
            ),
            'username' => array(
                'display' => true,
                'name' => 'username',
                'line' => 1,
            ),
            'time_zone' => array(
                'display' => true,
                'name' => 'Time Zone',
                'line' => 1,
            ),
            'city' => array(
                'display' => true,
                'name' => 'City',
                'line' => 1,
            ),

        );

    }


    /**
     * @return array
     *
     */
    public function getCategoriesDefaultColumns(){

        return $columns = array(
            'id' => array(
                'display' => true,
                'name' => 'ID #',
            ),
            'name' => array(
                'display' => true,
                'name' => 'Name',
            ),
            'description' => array(
                'display' => true,
                'name' => 'Description',
            ),
            'icon_url' => array(
                'display' => true,
                'name' => 'Icon URL',
            ),
            'project_name' => array(
                'display' => true,
                'name' => 'Project Name',
            ),
            'total_dialogues' => array(
                'display' => true,
                'name' => 'Total Dialogues',
            ),
            'approval_status' => array(
                'display' => true,
                'name' => 'Approval Status',
                'text' => $this->getApprovalStatus(),
            ),

        );

    }

    public function getDialoguesDefaultColumns(){

        return $columns = array(
            'id' => array(
                'display' => true,
                'name' => 'ID #',
            ),
            'name' => array(
                'display' => true,
                'name' => 'Name',
            ),
            'content' => array(
                'display' => true,
                'name' => 'Content',
//                'raw' => true,
            ),
            'icon_url' => array(
                'display' => false,
                'name' => 'Icon URL',
            ),
            'media_url' => array(
                'display' => true,
                'name' => 'Media URL',
            ),
            'approval_status' => array(
                'display' => false,
                'name' => 'Approval status',
                'text' => $this->getApprovalStatus(),
            ),
            'category_name' => array(
                'display' => false,
                'name' => 'Category',
            ),
            'project_name' => array(
                'display' => false,
                'name' => 'Project',
            ),
            'number_report' => array(
                'display' => true,
                'name' => 'Number of report',
            ),
            'type' => array(
                'display' => false,
                'name' => 'Media Type',
                'text' => array_flip($this->getDialogueTypes()),
            ),

        );

    }

    /**
     * @return array
     *
     */
    public function getFBCategoriesDefaultColumns(){

        return $columns = array(
            'id' => array(
                'display' => true,
                'name' => 'ID #',
            ),
            'name' => array(
                'display' => true,
                'name' => 'Name',
            ),
            'description' => array(
                'display' => true,
                'name' => 'Description',
            ),

        );

    }
    /**
     * @return array
     *
     */
    public function getFBVideosDefaultColumns(){

        return $columns = array(
            'id' => array(
                'display' => true,
                'name' => 'ID #',
            ),
            'name' => array(
                'display' => true,
                'name' => 'Name',
            ),
            'short_description' => array(
                'display' => true,
                'name' => 'Short Description',
            ),
            'video_url' => array(
                'display' => true,
                'name' => 'Video URL',
            ),
            'media_url' => array(
                'display' => true,
                'name' => 'Media URL',
            ),
             'type' => array(
                'display' => true,
                'name' => 'Type',
                 'text' => array_flip($this->getVideoTypes())
            ),
            'categories' => array(
                'display' => true,
                'name' => 'Categories',
            ),
            'approved' => array(
                'display' => true,
                'name' => 'Approved Status',
                'text' => $this->getApprovalStatus(),
            ),

        );

    }
    /**
     * @return array
     *
     */
    public function getFFCategoriesDefaultColumns(){

        return $columns = array(
            'id' => array(
                'display' => true,
                'name' => 'ID #',
            ),
            'name' => array(
                'display' => true,
                'name' => 'Name',
            ),

        );

    }

    /**
     * @return array
     *
     */
    public function getFFDialoguesDefaultColumns(){

        return $columns = array(
            'id' => array(
                'display' => true,
                'name' => 'ID #',
            ),
            'name' => array(
                'display' => true,
                'name' => 'Name',
            ),
            'content' => array(
                'display' => true,
                'name' => 'Content',
//                'raw' => true,
            ),
            'category_name' => array(
                'display' => true,
                'name' => 'Category',
            ),


        );

    }


    public function getQAsDefaultColumns(){

        return $columns = array(
            'id' => array(
                'display' => true,
                'name' => 'ID #',
            ),
            'question' => array(
                'display' => true,
                'name' => 'Question',
            ),
            'answer' => array(
                'display' => true,
                'name' => 'Answer',
            ),
            'project_name' => array(
                'display' => true,
                'name' => 'Project Name',
            ),

        );

    }
}