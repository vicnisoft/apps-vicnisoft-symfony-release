<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 8/21/16
 * Time: 8:58 AM
 */

namespace AppBundle\Service\ToolsParserHTML;

use AppBundle\Entity\Dialogues;
use AppBundle\Models\ParserHTML\EslFast;
use AppBundle\Service\Html2Text;
use Doctrine\Bundle\DoctrineBundle\Registry;
use PHPHtmlParser\Dom;

class EnglishEslFast
{


    private $doctrine;

    /**
     * EnglishEslFast constructor.
     */
    public function __construct(Registry $doctrine)
    {
        $this->doctrine = $doctrine;
    }


    public function parser(EslFast &$model){
        $result = array();
        try {
            $dom = new Dom();
            $dom->load($model->getUrl());
            $html = $dom->outerHtml;
            $fontsNodes = $dom->find('font');
            $name = null;

            foreach($fontsNodes as $fontNode){

                $fontSize = $fontNode->getAttribute('size');
                if($fontSize == "7") {
                    $name = $fontNode->text;
                    continue;

                } elseif($fontSize != 5) {
                    continue;
                }

                $childDom = new Dom();

                $htmlContent = $fontNode->innerHtml;
                $childDom->load($htmlContent);

                $audioList = array();
                $audioNodes = $childDom->find('audio');
                foreach($audioNodes as $node){
                    $audioURL = $node->getAttribute('src');
                    $audioList[] = $model->getAudioHost() . str_replace('../', '', $audioURL);
                }

                $contentHtml = $childDom->outerHtml;
                $html2Text = new Html2Text($contentHtml);
                $content = $html2Text->getText();
                $model->setName($name);
                $newDialogues = array();
                foreach($audioList as $index => $url) {
                   $newDialogues[] = $this->saveNewDialogue($model, $url, $content, $index);
                }

                $result = array(
                    'AudioURL' => $audioList ,
                    'Content' => $contentHtml,
                    'Name' => $name,
                    'URL' => $model->getUrl(),
                    'NewDialogues' => $newDialogues,
                );

            }

        } catch(\Exception $e){
            $result['ExceptionMessage'] = $e->getMessage();
        }

        return $result;
    }


    function saveNewDialogue(EslFast $model, $audioURL = '', $content = '' , $index = 0){
        try{
            if($categories = $model->getCategory()) {

                $index = $index + 1;

                $name = $model->getName() .' ' .$index;
                $d = $this->doctrine->getRepository('AppBundle:Dialogues')->findOneBy(array(
                    'name' => $name,
                ));
                if(is_null($d)){

                    $em = $this->doctrine->getManager();
                    $dialogue = new Dialogues();
                    $dialogue->setName($model->getName() .' ' .$index);
                    $dialogue->setContent($content);
                    $dialogue->setMediaUrl($audioURL);
                    $dialogue->setProject($categories->getProject());
                    $dialogue->setCategory($categories);

                    $dialogue->setCreatedOn(time());
                    $em->persist($dialogue);
                    $em->flush();

                    return $dialogue->getId();
                }

            }
        } catch(\Exception $e){

        }

        return -1;


    }

}