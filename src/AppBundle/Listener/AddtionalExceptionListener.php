<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 6/15/16
 * Time: 4:35 PM
 */

namespace AppBundle\Listener;

use Symfony\Component\Debug\Exception\FatalErrorException;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use AppBundle\Service\ExceptionLogger;

class AddtionalExceptionListener
{
    /** @var ExceptionLogger  */

    private $logger;


    /**
     * @param ExceptionLogger $logger
     *
     */

    public function __construct(ExceptionLogger $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param GetResponseForExceptionEvent $event
     *
     */

    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();
        $this->logger->error($exception->getMessage());
//        $this->logger->debug($e);


    }

    /**
     * @param \Exception $e
     * @return array
     *
     */

    /*

    private function getAdditionalExceptionInformation(\Exception $e){

        $filter = array(
            'SCRIPT_URI',
            'HTTPS',
            'HTTP_HOST',
            'HTTP_USER_AGENT',
            'SERVER_SIGNATURE',
            'SERVER_SOFTWARE',
            'SERVER_NAME',
            'SERVER_ADDR',
            'SERVER_PORT',
            'REMOTE_ADDR',
            'REMOTE_PORT',
            'REQUEST_TIME_FLOAT',
            'REQUEST_TIME',
        );

        $info = array();
        foreach($_SERVER as $key => $value){
            if(in_array($key, $filter)){
                $info[$key] = $value;
            }
        }

        $addtional = array();

        $addtional['MEMORY_USESAGE'] = $this->getMemoryUsage();


        $addtional['FREE_SPACE'] = $this->getFreeSpace();


        return array_merge(array(
            'CODE' => $e->getCode(),
            'MESSAGE' => $e->getMessage(),
        ), $info, $addtional);

    }


    private function getMemoryUsage() {

        $mem_usage = memory_get_usage(true);
        return $this->dataSize($mem_usage);
    }

    private function getFreeSpace(){
        $Bytes = disk_total_space("/");
        return $this->dataSize($Bytes);
    }

    private  function dataSize($Bytes)
    {
        $Type=array("", "kilo", "mega", "giga", "tera");
        $counter=0;
        while($Bytes>=1024)
        {
            $Bytes/=1024;
            $counter++;
        }
        return("".$Bytes." ".$Type[$counter]."bytes");
    }
    */


}