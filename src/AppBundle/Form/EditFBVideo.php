<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 6/9/16
 * Time: 5:33 PM
 */

namespace AppBundle\Form;

use AppBundle\Form\Type\VideoType;
use AppBundle\Service\Settings\SettingsManager;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;

class EditFBVideo extends AbstractType
{


    public function buildForm(FormBuilderInterface $builder, array $options){


        $builder->add('name', TextType::class, array(
            'label' => "Name *",
            'constraints' => new NotBlank(),

        ))
            ->add('video_url', UrlType::class, array(
            'label' => "Video URL *",
            'constraints' => new NotBlank(),

        ))
            ->add('type', VideoType::class, array(
                'label' => "Type *",
        ))
            ->add('videoCategories', EntityType::class, array(
                'label' => "Categories *",
                'class' => 'AppBundle\Entity\FbCategories',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('fb_c')
                        ->orderBy('fb_c.name', 'ASC');
                },
//                'choices' => $options['categories'],
                'choice_label' => 'name',
                'multiple' => true,
                'expanded' => false,
        ))

            ->add('short_description', TextareaType::class, array(
            'label' => "Short Description",
        ))
            ->add('description', TextareaType::class, array(
            'label' => "Description",
        ));


        $builder->add('cancel', SubmitType::class, array('label' => "Cancel", 'attr' => array('formnovalidate' => 'formnovalidate')))
            ->add('submit', SubmitType::class, array('label' => "Save",'attr' => array(
                'class' => 'btn-primary'
            )));

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\FbVideos',
            'categories' => '',
        ));
    }
}