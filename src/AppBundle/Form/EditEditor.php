<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 7/15/16
 * Time: 4:28 PM
 */

namespace AppBundle\Form;


use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class EditEditor extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('editor', CKEditorType::class, array(
            'config' => array('toolbar' => 'full'),
        ));

        $builder->add('cancel', SubmitType::class, array('label' => "Cancel", 'attr' => array('formnovalidate' => 'formnovalidate')))
            ->add('submit', SubmitType::class, array('label' => "Save",'attr' => array(
                'class' => 'btn-primary'
            )));
    }


}