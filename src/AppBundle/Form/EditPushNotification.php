<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 7/1/16
 * Time: 5:28 PM
 */

namespace AppBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AppBundle\Entity\Projects;

class EditPushNotification extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options){


        $builder->add('deviceId', TextType::class, array(
            'label' => "Device ID",
            'disabled' => true,

        ))->add('project', EntityType::class, array(
                'class' => 'AppBundle\Entity\Projects',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('p')
                        ->orderBy('p.name', 'ASC');
                },
                'choice_label' => 'name',
                'label' => 'Project *',
                'disabled' => true,

            ))->add('message', TextareaType::class, array(
            'label' => "Message *",
            'constraints' => new NotBlank(),

        ));


        $builder->add('cancel', SubmitType::class, array('label' => "Cancel", 'attr' => array('formnovalidate' => 'formnovalidate')))
            ->add('submit', SubmitType::class, array('label' => "Save",'attr' => array(
                'class' => 'btn-primary'
            )));


    }

}