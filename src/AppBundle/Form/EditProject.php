<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 6/4/16
 * Time: 8:20 PM
 */

namespace AppBundle\Form;


use Doctrine\ORM\EntityRepository;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class EditProject extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder , array $options){

        $builder->add('name', TextType::class, array(
            'label' => "Name"
        ))
            ->add('description', TextareaType::class,array(
                'label' => 'Description',
                'constraints' => new NotBlank(),
            ))
            ->add('projectIcon', FileType::class,array(
                'label' => 'Icon',
            ))
            ->add('pemUrlAspn', FileType::class,array(
                'label' => 'Pem File ASPN',
            ))
             ->add('passPharseAspn', TextType::class,array(
                'label' => 'Password Pharse ASPN',
            ))

            ->add('createdBy', EntityType::class, array(
                'class' => 'AppBundle\Entity\Users',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.username', 'ASC');
                },
                'choice_label' => 'username',

            ));

        $builder->add('cancel', SubmitType::class, array('label' => "Cancel", 'attr' => array('formnovalidate' => 'formnovalidate')))
            ->add('submit', SubmitType::class, array('label' => "Save",'attr' => array(
                'class' => 'btn-primary'
            )));
    }

}