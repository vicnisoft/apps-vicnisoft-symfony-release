<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 8/8/16
 * Time: 1:55 PM
 */

namespace AppBundle\Form;


use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class EditQA extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('questions', TextType::class, array(
            'label' => "Question *",
            'constraints' => new NotBlank(),
        ))
            ->add('answer', CKEditorType::class, array(
                'label' => "Answer *",
                'constraints' => array(new NotBlank()),

            ))
            ->add('project', EntityType::class, array(
                'class' => 'AppBundle\Entity\Projects',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('p')
                        ->orderBy('p.name', 'ASC');
                },
                'choice_label' => 'name',
                'label' => "Project",
                'data' => $options['project'],
                'disabled' => $options['project'] ? true : false,
            ));


        $builder->add('cancel', SubmitType::class, array('label' => "Cancel", 'attr' => array('formnovalidate' => 'formnovalidate')))
            ->add('submit', SubmitType::class, array('label' => "Save",'attr' => array(
                'class' => 'btn-primary'
            )));    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\QuestionsAnswers',
            'project' => true,
        ));
    }


}