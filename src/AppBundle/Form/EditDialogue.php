<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 6/5/16
 * Time: 10:59 PM
 */

namespace AppBundle\Form;


use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class EditDialogue extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder , array $options){

        $builder->add('name', TextType::class, array(
            'label' => "Name",
            'trim' => true,
        ))
            ->add('content', TextareaType::class,array(
                'label' => 'Content',
                'trim' => true,
                'attr' => array(
                    'rows' => 10,
                    'cols' => 50,
                )
            ))
            ->add('iconUrl', TextType::class, array(
                'label' => "Icon URL",
                'trim' => true,
            ))
            ->add('mediaUrl', TextType::class, array(
                'label' => "Media URL",
                'trim' => true,
            ))->add('approved', ChoiceType::class, array(
                'label' => "Approval status",
                'choices' => $options['approval_status'],
                'expanded' => true,
                'label_attr' => array('class' => 'radio_btn'),
                'attr' => array('class' => 'radio_btn')

            ));

        if(is_array($options['type'])){
            $builder->add('type', ChoiceType::class, array(
                'label' => "Type",
                'choices' => $options['type'],
                'expanded' => true,
                'label_attr' => array('class' => 'radio_btn'),
                'attr' => array('class' => 'radio_btn')
            ));
        }

        if($options['project'] == false){
            $builder->add('category', EntityType::class, array(
                'class' => 'AppBundle\Entity\Categories',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('c')->andWhere('c.active = 1')
                        ->orderBy('c.name', 'ASC');
                },
                'choice_label' => 'name',
                'label' => 'Category *'

            ));
        } else {
            $project =  $options['project'] ;

            $builder->add('category', EntityType::class, array(
                'class' => 'AppBundle\Entity\Categories',
                'query_builder' => function (EntityRepository $er) use ($project) {
                    return $er->createQueryBuilder('c')
                        ->leftJoin('c.project','p')
                        ->andWhere('c.active = 1')
                        ->andWhere("p.id = $project")
                        ->orderBy('c.name', 'ASC');
                },
                'choice_label' => 'name',
                'label' => 'Category *'

            ));
        }


        if($options['project'] == false){

            $builder->add('project', EntityType::class, array(
                'class' => 'AppBundle\Entity\Projects',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('p')->andWhere('p.active = 1')
                        ->orderBy('p.name', 'ASC');
                },
                'choice_label' => 'name',
                'label' => "Project",
            ));
        }

        $builder->add('cancel', SubmitType::class, array('label' => "Cancel", 'attr' => array('formnovalidate' => 'formnovalidate')))
            ->add('submit', SubmitType::class, array('label' => "Save",'attr' => array(
                'class' => 'btn-primary'
            )));
    }



    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Dialogues',
            'project' => false,
            'type' => array(
                '0' => 'Audio',
                '1' => 'Video',
            ),
            'approval_status' => array(),
        ));
    }

}