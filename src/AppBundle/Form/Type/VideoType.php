<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 6/9/16
 * Time: 6:16 PM
 */

namespace AppBundle\Form\Type;


use AppBundle\Service\Settings\SettingsManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class VideoType extends AbstractType
{
    private $settings;

    public function __construct(SettingsManager $settingsManager)
    {
        $this->settings = $settingsManager;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'choices' => $this->settings->getVideoTypes(),
        ));
    }

    public function getParent()
    {
        return ChoiceType::class;
    }
}