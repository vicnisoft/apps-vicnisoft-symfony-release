*======== Doctrines==========*

php bin/console doctrine:mapping:import --force AppBundle yml

This command line tool asks Doctrine to introspect the database and generate the YML metadata files
under the src/AppBundle/Resources/config/doctrine folder of your bundle.
This generates two files: EntityName1.orm.xml and EntityName2.orm.xml.

- Create local to place the entities (Create directory Entity under AppBundle)

# generates all entities in the AppBundle

php bin/console doctrine:generate:entities AppBundle

# Generate single Entity with name SocialUsers

Step1 :

php bin/console doctrine:mapping:import --force AppBundle yml --filter="UsersColumns"

Step 2:

php bin/console doctrine:mapping:convert annotation ./src/AppBundle/Entity --from-database --filter="UsersColumns"

Step 3:

Add namespace to class name

Step 4:

php bin/console doctrine:generate:entities AppBundle:QuestionsAnswers --no-backup
======= Clear cache =======

php bin/console cache:clear --env=prod

======== GIT ============
git merge -s ours
git merge -s theirs


==== SSH Access ====
185.28.21.161
65002
u582337641_apps
PASSWORD
ssh -l u582337641 -p 65002 185.28.21.161


kutheewa5sho2Ohr


==== Database query ========

* Empty database

SET FOREIGN_KEY_CHECKS = 0;

TRUNCATE devices;
TRUNCATE fb_categories;
TRUNCATE fb_categories_videos;
TRUNCATE fb_videos;
TRUNCATE ff_categories;
TRUNCATE ff_dialogues;
TRUNCATE groups;
TRUNCATE login_attempts;
TRUNCATE nlp_languages;
TRUNCATE nlp_words;
TRUNCATE projects;
TRUNCATE requests;
TRUNCATE users;
TRUNCATE users_devices;
TRUNCATE users_groups;
TRUNCATE login_attempts;
TRUNCATE nlp_languages;
TRUNCATE nlp_words;

SET FOREIGN_KEY_CHECKS = 1;

SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE devices;
DROP TABLE fb_categories;
DROP TABLE fb_categories_videos;
DROP TABLE fb_videos;
DROP TABLE ff_categories;
DROP TABLE ff_dialogues;
DROP TABLE groups;
DROP TABLE login_attempts;
DROP TABLE nlp_languages;
DROP TABLE nlp_words;


SET FOREIGN_KEY_CHECKS = 1;
//==============

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';


TRUNCATE categories;
TRUNCATE dialogues;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;


/ ---- Install assets

_php bin/console assets:install web_