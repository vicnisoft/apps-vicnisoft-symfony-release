<?php

/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 5/31/16
 * Time: 2:55 PM
 */

$servers = array(
    array(
        'uri' => '',
        'api_key' => ''
    )
);


$monitors = [
    'api/monitor/users',
    'api/monitor/inspections',
    'api/monitor/pos',
    'api/monitor/polines',
    'api/monitor/datasize',
];



if((isset($_FILES["file"]["type"]) && $_FILES["file"]["type"] != NULL)
    || (isset($_POST['json']) && $_POST['json'] != NULL)
    || (isset($argv[1]))){
    require_once('json2csv.class.php');
    $JSON2CSV = new JSON2CSVutil;
    if(isset($argv[1])){
        $shortopts = "f::";  // Required value
        $longopts  = array("file::","dest::");
        $arguments = getopt($shortopts, $longopts);
        if(isset($arguments["dest"])){
            $filepath = $arguments["dest"];
        }
        else{
            $filepath = "JSON2.CSV";
        }
        $JSON2CSV->flattenjsonFile2csv($arguments["file"], $filepath);
    }
    elseif($_FILES["file"]["type"] != NULL){
        $JSON2CSV->JSONfromFile($_FILES["file"]["tmp_name"]);
        $JSON2CSV->flattenDL("JSON2.CSV");
    }
    elseif($_POST['json'] != NULL){
        $JSON2CSV->readJSON($_POST['json']);
        $JSON2CSV->flattenDL("JSON2.CSV");
    }
}
else{
    ?>

    <html>
    <body>

    <form action="<?php echo $_SERVER['SCRIPT_NAME']; ?>" method="post" enctype="multipart/form-data">
        <label for="json">JSON data:</label>
<textarea name="json" cols="150" rows="40">

</textarea>
        <br />
        <label for="file">Filename:</label>
        <input type="file" name="file" id="file" />
        <br />
        <input type="submit" name="submit" value="Convert!" />
    </form>

    </body>
    </html>
    <?php



class JSON2CSVutil{

    public $dataArray;
    public $isNested = FALSE;
    function readJSON($JSONdata){
        $this->dataArray = json_decode($JSONdata,1);
        $this->prependColumnNames();
        return $this->dataArray;
    }
    function JSONfromFile($file){
        $this->dataArray = json_decode(file_get_contents($file),1);
        $this->prependColumnNames();
        return $this->dataArray;
    }
    private function prependColumnNames(){
        foreach(array_keys($this->dataArray[0]) as $key){
            $keys[0][$key] = $key;
        }
        $this->dataArray = array_merge($keys, $this->dataArray);
    }
    function save2CSV($file){
        if($this->isItNested() || !is_array($this->dataArray)){
            echo "JSON is either invalid or has nested elements.";
        }
        else{
            $fileIO = fopen($file, 'w+');
            foreach ($this->dataArray as $fields) {
                fputcsv($fileIO, $fields);
            }
            fclose($fileIO);
        }
    }
    function flatten2CSV($file){
        $fileIO = fopen($file, 'w+');
        foreach ($this->dataArray as $items) {
            $flatData = array();
            $fields = new RecursiveIteratorIterator(new RecursiveArrayIterator($items));
            foreach($fields as $value) {
                array_push($flatData, $value);
            }
            fputcsv($fileIO, $flatData, ";", '"');
        }
        fclose($fileIO);
    }
    function browserDL($CSVname){
        if($this->isItNested() || !is_array($this->dataArray)){
            echo "<h1>JSON is either invalid or has nested elements.</h1>";
        }
        else{
            header("Content-Type: text/csv; charset=utf-8");
            header("Content-Disposition: attachment; filename=$CSVname");
            $output = fopen('php://output', 'w');
            foreach ($this->dataArray as $fields) {
                fputcsv($output, $fields);
            }
        }
    }
    function flattenDL($CSVname){
        header("Content-Type: text/csv; charset=utf-8");
        header("Content-Disposition: attachment; filename=$CSVname");
        $output = fopen("php://output", "w");
        foreach ($this->dataArray as $items) {
            $flatData = array();
            $fields = new RecursiveIteratorIterator(new RecursiveArrayIterator($items));
            foreach($fields as $value) {
                array_push($flatData, $value);
            }
            fputcsv($output, $flatData, ";", '"');
        }
    }
    private function isItNested(){
        foreach($this->dataArray as $data){
            if(is_array($data)){
                $isNested = TRUE;
                break 1;
            }
        }
        return $this->isNested;
    }
    function savejson2csv($JSONdata, $file){
        $this->readJSON($JSONdata);
        $this->save2CSV($file);
    }
    function flattenjson2csv($JSONdata, $file){
        $this->readJSON($JSONdata);
        $this->flatten2CSV($file);
    }
    function savejsonFile2csv($file, $destFile){
        $this->JSONfromFile($file);
        $this->save2CSV($destFile);
    }
    function flattenjsonFile2csv($file, $destFile){
        $this->JSONfromFile($file);
        $this->flatten2CSV($destFile);
    }
}

?>
