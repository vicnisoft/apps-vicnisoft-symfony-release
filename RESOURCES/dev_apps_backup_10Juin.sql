-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Jun 09, 2016 at 05:50 PM
-- Server version: 5.5.42
-- PHP Version: 7.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dev_apps`
--

-- --------------------------------------------------------

--
-- Table structure for table `devices`
--

CREATE TABLE `devices` (
  `id` int(11) unsigned NOT NULL,
  `device_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `os_version` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `localize` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `devices`
--

INSERT INTO `devices` (`id`, `device_id`, `type`, `os_version`, `localize`, `created_on`) VALUES
(1, 'aaa', NULL, NULL, NULL, 1465457866),
(2, 'bbb', NULL, '9.0', NULL, 1465458714),
(3, 'bbbc', 'iPhone', '9.0', NULL, 1465479611),
(4, 'bbbcsssss', 'iPhone', '9.0', NULL, 1465479625);

-- --------------------------------------------------------

--
-- Table structure for table `fb_categories`
--

CREATE TABLE `fb_categories` (
  `id` int(11) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created_on` int(11) unsigned NOT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fb_categories`
--

INSERT INTO `fb_categories` (`id`, `name`, `description`, `created_on`, `active`) VALUES
(1, 'Brazil', 'All relate to Brazil edited', 1465466890, 1),
(2, 'Arsenal', 'All related to arsenal', 1465467014, 1);

-- --------------------------------------------------------

--
-- Table structure for table `fb_categories_videos`
--

CREATE TABLE `fb_categories_videos` (
  `id` int(11) unsigned NOT NULL,
  `category_id` int(11) unsigned NOT NULL,
  `video_id` int(11) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fb_videos`
--

CREATE TABLE `fb_videos` (
  `id` int(11) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `short_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `video_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `updated_on` int(11) unsigned DEFAULT NULL,
  `created_by_id` int(11) unsigned DEFAULT NULL,
  `approved` tinyint(1) unsigned DEFAULT '0',
  `updated_by_id` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fb_videos`
--

INSERT INTO `fb_videos` (`id`, `name`, `short_description`, `description`, `video_url`, `type`, `created_on`, `updated_on`, `created_by_id`, `approved`, `updated_by_id`, `active`) VALUES
(1, 'First video', 'sacas', NULL, 'http://24h-video.24hstatic.com/upload/2-2016/videoclip/2016-06-08/1465342331-spain_v_georgia_01.mp4', 'mp4', 1465486986, NULL, 67, 0, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ff_categories`
--

CREATE TABLE `ff_categories` (
  `id` int(11) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created_by_id` int(11) unsigned DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `icon_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `recycled` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ff_categories`
--

INSERT INTO `ff_categories` (`id`, `name`, `description`, `created_by_id`, `created_on`, `icon_url`, `recycled`) VALUES
(2, 'Alimentation', NULL, NULL, 1465098138, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue1.gif', 0),
(3, 'Premiers contacts :', NULL, NULL, 1465098138, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/sejour.gif', 0),
(4, 'Accueil client : ', NULL, NULL, 1465098138, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-3.gif', 0),
(5, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-4.gif', NULL, NULL, 1465098139, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-4.gif', 0),
(6, 'AIDER SON PROCHAIN', NULL, NULL, 1465098139, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-5.gif', 0),
(7, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-6.gif', NULL, NULL, 1465098139, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-6.gif', 0),
(8, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-7.gif', NULL, NULL, 1465098139, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-7.gif', 0),
(9, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-8.gif', NULL, NULL, 1465098139, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-8.gif', 0),
(10, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-9.gif', NULL, NULL, 1465098139, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-9.gif', 0),
(11, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-10.gif', NULL, NULL, 1465098139, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-10.gif', 0),
(12, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-11.gif', NULL, NULL, 1465098139, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-11.gif', 0),
(13, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-12.gif', NULL, NULL, 1465098139, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-12.gif', 0),
(14, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-13.gif', NULL, NULL, 1465098139, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-13.gif', 0),
(15, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-14.gif', NULL, NULL, 1465098139, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-14.gif', 0),
(16, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-15.gif', NULL, NULL, 1465098139, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-15.gif', 0),
(17, 'Alimentation', NULL, NULL, 1465378492, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue1.gif', 0),
(18, 'Premiers contacts :', NULL, NULL, 1465378492, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/sejour.gif', 0),
(19, 'Accueil client : ', NULL, NULL, 1465378492, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-3.gif', 0),
(20, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-4.gif', NULL, NULL, 1465378492, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-4.gif', 0),
(21, 'AIDER SON PROCHAIN', NULL, NULL, 1465378492, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-5.gif', 0),
(22, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-6.gif', NULL, NULL, 1465378492, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-6.gif', 0),
(23, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-7.gif', NULL, NULL, 1465378492, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-7.gif', 0),
(24, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-8.gif', NULL, NULL, 1465378492, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-8.gif', 0),
(25, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-9.gif', NULL, NULL, 1465378492, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-9.gif', 0),
(26, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-10.gif', NULL, NULL, 1465378492, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-10.gif', 0),
(27, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-11.gif', NULL, NULL, 1465378492, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-11.gif', 0),
(28, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-12.gif', NULL, NULL, 1465378492, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-12.gif', 0),
(29, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-13.gif', NULL, NULL, 1465378493, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-13.gif', 0),
(30, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-14.gif', NULL, NULL, 1465378493, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-14.gif', 0),
(31, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-15.gif', NULL, NULL, 1465378493, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-15.gif', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ff_dialogues`
--

CREATE TABLE `ff_dialogues` (
  `id` int(11) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `icon_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `audio_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `inspector_id` int(11) unsigned DEFAULT NULL,
  `recycled` tinyint(1) DEFAULT '0',
  `updated_on` int(11) unsigned DEFAULT NULL,
  `updated_by_id` int(11) unsigned DEFAULT NULL,
  `approved` tinyint(1) DEFAULT '0',
  `category_id` int(11) unsigned DEFAULT NULL,
  `content_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by_id` int(11) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=336 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ff_dialogues`
--

INSERT INTO `ff_dialogues` (`id`, `name`, `content`, `icon_url`, `audio_url`, `created_on`, `inspector_id`, `recycled`, `updated_on`, `updated_by_id`, `approved`, `category_id`, `content_url`, `created_by_id`) VALUES
(1, 'à la boulangerie', 'Employé : Monsieur bonjour.\r\n\r\nClient : Bonjour, je voudrais trois croissants au beurre, un pain aux raisins et un pain au chocolat, s’il vous plaît.\r\n\r\nEmployé : Oui, ça sera tout ?\r\n\r\nClient : Qu’est-ce que vous avez comme tartes ?\r\n\r\nEmployé : J’ai des tartes aux pommes ou des tartes aux fraises.\r\n\r\nClient : Je vais prendre une tarte aux fraises.\r\n\r\nEmployé : Oui c’est pour combien de personnes ?\r\n\r\nClient : Pour six personnes.\r\n\r\nEmployé : Voilà monsieur. 25 euros cinquante.\r\n\r\nClient : Voilà.\r\n\r\nEmployé : Merci monsieur, au revoir.\r\n\r\nClient : Merci, bonne journée.', 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2008/03/boulangerie.jpg', '', 1465098138, NULL, 0, 1465145025, NULL, 0, 2, 'http://www.podcastfrancaisfacile.com/dialogue/2008/03/la-boulangerie-1.html', NULL),
(2, 'à la boulangerie 2/2', 'Client : Bonjour, monsieur.\r\n\r\nEmployé : Bonjour, monsieur, vous désirez ?\r\n\r\nClient : Je vais prendre deux baguettes et quatre pains au chocolat.\r\n\r\nEmployé : Voilà, vous désirez autre chose ?\r\n\r\nClient : Oui, vous avez des croissants aux amandes ?\r\n\r\nEmployé : Oui monsieur, vous en voulez combien ?\r\n\r\nClient : Deux, s’il vous plaît.\r\n\r\nEmployé : Alors, deux baguettes, quatre pains au chocolat et deux croissants aux amandes. 8 euros 20, s’il vous plaît.\r\n\r\nClient : Voilà.\r\n\r\nEmployé : Merci, monsieur.\r\n\r\nClient : Merci, monsieur. Au revoir.', 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2015/02/boulangerie.jpg', 'http://www.podcastfrancaisfacile.com/wp-content/uploads/files/boulangerie2.mp3', 1465098138, NULL, 0, 1465144265, NULL, 0, 2, 'http://www.podcastfrancaisfacile.com/dialogue/2015/02/acheter-du-pain-a-la-boulangerie.html', NULL),
(3, 'à la boucherie', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 2, 'http://www.podcastfrancaisfacile.com/dialogue/2008/04/bonjour-bienven-1.html', NULL),
(4, 'au marché', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 2, 'http://www.podcastfrancaisfacile.com/podcast/2013/05/au-marche-faire-ses-courses-dialogue-fle-francais-apprendre-learn.html', NULL),
(5, 'chez le fromager 1/3', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 2, 'http://www.podcastfrancaisfacile.com/podcast/2014/02/chez-le-fromager-13.html', NULL),
(6, 'chez le fromager 2/3', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 2, 'http://www.podcastfrancaisfacile.com/podcast/2014/02/chez-le-fromager-23.html', NULL),
(7, 'chez le fromager 3/3', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 2, 'http://www.podcastfrancaisfacile.com/communication/2014/02/dialogue-chez-le-fromager-33.html', NULL),
(8, 'chez le volailler', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 2, 'http://www.podcastfrancaisfacile.com/dialogue/2015/07/poulet-de-bresse.html', NULL),
(9, 'au café', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 2, 'http://www.podcastfrancaisfacile.com/podcast/2008/03/au-caf-1.html', NULL),
(10, 'à la poste', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 2, 'http://www.podcastfrancaisfacile.com/podcast/2013/05/dialogue-a-la-poste.html', NULL),
(11, 'à la gare', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 2, 'http://www.podcastfrancaisfacile.com/podcast/2013/05/a-la-gare.html', NULL),
(12, 'à la pharmacie', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 2, 'http://www.podcastfrancaisfacile.com/podcast/2008/04/la-pharmacie-1.html', NULL),
(13, 'prendre le taxi', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 2, 'http://www.podcastfrancaisfacile.com/podcast/2014/11/prendre-le-taxi.html', NULL),
(14, 'situations diverses', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 2, 'http://www.podcastfrancaisfacile.com/podcast/2014/09/situations-diverses.html', NULL),
(15, 'à la bijouterie (hésiter)', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 2, 'http://www.podcastfrancaisfacile.com/podcast/2013/09/hesiter-dans-une-bijouterie.html', NULL),
(16, 'acheter une cravate', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 2, 'http://www.podcastfrancaisfacile.com/podcast/2014/10/marchand-de-vetements-achat-dune-cravate.html', NULL),
(17, 'Qui est-ce ? 1/2', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 3, 'http://www.podcastfrancaisfacile.com/podcast/2014/07/stage-linguistique-en-france-qui-est-ce-1.html', NULL),
(18, 'Qui est-ce ? 2/2', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 3, 'http://www.podcastfrancaisfacile.com/debutant/2015/03/qui-est-ce-dialogue-2.html', NULL),
(19, 'à l’aéroport', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 3, 'http://www.podcastfrancaisfacile.com/podcast/2014/03/sejour-en-france-premiere-rencontre-de-la-famille-daccueil.html', NULL),
(20, 'la visite de la chambre 1/3', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 3, 'http://www.podcastfrancaisfacile.com/debutant/2015/08/dialogue-francais-pour-debutant.html', NULL),
(21, 'la visite de la chambre 2/3', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 3, 'http://www.podcastfrancaisfacile.com/dialogue/2015/08/apprendre-le-francais-visite-de-la-chambre-2.html', NULL),
(22, 'famille d’accueil', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 3, 'http://www.podcastfrancaisfacile.com/podcast/2014/03/chez-la-famille-daccueil-premiere-rencontre.html', NULL),
(23, 'à l’école de langue 1/2', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 3, 'http://www.podcastfrancaisfacile.com/podcast/2014/07/rencontre-comment-adresser-la-parole-a-quelque-un.html', NULL),
(24, 'à l’école de langue 2/2', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 3, 'http://www.podcastfrancaisfacile.com/podcast/2014/07/rencontre-2.html', NULL),
(25, 'questions au prof', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 3, 'http://www.podcastfrancaisfacile.com/podcast/2014/07/poser-des-questions-au-professeur.html', NULL),
(26, 'demander un service', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 3, 'http://www.podcastfrancaisfacile.com/podcast/2014/07/demander-un-service-en-classe.html', NULL),
(27, '50 phrases pour communiquer en cours de langue', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 3, 'http://www.podcastfrancaisfacile.com/podcast/2013/03/50phrases.html', NULL),
(28, 'parler de projets', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 3, 'http://www.podcastfrancaisfacile.com/podcast/2014/08/projet-pour-le-lendemain.html', NULL),
(29, 'sortie avec l’école', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 3, 'http://www.podcastfrancaisfacile.com/podcast/2014/08/sortie-avec-lecole.html', NULL),
(30, 'après la sortie', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 3, 'http://www.podcastfrancaisfacile.com/podcast/2014/08/apres-une-sortie.html', NULL),
(31, 'devoirs (les sigles)', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 3, 'http://www.podcastfrancaisfacile.com/dialogue/2015/09/sigles-et-acronymes.html', NULL),
(32, 'mission', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 4, 'http://www.podcastfrancaisfacile.com/dialogue/2015/05/accueil-client-dialogue-1-7.html', NULL),
(33, 'conseiller un collègue', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 4, 'http://www.podcastfrancaisfacile.com/dialogue/2015/05/accueil-client-27.html', NULL),
(34, 'le programme', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 4, 'http://www.podcastfrancaisfacile.com/dialogue/2015/05/accueil-client-37.html', NULL),
(35, ' à l&rsquo;aéroport', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 4, 'http://www.podcastfrancaisfacile.com/dialogue/2015/05/accueil-client-47.html', NULL),
(36, 'dans le taxi', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 4, 'http://www.podcastfrancaisfacile.com/dialogue/2015/05/accueil-client-57-francais-des-affaires.html', NULL),
(37, 'après la réunion', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 4, 'http://www.podcastfrancaisfacile.com/dialogue/2015/06/accueil-client.html', NULL),
(38, 'le compte rendu', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 4, 'http://www.podcastfrancaisfacile.com/dialogue/2015/06/accueil-client-77-francais-des-affaires.html', NULL),
(39, 'au fastfood', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 4, 'http://www.podcastfrancaisfacile.com/podcast/2014/11/entretien-dembauche-au-fastfood.html', NULL),
(40, 'cours particuliers', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 4, 'http://www.podcastfrancaisfacile.com/podcast/2014/12/entretien-dembauche-par-telephone-pour-des-cours-particuliers.html', NULL),
(41, ' job au camping', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 4, 'http://www.podcastfrancaisfacile.com/podcast/2014/12/entretien-dembauche-pour-un-job-dans-un-camping.html', NULL),
(42, 'compte rendu', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 4, 'http://www.podcastfrancaisfacile.com/podcast/2014/11/au-bureau-compte-rendu.html', NULL),
(43, 'vendanges', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 4, 'http://www.podcastfrancaisfacile.com/podcast/2014/01/premier-jour-de-vendanges.html', NULL),
(44, 'entre le directeur et la secrétaire', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 4, 'http://www.podcastfrancaisfacile.com/podcast/2014/08/discussion-entre-directeur-et-secretaire-1.html', NULL),
(45, 'attente confirmation 1/2 ', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 4, 'http://www.podcastfrancaisfacile.com/podcast/2014/12/remplacement-12-attente-de-confirmation.html', NULL),
(46, 'confirmation 2/2', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 4, 'http://www.podcastfrancaisfacile.com/communication/2015/01/nourrice-paris.html', NULL),
(47, 'bricoler', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 4, 'http://www.podcastfrancaisfacile.com/podcast/2014/11/brico-macho.html', NULL),
(48, 'le pot de départ', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 4, 'http://www.podcastfrancaisfacile.com/dialoguer/2015/01/affaires-pot-de-depart.html', NULL),
(49, 'la livraison de boudin', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 4, 'http://www.podcastfrancaisfacile.com/dialogue/2015/01/la-livraison-de-boudin.html', NULL),
(50, 'louer une voiture', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 5, 'http://www.podcastfrancaisfacile.com/podcast/2014/01/a-lagence-de-location.html', NULL),
(51, 'louer un appartement', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 5, 'http://www.podcastfrancaisfacile.com/podcast/2012/01/dialogue-location.html', NULL),
(52, 'visiter une maison', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 5, 'http://www.podcastfrancaisfacile.com/podcast/2014/02/visite-dune-maison.html', NULL),
(53, 'louer des vélos 1/3', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 5, 'http://www.podcastfrancaisfacile.com/podcast/2014/09/louer-des-velos-se-renseigner.html', NULL),
(54, 'louer des vélos 2/3', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 5, 'http://www.podcastfrancaisfacile.com/podcast/2014/09/louer-un-velo-23-au-moment-de-la-location.html', NULL),
(55, 'louer des vélos 3/3', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 5, 'http://www.podcastfrancaisfacile.com/podcast/2014/10/louer-un-velo-33-au-moment-du-retour.html', NULL),
(56, 'accueil', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 6, 'http://www.podcastfrancaisfacile.com/podcast/2014/03/recevoir-un-ami-pour-la-premiere-fois-accueil.html', NULL),
(57, 'apéritif', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 6, 'http://www.podcastfrancaisfacile.com/podcast/2014/03/dialogue-a-laperitif.html', NULL),
(58, 'pendant le repas', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 6, 'http://www.podcastfrancaisfacile.com/podcast/2014/03/inviter-recevoir-des-amis-pendant-le-repas.html', NULL),
(59, 'dire au revoir', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 6, 'http://www.podcastfrancaisfacile.com/podcast/2014/03/invitation-chez-des-amis-au-moment-de-dire-au-revoir.html', NULL),
(60, 'avant le dîner', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 6, 'http://www.podcastfrancaisfacile.com/dialogue/2015/07/diner-entre-amis.html', NULL),
(61, ' la grève &#8211; proposer son aide', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 6, 'http://www.podcastfrancaisfacile.com/dialogue/2015/08/solidarite-dialogue-fle.html', NULL),
(62, ' ', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 6, 'http://www.podcastfrancaisfacile.com/dialogue/2015/08/solidarite-dialogue-fle.html', NULL),
(63, 'la grève (informer)', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 6, 'http://www.podcastfrancaisfacile.com/dialogue/2015/08/la-greve-systeme-d.html', NULL),
(64, ' ', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 6, 'http://www.podcastfrancaisfacile.com/dialogue/2015/08/solidarite-dialogue-fle.html', NULL),
(65, 'Système D 2/3 (marchandage)', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 6, 'http://www.podcastfrancaisfacile.com/dialogue/2015/08/dialogue-en-francais-marchandage.html', NULL),
(66, 'Système D 3/3', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 6, 'http://www.podcastfrancaisfacile.com/dialogue/2015/08/systeme-d-33-accident.html', NULL),
(67, 'à la poste', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 7, 'http://www.podcastfrancaisfacile.com/podcast/2013/11/protester.html', NULL),
(68, 'crotte de chien', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 7, 'http://www.podcastfrancaisfacile.com/podcast/2014/01/crotte-de-chien.html', NULL),
(69, 'dans un restaurant', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 7, 'http://www.podcastfrancaisfacile.com/dialogue/2015/02/se-plaindre-du-service-dans-un-restaurant-au-moment-de-regler-laddition.html', NULL),
(70, 'à propos d&rsquo;un ami', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 7, 'http://www.podcastfrancaisfacile.com/podcast/2006/08/retard.html', NULL),
(71, 'au service après-vente (SAV)', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 7, 'http://www.podcastfrancaisfacile.com/dialogue/2015/06/service-apres-vente-sav.html', NULL),
(72, 'un voisin bruyant', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 7, 'http://www.podcastfrancaisfacile.com/dialogue/2015/06/voisin-trop-bruyant.html', NULL),
(73, 'vandalisme', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 7, 'http://www.podcastfrancaisfacile.com/dialogue/2015/04/vandalisme.html', NULL),
(74, 'la grève', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 7, 'http://www.podcastfrancaisfacile.com/dialogue/2015/08/manifester-son-mecontentement-dialogue-fle.html', NULL),
(75, 'centre sportif', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 8, 'http://www.podcastfrancaisfacile.com/podcast/2014/02/se-renseigner-aupres-dun-centre-sportif.html', NULL),
(76, 'à l&rsquo;office de tourisme', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 8, 'http://www.podcastfrancaisfacile.com/podcast/2014/11/office-de-tourisme-de-rochefort.html', NULL),
(77, 'demander son chemin dans la rue', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 8, 'http://www.podcastfrancaisfacile.com/podcast/2013/05/demander-son-chemin-dans-la-rue.html', NULL),
(78, 'demander sa route', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 8, 'http://www.podcastfrancaisfacile.com/podcast/2013/05/sur-la-route.html', NULL),
(79, 'au commissariat : vol', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 8, 'http://www.podcastfrancaisfacile.com/podcast/2014/11/au-commissariat.html', NULL),
(80, 'renseignement cours d&rsquo;anglais', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 8, 'http://www.podcastfrancaisfacile.com/podcast/2014/11/renseignement-apprendre-langlais-cours-danglais.html', NULL),
(81, 'le test d&rsquo;évaluation &#8211; cours d&rsquo;anglais', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 8, 'http://www.podcastfrancaisfacile.com/podcast/2014/11/le-test-devaluation-anglais.html', NULL),
(82, 'différence entre acheter &amp; s’acheter', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 8, 'http://www.podcastfrancaisfacile.com/podcast/2013/08/difference-entre-acheter-et-sacheter.html', NULL),
(83, 'à propos d’un travail', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 8, 'http://www.podcastfrancaisfacile.com/podcast/2013/06/recherche-travail-france.html', NULL),
(84, 'demander une aide &#8211; expliquer le fonctionnement d’une machine', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 8, 'http://www.podcastfrancaisfacile.com/podcast/2013/07/demander-une-aider-expliquer-francais-french-dialogue.html', NULL),
(85, 'demander une aide &#8211; imprécisions', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 8, 'http://www.podcastfrancaisfacile.com/podcast/2014/12/imprecisions-demander-de-laide.html', NULL),
(86, 'la séance photo (féliciter)', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 8, 'http://www.podcastfrancaisfacile.com/podcast/2014/07/seance-photo-imperatif-complimenter.html', NULL),
(87, 'horoscope (expression du temps)', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 8, 'http://www.podcastfrancaisfacile.com/podcast/2013/12/-lhoroscope-lexpression-du-temps.html', NULL),
(88, 'parler de son appartement', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 8, 'http://www.podcastfrancaisfacile.com/podcast/2013/12/parler-du-lieu-ou-on-vit-expression-de-lieu-en-francais-fle.html', NULL),
(89, 'parler de son appartement', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 8, 'http://www.podcastfrancaisfacile.com/podcast/2013/12/parler-du-lieu-ou-on-vit-expression-de-lieu-en-francais-fle.html', NULL),
(90, 'astuce pour démouler un cake', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 8, 'http://www.podcastfrancaisfacile.com/dialogue/2015/06/comment-demouler-un-cake-premier-ou-second-degre.html', NULL),
(91, 'conseiller un site touristique', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 8, 'http://www.podcastfrancaisfacile.com/dialogue/2015/04/mont-saint-michel.html', NULL),
(92, 'la chasse aux champignons', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 8, 'http://www.podcastfrancaisfacile.com/dialogue/2015/10/chasse-aux-champignons.html', NULL),
(93, 'laisser un message informel sur répondeur ', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 9, 'http://www.podcastfrancaisfacile.com/texte/2015/11/laisser-un-message-informel-comprendre-un-message.html', NULL),
(94, 'laisser un message', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 9, 'http://www.podcastfrancaisfacile.com/podcast/2013/11/comment-repondre-au-telephone-telephoner-laisser-un-message-francais.html', NULL),
(95, 'prendre rendez-vous pour une urgence', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 9, 'http://www.podcastfrancaisfacile.com/podcast/2014/10/prendre-un-rendez-vous-pour-une-urgence.html', NULL),
(96, 'prendre rendez-vous par téléphone', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 9, 'http://www.podcastfrancaisfacile.com/dialogue/2015/04/prendre-rendez-vous-par-telephone.html', NULL),
(97, 'le portefeuille oublié', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 9, 'http://www.podcastfrancaisfacile.com/podcast/2014/12/le-portefeuille-oublie.html', NULL),
(98, 'un oubli', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 10, 'http://www.podcastfrancaisfacile.com/podcast/2013/09/dialogue-learn-french-conversation-free.html', NULL),
(99, 'étourdi &#8211; demander de l&rsquo;aide', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 10, 'http://www.podcastfrancaisfacile.com/podcast/2014/07/etourdi-humour-chercher-trouver-un-objet-demander.html', NULL),
(100, 'dépêchons-nous ! (entre une mère et son fils)', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 10, 'http://www.podcastfrancaisfacile.com/podcast/2013/11/depechons-nous-forme-imperative-francais.html', NULL),
(101, 'changement de vêtement', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 10, 'http://www.podcastfrancaisfacile.com/podcast/2014/06/changement-de-vetement.html', NULL),
(102, 'la liste de courses 1', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 10, 'http://www.podcastfrancaisfacile.com/podcast/2013/10/la-liste-de-course.html', NULL),
(103, 'la liste de courses 2', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 10, 'http://www.podcastfrancaisfacile.com/podcast/2014/03/la-liste-de-courses-2-.html', NULL),
(104, 'le retour des courses', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 10, 'http://www.podcastfrancaisfacile.com/podcast/2014/03/retour-des-courses.html', NULL),
(105, 'à propos d&rsquo;un changement de comportement (subjonctif)', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 10, 'http://www.podcastfrancaisfacile.com/podcast/2014/02/changement-de-comportement-expression-du-doute-subjonctif.html', NULL),
(106, 'malade', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 10, 'http://www.podcastfrancaisfacile.com/podcast/2014/08/malade.html', NULL),
(107, 'gueule de bois', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 10, 'http://www.podcastfrancaisfacile.com/podcast/2014/08/gueule-de-bois.html', NULL),
(108, 'dispute autour d&rsquo;un film', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 10, 'http://www.podcastfrancaisfacile.com/podcast/2014/02/dispute-autour-dun-film.html', NULL),
(109, 'dispute cadeau d&rsquo;anniversaire', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 10, 'http://www.podcastfrancaisfacile.com/podcast/2014/10/donner-son-avis-cadeau-quon-naime-pas.html', NULL),
(110, 'les devoirs', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 10, 'http://www.podcastfrancaisfacile.com/podcast/2014/11/les-devoirs-pronom-en-y.html', NULL),
(111, 'le cadeau d&rsquo;anniversaire : ironie', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 10, 'http://www.podcastfrancaisfacile.com/podcast/2014/11/cadeau-danniversaire-ironie.html', NULL),
(112, 'la leçon de piano', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 10, 'http://www.podcastfrancaisfacile.com/grammaire/2015/01/a-propos-du-piano-pronom-en.html', NULL),
(113, 'expliquer comment on s’est blessé', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 11, 'http://www.podcastfrancaisfacile.com/podcast/2013/06/chez-le-docteur.html', NULL),
(114, 'problème à la gorge', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 11, 'http://www.podcastfrancaisfacile.com/podcast/2014/09/chez-le-medecin.html', NULL),
(115, 'blessure grave', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 11, 'http://www.podcastfrancaisfacile.com/podcast/2014/10/blessure-grave-dialogue-fle.html', NULL),
(116, 'problème de tension', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 11, 'http://www.podcastfrancaisfacile.com/podcast/2014/10/probleme-de-tension.html', NULL),
(117, 'tour de reins', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 11, 'http://www.podcastfrancaisfacile.com/podcast/2014/10/dialogue-chez-le-docteur-fle-tour-de-reins.html', NULL),
(118, 'tour de reins', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 11, 'http://www.podcastfrancaisfacile.com/podcast/2014/10/dialogue-chez-le-docteur-fle-tour-de-reins.html', NULL),
(119, 'mal à l&rsquo;estomac', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 11, 'http://www.podcastfrancaisfacile.com/dialogue/2016/05/dialogue-chez-docteur.html', NULL),
(120, 'à la pharmacie (champignons)', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 11, 'http://www.podcastfrancaisfacile.com/dialogue/2015/11/verification-champignon-pharmacie.html', NULL),
(121, 'réserver une table au restaurant', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 12, 'http://www.podcastfrancaisfacile.com/podcast/2013/08/reserver-une-table-au-restaurant.html', NULL),
(122, 'modifier une réservation (au restaurant)', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 12, 'http://www.podcastfrancaisfacile.com/podcast/2013/08/modifier-une-reservation-au-restaurant.html', NULL),
(123, 'prendre rendez-vous chez le coiffeur', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 12, 'http://www.podcastfrancaisfacile.com/podcast/2014/03/prendre-rendez-vous-chez-le-coiffeur.html', NULL),
(124, 'entre amis (inviter / insister / refuser)', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 12, 'http://www.podcastfrancaisfacile.com/podcast/2013/06/inviter-quelquun-insister-refuser.html', NULL),
(125, 'à propos d’une sortie (proposer / refuser / insister)', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 12, 'http://www.podcastfrancaisfacile.com/podcast/2013/06/a-propos-dune-sortie-proposer-refuser-accepter.html', NULL),
(126, ' situation informelle', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 12, 'http://www.podcastfrancaisfacile.com/dialogue/2015/07/fixer-un-rendez-vous-2.html', NULL),
(127, 'enquête pour donner son avis (adverbes de fréquence)', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 13, 'http://www.podcastfrancaisfacile.com/podcast/2013/12/dialogue-enquete-loisir-adverbes-de-frequence.html', NULL),
(128, 'interview d’un joueur de tennis (la durée au présent)', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 13, 'http://www.podcastfrancaisfacile.com/podcast/2013/12/la-duree-au-present-faire-du-sport.html', NULL),
(129, 'interview : expatriation 1', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 13, 'http://www.podcastfrancaisfacile.com/podcast/2014/03/expatriation-interview.html', NULL),
(130, 'interview : expatriation 2', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 13, 'http://www.podcastfrancaisfacile.com/dialogue/2015/02/travailler-au-canada.html', NULL),
(131, 'interview à la brocante', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 13, 'http://www.podcastfrancaisfacile.com/podcast/2011/06/la-brocante-interview.html', NULL),
(132, 'manifestation contre l&rsquo;austérité', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 13, 'http://www.podcastfrancaisfacile.com/dialogue/2015/03/manifestation-contre-lausterite.html', NULL),
(133, 'rencontre avec le professeur', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 14, 'http://www.podcastfrancaisfacile.com/podcast/2014/02/rencontre-avec-le-professeur-des-ecoles.html', NULL),
(134, 'suivi scolaire', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 14, 'http://www.podcastfrancaisfacile.com/podcast/2014/10/avec-le-professeur-des-ecoles-suivi-scolaire.html', NULL),
(135, 'problème de comportement en classe', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 14, 'http://www.podcastfrancaisfacile.com/podcast/2014/02/probleme-de-comportement-en-classe.html', NULL),
(136, 'interdictions', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 14, 'http://www.podcastfrancaisfacile.com/podcast/2013/11/interdictions.html', NULL),
(137, 'entre le directeur et un professeur (les indéfinis 1/2)', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 14, 'http://www.podcastfrancaisfacile.com/podcast/2013/12/au-lycee-entre-le-directeur-et-un-professeur-les-indefinis-1-2.html', NULL),
(138, 'discussion avec le professeur de math', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 14, 'http://www.podcastfrancaisfacile.com/non-classe/2015/07/discussion-avec-le-professeur-de-maths.html', NULL),
(139, 'parler de sa région', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 15, 'http://www.podcastfrancaisfacile.com/podcast/2013/10/parler-de-sa-region-.html', NULL),
(140, 'parler de ses origines', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 15, 'http://www.podcastfrancaisfacile.com/podcast/2013/10/dialogue-en-francais-parler-de-sa-region-1.html', NULL),
(141, 'parler de son week-end', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 15, 'http://www.podcastfrancaisfacile.com/podcast/2013/11/parler-de-son-week-end.html', NULL),
(142, 'parler des vacances : situation 1', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 15, 'http://www.podcastfrancaisfacile.com/podcast/2013/11/parler-de-ses-vacances.html', NULL),
(143, 'parler des vacances : situation 2', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 15, 'http://www.podcastfrancaisfacile.com/podcast/2013/12/parler-de-ses-vacances-2.html', NULL),
(144, 'parler de deux personnes (comparer)', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 15, 'http://www.podcastfrancaisfacile.com/podcast/2014/01/comparer-comparaison-dialogue.html', NULL),
(145, 'échanges (quelqu&rsquo;un / quelque chose)', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 15, 'http://www.podcastfrancaisfacile.com/podcast/2013/12/echanges-quelquun-personne.html', NULL),
(146, 'la nouvelle stagiaire', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 15, 'http://www.podcastfrancaisfacile.com/podcast/2014/01/francais-authentique-dialogue-gratuit-fle-la-nouvelle-stagiaire.html', NULL),
(147, 'prise de poids', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 15, 'http://www.podcastfrancaisfacile.com/dialogue/2015/01/faire-un-regime.html', NULL),
(148, 'parler de son régime', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 15, 'http://www.podcastfrancaisfacile.com/dialogue/2015/01/reussir-son-regime.html', NULL),
(149, 'enquête pour le cours de français', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 16, 'http://www.podcastfrancaisfacile.com/podcast/2013/12/donner-son-avis-adverbe-adjectif.html', NULL),
(150, 'parler d’un film', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 16, 'http://www.podcastfrancaisfacile.com/podcast/2013/12/parler-dun-film.html', NULL),
(151, 'cinéma (spectateur mécontent)', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 16, 'http://www.podcastfrancaisfacile.com/podcast/2014/02/a-la-sortie-du-film-spectateurs-plutot-mecontents.html', NULL),
(152, 'cinéma (spectateur satisfait)', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 16, 'http://www.podcastfrancaisfacile.com/podcast/2014/02/a-la-sortie-du-cinema-spectateurs-satisfaits.html', NULL),
(153, 'à propos des élections', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 16, 'http://www.podcastfrancaisfacile.com/podcast/2014/06/parler-de-politique-en-cours-de-fle-francais-langue-etrangere-dialogue.html', NULL),
(154, 'le verre de trop', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 16, 'http://www.podcastfrancaisfacile.com/podcast/2014/01/le-verre-de-trop.html', NULL),
(155, 'rénovation appartement', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 16, 'http://www.podcastfrancaisfacile.com/podcast/2014/06/renovation-appartement.html', NULL),
(156, 'misogyne', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 16, 'http://www.podcastfrancaisfacile.com/podcast/2014/12/misogyne-dialogue-intermediaire-francais.html', NULL),
(157, 'parler politique', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 16, 'http://www.podcastfrancaisfacile.com/podcast/2014/11/politique-liberal-vs-socialiste.html', NULL),
(158, 'changement de poste', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 16, 'http://www.podcastfrancaisfacile.com/podcast/2014/11/donner-son-opinion-dialogue-fle.html', NULL),
(159, 'parler d&rsquo;un roman', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 16, 'http://www.podcastfrancaisfacile.com/dialogue/2015/02/parler-dun-roman-policier.html', NULL),
(160, 'parler d&rsquo;un roman 2', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 16, 'http://www.podcastfrancaisfacile.com/dialogue/2015/03/dialogue-fle-parler-dun-livre.html', NULL),
(161, 'salaire joueurs de foot', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 16, 'http://www.podcastfrancaisfacile.com/dialogue/2015/06/prix-des-transferts-au-football.html', NULL),
(162, 'travail le dimanche 1/3', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 16, 'http://www.podcastfrancaisfacile.com/dialogue/2015/04/travail-le-dimanche.html', NULL),
(163, 'travail le dimanche 2/3', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 16, 'http://www.podcastfrancaisfacile.com/dialogue/2015/04/travail-le-dimanche-23.html', NULL),
(164, 'travail le dimanche 3/3', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 16, 'http://www.podcastfrancaisfacile.com/dialogue/2015/04/travail-le-dimanche-avis-des-commercants-33.html', NULL),
(165, 'intéressé/intéressant', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 16, 'http://www.podcastfrancaisfacile.com/dialogue/2015/04/interesse-interessant-sinteresser-a.html', NULL),
(166, 'l&rsquo;uniforme à l&rsquo;école', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 16, 'http://www.podcastfrancaisfacile.com/dialogue/2015/08/uniforme-ecole.html', NULL),
(167, 'la réforme de l&rsquo;orthographe', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 16, 'http://www.podcastfrancaisfacile.com/dialogue/2016/02/la-reforme-de-lorthographe.html', NULL),
(168, 'à la boulangerie', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 17, 'http://www.podcastfrancaisfacile.com/dialogue/2008/03/la-boulangerie-1.html', NULL),
(169, 'à la boulangerie 2/2', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 17, 'http://www.podcastfrancaisfacile.com/dialogue/2015/02/acheter-du-pain-a-la-boulangerie.html', NULL),
(170, 'à la boucherie', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 17, 'http://www.podcastfrancaisfacile.com/dialogue/2008/04/bonjour-bienven-1.html', NULL),
(171, 'au marché', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 17, 'http://www.podcastfrancaisfacile.com/podcast/2013/05/au-marche-faire-ses-courses-dialogue-fle-francais-apprendre-learn.html', NULL),
(172, 'chez le fromager 1/3', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 17, 'http://www.podcastfrancaisfacile.com/podcast/2014/02/chez-le-fromager-13.html', NULL),
(173, 'chez le fromager 2/3', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 17, 'http://www.podcastfrancaisfacile.com/podcast/2014/02/chez-le-fromager-23.html', NULL),
(174, 'chez le fromager 3/3', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 17, 'http://www.podcastfrancaisfacile.com/communication/2014/02/dialogue-chez-le-fromager-33.html', NULL),
(175, 'chez le volailler', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 17, 'http://www.podcastfrancaisfacile.com/dialogue/2015/07/poulet-de-bresse.html', NULL),
(176, 'au café', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 17, 'http://www.podcastfrancaisfacile.com/podcast/2008/03/au-caf-1.html', NULL),
(177, 'à la poste', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 17, 'http://www.podcastfrancaisfacile.com/podcast/2013/05/dialogue-a-la-poste.html', NULL),
(178, 'à la gare', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 17, 'http://www.podcastfrancaisfacile.com/podcast/2013/05/a-la-gare.html', NULL),
(179, 'à la pharmacie', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 17, 'http://www.podcastfrancaisfacile.com/podcast/2008/04/la-pharmacie-1.html', NULL),
(180, 'prendre le taxi', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 17, 'http://www.podcastfrancaisfacile.com/podcast/2014/11/prendre-le-taxi.html', NULL),
(181, 'situations diverses', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 17, 'http://www.podcastfrancaisfacile.com/podcast/2014/09/situations-diverses.html', NULL),
(182, 'à la bijouterie (hésiter)', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 17, 'http://www.podcastfrancaisfacile.com/podcast/2013/09/hesiter-dans-une-bijouterie.html', NULL),
(183, 'acheter une cravate', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 17, 'http://www.podcastfrancaisfacile.com/podcast/2014/10/marchand-de-vetements-achat-dune-cravate.html', NULL),
(184, 'Qui est-ce ? 1/2', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 18, 'http://www.podcastfrancaisfacile.com/podcast/2014/07/stage-linguistique-en-france-qui-est-ce-1.html', NULL),
(185, 'Qui est-ce ? 2/2', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 18, 'http://www.podcastfrancaisfacile.com/debutant/2015/03/qui-est-ce-dialogue-2.html', NULL),
(186, 'à l’aéroport', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 18, 'http://www.podcastfrancaisfacile.com/podcast/2014/03/sejour-en-france-premiere-rencontre-de-la-famille-daccueil.html', NULL),
(187, 'la visite de la chambre 1/3', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 18, 'http://www.podcastfrancaisfacile.com/debutant/2015/08/dialogue-francais-pour-debutant.html', NULL),
(188, 'la visite de la chambre 2/3', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 18, 'http://www.podcastfrancaisfacile.com/dialogue/2015/08/apprendre-le-francais-visite-de-la-chambre-2.html', NULL),
(189, 'famille d’accueil', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 18, 'http://www.podcastfrancaisfacile.com/podcast/2014/03/chez-la-famille-daccueil-premiere-rencontre.html', NULL),
(190, 'à l’école de langue 1/2', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 18, 'http://www.podcastfrancaisfacile.com/podcast/2014/07/rencontre-comment-adresser-la-parole-a-quelque-un.html', NULL),
(191, 'à l’école de langue 2/2', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 18, 'http://www.podcastfrancaisfacile.com/podcast/2014/07/rencontre-2.html', NULL),
(192, 'questions au prof', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 18, 'http://www.podcastfrancaisfacile.com/podcast/2014/07/poser-des-questions-au-professeur.html', NULL),
(193, 'demander un service', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 18, 'http://www.podcastfrancaisfacile.com/podcast/2014/07/demander-un-service-en-classe.html', NULL),
(194, '50 phrases pour communiquer en cours de langue', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 18, 'http://www.podcastfrancaisfacile.com/podcast/2013/03/50phrases.html', NULL),
(195, 'parler de projets', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 18, 'http://www.podcastfrancaisfacile.com/podcast/2014/08/projet-pour-le-lendemain.html', NULL),
(196, 'sortie avec l’école', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 18, 'http://www.podcastfrancaisfacile.com/podcast/2014/08/sortie-avec-lecole.html', NULL),
(197, 'après la sortie', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 18, 'http://www.podcastfrancaisfacile.com/podcast/2014/08/apres-une-sortie.html', NULL),
(198, 'devoirs (les sigles)', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 18, 'http://www.podcastfrancaisfacile.com/dialogue/2015/09/sigles-et-acronymes.html', NULL),
(199, 'mission', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 19, 'http://www.podcastfrancaisfacile.com/dialogue/2015/05/accueil-client-dialogue-1-7.html', NULL),
(200, 'conseiller un collègue', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 19, 'http://www.podcastfrancaisfacile.com/dialogue/2015/05/accueil-client-27.html', NULL),
(201, 'le programme', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 19, 'http://www.podcastfrancaisfacile.com/dialogue/2015/05/accueil-client-37.html', NULL),
(202, ' à l&rsquo;aéroport', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 19, 'http://www.podcastfrancaisfacile.com/dialogue/2015/05/accueil-client-47.html', NULL),
(203, 'dans le taxi', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 19, 'http://www.podcastfrancaisfacile.com/dialogue/2015/05/accueil-client-57-francais-des-affaires.html', NULL),
(204, 'après la réunion', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 19, 'http://www.podcastfrancaisfacile.com/dialogue/2015/06/accueil-client.html', NULL),
(205, 'le compte rendu', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 19, 'http://www.podcastfrancaisfacile.com/dialogue/2015/06/accueil-client-77-francais-des-affaires.html', NULL),
(206, 'au fastfood', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 19, 'http://www.podcastfrancaisfacile.com/podcast/2014/11/entretien-dembauche-au-fastfood.html', NULL),
(207, 'cours particuliers', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 19, 'http://www.podcastfrancaisfacile.com/podcast/2014/12/entretien-dembauche-par-telephone-pour-des-cours-particuliers.html', NULL),
(208, ' job au camping', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 19, 'http://www.podcastfrancaisfacile.com/podcast/2014/12/entretien-dembauche-pour-un-job-dans-un-camping.html', NULL),
(209, 'compte rendu', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 19, 'http://www.podcastfrancaisfacile.com/podcast/2014/11/au-bureau-compte-rendu.html', NULL),
(210, 'vendanges', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 19, 'http://www.podcastfrancaisfacile.com/podcast/2014/01/premier-jour-de-vendanges.html', NULL),
(211, 'entre le directeur et la secrétaire', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 19, 'http://www.podcastfrancaisfacile.com/podcast/2014/08/discussion-entre-directeur-et-secretaire-1.html', NULL),
(212, 'attente confirmation 1/2 ', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 19, 'http://www.podcastfrancaisfacile.com/podcast/2014/12/remplacement-12-attente-de-confirmation.html', NULL),
(213, 'confirmation 2/2', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 19, 'http://www.podcastfrancaisfacile.com/communication/2015/01/nourrice-paris.html', NULL),
(214, 'bricoler', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 19, 'http://www.podcastfrancaisfacile.com/podcast/2014/11/brico-macho.html', NULL),
(215, 'le pot de départ', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 19, 'http://www.podcastfrancaisfacile.com/dialoguer/2015/01/affaires-pot-de-depart.html', NULL),
(216, 'la livraison de boudin', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 19, 'http://www.podcastfrancaisfacile.com/dialogue/2015/01/la-livraison-de-boudin.html', NULL),
(217, 'louer une voiture', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 20, 'http://www.podcastfrancaisfacile.com/podcast/2014/01/a-lagence-de-location.html', NULL),
(218, 'louer un appartement', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 20, 'http://www.podcastfrancaisfacile.com/podcast/2012/01/dialogue-location.html', NULL),
(219, 'visiter une maison', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 20, 'http://www.podcastfrancaisfacile.com/podcast/2014/02/visite-dune-maison.html', NULL),
(220, 'louer des vélos 1/3', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 20, 'http://www.podcastfrancaisfacile.com/podcast/2014/09/louer-des-velos-se-renseigner.html', NULL),
(221, 'louer des vélos 2/3', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 20, 'http://www.podcastfrancaisfacile.com/podcast/2014/09/louer-un-velo-23-au-moment-de-la-location.html', NULL),
(222, 'louer des vélos 3/3', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 20, 'http://www.podcastfrancaisfacile.com/podcast/2014/10/louer-un-velo-33-au-moment-du-retour.html', NULL),
(223, 'accueil', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 21, 'http://www.podcastfrancaisfacile.com/podcast/2014/03/recevoir-un-ami-pour-la-premiere-fois-accueil.html', NULL),
(224, 'apéritif', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 21, 'http://www.podcastfrancaisfacile.com/podcast/2014/03/dialogue-a-laperitif.html', NULL),
(225, 'pendant le repas', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 21, 'http://www.podcastfrancaisfacile.com/podcast/2014/03/inviter-recevoir-des-amis-pendant-le-repas.html', NULL),
(226, 'dire au revoir', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 21, 'http://www.podcastfrancaisfacile.com/podcast/2014/03/invitation-chez-des-amis-au-moment-de-dire-au-revoir.html', NULL),
(227, 'avant le dîner', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 21, 'http://www.podcastfrancaisfacile.com/dialogue/2015/07/diner-entre-amis.html', NULL),
(228, ' la grève &#8211; proposer son aide', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 21, 'http://www.podcastfrancaisfacile.com/dialogue/2015/08/solidarite-dialogue-fle.html', NULL),
(229, ' ', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 21, 'http://www.podcastfrancaisfacile.com/dialogue/2015/08/solidarite-dialogue-fle.html', NULL),
(230, 'la grève (informer)', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 21, 'http://www.podcastfrancaisfacile.com/dialogue/2015/08/la-greve-systeme-d.html', NULL),
(231, ' ', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 21, 'http://www.podcastfrancaisfacile.com/dialogue/2015/08/solidarite-dialogue-fle.html', NULL),
(232, 'Système D 2/3 (marchandage)', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 21, 'http://www.podcastfrancaisfacile.com/dialogue/2015/08/dialogue-en-francais-marchandage.html', NULL),
(233, 'Système D 3/3', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 21, 'http://www.podcastfrancaisfacile.com/dialogue/2015/08/systeme-d-33-accident.html', NULL),
(234, 'à la poste', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 22, 'http://www.podcastfrancaisfacile.com/podcast/2013/11/protester.html', NULL),
(235, 'crotte de chien', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 22, 'http://www.podcastfrancaisfacile.com/podcast/2014/01/crotte-de-chien.html', NULL),
(236, 'dans un restaurant', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 22, 'http://www.podcastfrancaisfacile.com/dialogue/2015/02/se-plaindre-du-service-dans-un-restaurant-au-moment-de-regler-laddition.html', NULL),
(237, 'à propos d&rsquo;un ami', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 22, 'http://www.podcastfrancaisfacile.com/podcast/2006/08/retard.html', NULL),
(238, 'au service après-vente (SAV)', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 22, 'http://www.podcastfrancaisfacile.com/dialogue/2015/06/service-apres-vente-sav.html', NULL),
(239, 'un voisin bruyant', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 22, 'http://www.podcastfrancaisfacile.com/dialogue/2015/06/voisin-trop-bruyant.html', NULL),
(240, 'vandalisme', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 22, 'http://www.podcastfrancaisfacile.com/dialogue/2015/04/vandalisme.html', NULL),
(241, 'la grève', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 22, 'http://www.podcastfrancaisfacile.com/dialogue/2015/08/manifester-son-mecontentement-dialogue-fle.html', NULL),
(242, 'centre sportif', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 23, 'http://www.podcastfrancaisfacile.com/podcast/2014/02/se-renseigner-aupres-dun-centre-sportif.html', NULL),
(243, 'à l&rsquo;office de tourisme', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 23, 'http://www.podcastfrancaisfacile.com/podcast/2014/11/office-de-tourisme-de-rochefort.html', NULL),
(244, 'demander son chemin dans la rue', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 23, 'http://www.podcastfrancaisfacile.com/podcast/2013/05/demander-son-chemin-dans-la-rue.html', NULL),
(245, 'demander sa route', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 23, 'http://www.podcastfrancaisfacile.com/podcast/2013/05/sur-la-route.html', NULL),
(246, 'au commissariat : vol', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 23, 'http://www.podcastfrancaisfacile.com/podcast/2014/11/au-commissariat.html', NULL),
(247, 'renseignement cours d&rsquo;anglais', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 23, 'http://www.podcastfrancaisfacile.com/podcast/2014/11/renseignement-apprendre-langlais-cours-danglais.html', NULL),
(248, 'le test d&rsquo;évaluation &#8211; cours d&rsquo;anglais', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 23, 'http://www.podcastfrancaisfacile.com/podcast/2014/11/le-test-devaluation-anglais.html', NULL),
(249, 'différence entre acheter &amp; s’acheter', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 23, 'http://www.podcastfrancaisfacile.com/podcast/2013/08/difference-entre-acheter-et-sacheter.html', NULL),
(250, 'à propos d’un travail', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 23, 'http://www.podcastfrancaisfacile.com/podcast/2013/06/recherche-travail-france.html', NULL),
(251, 'demander une aide &#8211; expliquer le fonctionnement d’une machine', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 23, 'http://www.podcastfrancaisfacile.com/podcast/2013/07/demander-une-aider-expliquer-francais-french-dialogue.html', NULL),
(252, 'demander une aide &#8211; imprécisions', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 23, 'http://www.podcastfrancaisfacile.com/podcast/2014/12/imprecisions-demander-de-laide.html', NULL),
(253, 'la séance photo (féliciter)', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 23, 'http://www.podcastfrancaisfacile.com/podcast/2014/07/seance-photo-imperatif-complimenter.html', NULL),
(254, 'horoscope (expression du temps)', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 23, 'http://www.podcastfrancaisfacile.com/podcast/2013/12/-lhoroscope-lexpression-du-temps.html', NULL),
(255, 'parler de son appartement', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 23, 'http://www.podcastfrancaisfacile.com/podcast/2013/12/parler-du-lieu-ou-on-vit-expression-de-lieu-en-francais-fle.html', NULL),
(256, 'parler de son appartement', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 23, 'http://www.podcastfrancaisfacile.com/podcast/2013/12/parler-du-lieu-ou-on-vit-expression-de-lieu-en-francais-fle.html', NULL),
(257, 'astuce pour démouler un cake', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 23, 'http://www.podcastfrancaisfacile.com/dialogue/2015/06/comment-demouler-un-cake-premier-ou-second-degre.html', NULL),
(258, 'conseiller un site touristique', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 23, 'http://www.podcastfrancaisfacile.com/dialogue/2015/04/mont-saint-michel.html', NULL),
(259, 'la chasse aux champignons', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 23, 'http://www.podcastfrancaisfacile.com/dialogue/2015/10/chasse-aux-champignons.html', NULL),
(260, 'laisser un message informel sur répondeur ', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 24, 'http://www.podcastfrancaisfacile.com/texte/2015/11/laisser-un-message-informel-comprendre-un-message.html', NULL),
(261, 'laisser un message', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 24, 'http://www.podcastfrancaisfacile.com/podcast/2013/11/comment-repondre-au-telephone-telephoner-laisser-un-message-francais.html', NULL),
(262, 'prendre rendez-vous pour une urgence', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 24, 'http://www.podcastfrancaisfacile.com/podcast/2014/10/prendre-un-rendez-vous-pour-une-urgence.html', NULL),
(263, 'prendre rendez-vous par téléphone', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 24, 'http://www.podcastfrancaisfacile.com/dialogue/2015/04/prendre-rendez-vous-par-telephone.html', NULL),
(264, 'le portefeuille oublié', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 24, 'http://www.podcastfrancaisfacile.com/podcast/2014/12/le-portefeuille-oublie.html', NULL),
(265, 'à propos du temps qu&rsquo;il fait', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 25, 'http://www.podcastfrancaisfacile.com/podcast/2013/06/avec-la-voisine-a-propos-du-temps.html', NULL),
(266, 'un oubli', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 25, 'http://www.podcastfrancaisfacile.com/podcast/2013/09/dialogue-learn-french-conversation-free.html', NULL),
(267, 'étourdi &#8211; demander de l&rsquo;aide', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 25, 'http://www.podcastfrancaisfacile.com/podcast/2014/07/etourdi-humour-chercher-trouver-un-objet-demander.html', NULL),
(268, 'dépêchons-nous ! (entre une mère et son fils)', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 25, 'http://www.podcastfrancaisfacile.com/podcast/2013/11/depechons-nous-forme-imperative-francais.html', NULL);
INSERT INTO `ff_dialogues` (`id`, `name`, `content`, `icon_url`, `audio_url`, `created_on`, `inspector_id`, `recycled`, `updated_on`, `updated_by_id`, `approved`, `category_id`, `content_url`, `created_by_id`) VALUES
(269, 'changement de vêtement', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 25, 'http://www.podcastfrancaisfacile.com/podcast/2014/06/changement-de-vetement.html', NULL),
(270, 'la liste de courses 1', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 25, 'http://www.podcastfrancaisfacile.com/podcast/2013/10/la-liste-de-course.html', NULL),
(271, 'la liste de courses 2', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 25, 'http://www.podcastfrancaisfacile.com/podcast/2014/03/la-liste-de-courses-2-.html', NULL),
(272, 'le retour des courses', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 25, 'http://www.podcastfrancaisfacile.com/podcast/2014/03/retour-des-courses.html', NULL),
(273, 'à propos d&rsquo;un changement de comportement (subjonctif)', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 25, 'http://www.podcastfrancaisfacile.com/podcast/2014/02/changement-de-comportement-expression-du-doute-subjonctif.html', NULL),
(274, 'malade', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 25, 'http://www.podcastfrancaisfacile.com/podcast/2014/08/malade.html', NULL),
(275, 'gueule de bois', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 25, 'http://www.podcastfrancaisfacile.com/podcast/2014/08/gueule-de-bois.html', NULL),
(276, 'dispute autour d&rsquo;un film', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 25, 'http://www.podcastfrancaisfacile.com/podcast/2014/02/dispute-autour-dun-film.html', NULL),
(277, 'dispute cadeau d&rsquo;anniversaire', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 25, 'http://www.podcastfrancaisfacile.com/podcast/2014/10/donner-son-avis-cadeau-quon-naime-pas.html', NULL),
(278, 'les devoirs', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 25, 'http://www.podcastfrancaisfacile.com/podcast/2014/11/les-devoirs-pronom-en-y.html', NULL),
(279, 'le cadeau d&rsquo;anniversaire : ironie', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 25, 'http://www.podcastfrancaisfacile.com/podcast/2014/11/cadeau-danniversaire-ironie.html', NULL),
(280, 'la leçon de piano', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 25, 'http://www.podcastfrancaisfacile.com/grammaire/2015/01/a-propos-du-piano-pronom-en.html', NULL),
(281, 'expliquer comment on s’est blessé', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 26, 'http://www.podcastfrancaisfacile.com/podcast/2013/06/chez-le-docteur.html', NULL),
(282, 'problème à la gorge', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 26, 'http://www.podcastfrancaisfacile.com/podcast/2014/09/chez-le-medecin.html', NULL),
(283, 'blessure grave', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 26, 'http://www.podcastfrancaisfacile.com/podcast/2014/10/blessure-grave-dialogue-fle.html', NULL),
(284, 'problème de tension', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 26, 'http://www.podcastfrancaisfacile.com/podcast/2014/10/probleme-de-tension.html', NULL),
(285, 'tour de reins', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 26, 'http://www.podcastfrancaisfacile.com/podcast/2014/10/dialogue-chez-le-docteur-fle-tour-de-reins.html', NULL),
(286, 'tour de reins', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 26, 'http://www.podcastfrancaisfacile.com/podcast/2014/10/dialogue-chez-le-docteur-fle-tour-de-reins.html', NULL),
(287, 'mal à l&rsquo;estomac', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 26, 'http://www.podcastfrancaisfacile.com/dialogue/2016/05/dialogue-chez-docteur.html', NULL),
(288, 'à la pharmacie (champignons)', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 26, 'http://www.podcastfrancaisfacile.com/dialogue/2015/11/verification-champignon-pharmacie.html', NULL),
(289, 'réserver une table au restaurant', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 27, 'http://www.podcastfrancaisfacile.com/podcast/2013/08/reserver-une-table-au-restaurant.html', NULL),
(290, 'modifier une réservation (au restaurant)', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 27, 'http://www.podcastfrancaisfacile.com/podcast/2013/08/modifier-une-reservation-au-restaurant.html', NULL),
(291, 'prendre rendez-vous chez le coiffeur', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 27, 'http://www.podcastfrancaisfacile.com/podcast/2014/03/prendre-rendez-vous-chez-le-coiffeur.html', NULL),
(292, 'entre amis (inviter / insister / refuser)', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 27, 'http://www.podcastfrancaisfacile.com/podcast/2013/06/inviter-quelquun-insister-refuser.html', NULL),
(293, 'à propos d’une sortie (proposer / refuser / insister)', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 27, 'http://www.podcastfrancaisfacile.com/podcast/2013/06/a-propos-dune-sortie-proposer-refuser-accepter.html', NULL),
(294, ' situation informelle', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 27, 'http://www.podcastfrancaisfacile.com/dialogue/2015/07/fixer-un-rendez-vous-2.html', NULL),
(295, 'enquête pour donner son avis (adverbes de fréquence)', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 28, 'http://www.podcastfrancaisfacile.com/podcast/2013/12/dialogue-enquete-loisir-adverbes-de-frequence.html', NULL),
(296, 'interview d’un joueur de tennis (la durée au présent)', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 28, 'http://www.podcastfrancaisfacile.com/podcast/2013/12/la-duree-au-present-faire-du-sport.html', NULL),
(297, 'interview : expatriation 1', '', '', '', 1465378492, NULL, 0, NULL, NULL, 0, 28, 'http://www.podcastfrancaisfacile.com/podcast/2014/03/expatriation-interview.html', NULL),
(298, 'interview : expatriation 2', '', '', '', 1465378493, NULL, 0, NULL, NULL, 0, 28, 'http://www.podcastfrancaisfacile.com/dialogue/2015/02/travailler-au-canada.html', NULL),
(299, 'interview à la brocante', '', '', '', 1465378493, NULL, 0, NULL, NULL, 0, 28, 'http://www.podcastfrancaisfacile.com/podcast/2011/06/la-brocante-interview.html', NULL),
(300, 'manifestation contre l&rsquo;austérité', '', '', '', 1465378493, NULL, 0, NULL, NULL, 0, 28, 'http://www.podcastfrancaisfacile.com/dialogue/2015/03/manifestation-contre-lausterite.html', NULL),
(301, 'rencontre avec le professeur', '', '', '', 1465378493, NULL, 0, NULL, NULL, 0, 29, 'http://www.podcastfrancaisfacile.com/podcast/2014/02/rencontre-avec-le-professeur-des-ecoles.html', NULL),
(302, 'suivi scolaire', '', '', '', 1465378493, NULL, 0, NULL, NULL, 0, 29, 'http://www.podcastfrancaisfacile.com/podcast/2014/10/avec-le-professeur-des-ecoles-suivi-scolaire.html', NULL),
(303, 'problème de comportement en classe', '', '', '', 1465378493, NULL, 0, NULL, NULL, 0, 29, 'http://www.podcastfrancaisfacile.com/podcast/2014/02/probleme-de-comportement-en-classe.html', NULL),
(304, 'interdictions', '', '', '', 1465378493, NULL, 0, NULL, NULL, 0, 29, 'http://www.podcastfrancaisfacile.com/podcast/2013/11/interdictions.html', NULL),
(305, 'entre le directeur et un professeur (les indéfinis 1/2)', '', '', '', 1465378493, NULL, 0, NULL, NULL, 0, 29, 'http://www.podcastfrancaisfacile.com/podcast/2013/12/au-lycee-entre-le-directeur-et-un-professeur-les-indefinis-1-2.html', NULL),
(306, 'discussion avec le professeur de math', '', '', '', 1465378493, NULL, 0, NULL, NULL, 0, 29, 'http://www.podcastfrancaisfacile.com/non-classe/2015/07/discussion-avec-le-professeur-de-maths.html', NULL),
(307, 'parler de sa région', '', '', '', 1465378493, NULL, 0, NULL, NULL, 0, 30, 'http://www.podcastfrancaisfacile.com/podcast/2013/10/parler-de-sa-region-.html', NULL),
(308, 'parler de ses origines', '', '', '', 1465378493, NULL, 0, NULL, NULL, 0, 30, 'http://www.podcastfrancaisfacile.com/podcast/2013/10/dialogue-en-francais-parler-de-sa-region-1.html', NULL),
(309, 'parler de son week-end', '', '', '', 1465378493, NULL, 0, NULL, NULL, 0, 30, 'http://www.podcastfrancaisfacile.com/podcast/2013/11/parler-de-son-week-end.html', NULL),
(310, 'parler des vacances : situation 1', '', '', '', 1465378493, NULL, 0, NULL, NULL, 0, 30, 'http://www.podcastfrancaisfacile.com/podcast/2013/11/parler-de-ses-vacances.html', NULL),
(311, 'parler des vacances : situation 2', '', '', '', 1465378493, NULL, 0, NULL, NULL, 0, 30, 'http://www.podcastfrancaisfacile.com/podcast/2013/12/parler-de-ses-vacances-2.html', NULL),
(312, 'parler de deux personnes (comparer)', '', '', '', 1465378493, NULL, 0, NULL, NULL, 0, 30, 'http://www.podcastfrancaisfacile.com/podcast/2014/01/comparer-comparaison-dialogue.html', NULL),
(313, 'échanges (quelqu&rsquo;un / quelque chose)', '', '', '', 1465378493, NULL, 0, NULL, NULL, 0, 30, 'http://www.podcastfrancaisfacile.com/podcast/2013/12/echanges-quelquun-personne.html', NULL),
(314, 'la nouvelle stagiaire', '', '', '', 1465378493, NULL, 0, NULL, NULL, 0, 30, 'http://www.podcastfrancaisfacile.com/podcast/2014/01/francais-authentique-dialogue-gratuit-fle-la-nouvelle-stagiaire.html', NULL),
(315, 'prise de poids', '', '', '', 1465378493, NULL, 0, NULL, NULL, 0, 30, 'http://www.podcastfrancaisfacile.com/dialogue/2015/01/faire-un-regime.html', NULL),
(316, 'parler de son régime', '', '', '', 1465378493, NULL, 0, NULL, NULL, 0, 30, 'http://www.podcastfrancaisfacile.com/dialogue/2015/01/reussir-son-regime.html', NULL),
(317, 'enquête pour le cours de français', '', '', '', 1465378493, NULL, 0, NULL, NULL, 0, 31, 'http://www.podcastfrancaisfacile.com/podcast/2013/12/donner-son-avis-adverbe-adjectif.html', NULL),
(318, 'parler d’un film', '', '', '', 1465378493, NULL, 0, NULL, NULL, 0, 31, 'http://www.podcastfrancaisfacile.com/podcast/2013/12/parler-dun-film.html', NULL),
(319, 'cinéma (spectateur mécontent)', '', '', '', 1465378493, NULL, 0, NULL, NULL, 0, 31, 'http://www.podcastfrancaisfacile.com/podcast/2014/02/a-la-sortie-du-film-spectateurs-plutot-mecontents.html', NULL),
(320, 'cinéma (spectateur satisfait)', '', '', '', 1465378493, NULL, 0, NULL, NULL, 0, 31, 'http://www.podcastfrancaisfacile.com/podcast/2014/02/a-la-sortie-du-cinema-spectateurs-satisfaits.html', NULL),
(321, 'à propos des élections', '', '', '', 1465378493, NULL, 0, NULL, NULL, 0, 31, 'http://www.podcastfrancaisfacile.com/podcast/2014/06/parler-de-politique-en-cours-de-fle-francais-langue-etrangere-dialogue.html', NULL),
(322, 'le verre de trop', '', '', '', 1465378493, NULL, 0, NULL, NULL, 0, 31, 'http://www.podcastfrancaisfacile.com/podcast/2014/01/le-verre-de-trop.html', NULL),
(323, 'rénovation appartement', '', '', '', 1465378493, NULL, 0, NULL, NULL, 0, 31, 'http://www.podcastfrancaisfacile.com/podcast/2014/06/renovation-appartement.html', NULL),
(324, 'misogyne', '', '', '', 1465378493, NULL, 0, NULL, NULL, 0, 31, 'http://www.podcastfrancaisfacile.com/podcast/2014/12/misogyne-dialogue-intermediaire-francais.html', NULL),
(325, 'parler politique', '', '', '', 1465378493, NULL, 0, NULL, NULL, 0, 31, 'http://www.podcastfrancaisfacile.com/podcast/2014/11/politique-liberal-vs-socialiste.html', NULL),
(326, 'changement de poste', '', '', '', 1465378493, NULL, 0, NULL, NULL, 0, 31, 'http://www.podcastfrancaisfacile.com/podcast/2014/11/donner-son-opinion-dialogue-fle.html', NULL),
(327, 'parler d&rsquo;un roman', '', '', '', 1465378493, NULL, 0, NULL, NULL, 0, 31, 'http://www.podcastfrancaisfacile.com/dialogue/2015/02/parler-dun-roman-policier.html', NULL),
(328, 'parler d&rsquo;un roman 2', '', '', '', 1465378493, NULL, 0, NULL, NULL, 0, 31, 'http://www.podcastfrancaisfacile.com/dialogue/2015/03/dialogue-fle-parler-dun-livre.html', NULL),
(329, 'salaire joueurs de foot', '', '', '', 1465378493, NULL, 0, NULL, NULL, 0, 31, 'http://www.podcastfrancaisfacile.com/dialogue/2015/06/prix-des-transferts-au-football.html', NULL),
(330, 'travail le dimanche 1/3', '', '', '', 1465378493, NULL, 0, NULL, NULL, 0, 31, 'http://www.podcastfrancaisfacile.com/dialogue/2015/04/travail-le-dimanche.html', NULL),
(331, 'travail le dimanche 2/3', '', '', '', 1465378493, NULL, 0, NULL, NULL, 0, 31, 'http://www.podcastfrancaisfacile.com/dialogue/2015/04/travail-le-dimanche-23.html', NULL),
(332, 'travail le dimanche 3/3', '', '', '', 1465378493, NULL, 0, NULL, NULL, 0, 31, 'http://www.podcastfrancaisfacile.com/dialogue/2015/04/travail-le-dimanche-avis-des-commercants-33.html', NULL),
(333, 'intéressé/intéressant', '', '', '', 1465378493, NULL, 0, NULL, NULL, 0, 31, 'http://www.podcastfrancaisfacile.com/dialogue/2015/04/interesse-interessant-sinteresser-a.html', NULL),
(334, 'l&rsquo;uniforme à l&rsquo;école', '', '', '', 1465378493, NULL, 0, NULL, NULL, 0, 31, 'http://www.podcastfrancaisfacile.com/dialogue/2015/08/uniforme-ecole.html', NULL),
(335, 'la réforme de l&rsquo;orthographe', '', '', '', 1465378493, NULL, 0, NULL, NULL, 0, 31, 'http://www.podcastfrancaisfacile.com/dialogue/2016/02/la-reforme-de-lorthographe.html', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` int(11) unsigned NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User');

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) unsigned NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `nlp_languages`
--

CREATE TABLE `nlp_languages` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=165 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `nlp_languages`
--

INSERT INTO `nlp_languages` (`id`, `name`, `code`) VALUES
(1, 'Abkhazian', 'ab'),
(2, 'Afar', 'aa'),
(3, 'Afrikaans', 'af'),
(4, 'Albanian', 'sq'),
(5, 'Amharic', 'am'),
(6, 'Arabic', 'ar'),
(7, 'Aragonese', 'an'),
(8, 'Armenian', 'hy'),
(9, 'Assamese', 'as'),
(10, 'Aymara', 'ay'),
(11, 'Azerbaijani', 'az'),
(12, 'Bashkir', 'ba'),
(13, 'Basque', 'eu'),
(14, 'Bengali(Bangla)', 'bn'),
(15, 'Bhutani', 'dz'),
(16, 'Bihari', 'bh'),
(17, 'Bislama', 'bi'),
(18, 'Breton', 'br'),
(19, 'Bulgarian', 'bg'),
(20, 'Burmese', 'my'),
(21, 'Byelorussian(Belarusian)', 'be'),
(22, 'Cambodian', 'km'),
(23, 'Catalan', 'ca'),
(24, 'Cherokee', NULL),
(25, 'Chewa', NULL),
(26, 'Chinese', 'zh'),
(27, 'Chinese(Simplified)', 'zh-Hans'),
(28, 'Chinese(Traditional)', 'zh-Hant'),
(29, 'Corsican', 'co'),
(30, 'Croatian', 'hr'),
(31, 'Czech', 'cs'),
(32, 'Danish', 'da'),
(33, 'Divehi', NULL),
(34, 'Dutch', 'nl'),
(35, 'Edo', NULL),
(36, 'English', 'en'),
(37, 'Esperanto', 'eo'),
(38, 'Estonian', 'et'),
(39, 'Faeroese', 'fo'),
(40, 'Farsi', 'fa'),
(41, 'Fiji', 'fj'),
(42, 'Finnish', 'fi'),
(43, 'Flemish', NULL),
(44, 'French', 'fr'),
(45, 'Frisian', 'fy'),
(46, 'Fulfulde', NULL),
(47, 'Galician', 'gl'),
(48, 'Gaelic(Scottish)', 'gd'),
(49, 'Gaelic(Manx)', 'gv'),
(50, 'Georgian', 'ka'),
(51, 'German', 'de'),
(52, 'Greek', 'el'),
(53, 'Greenlandic', 'kl'),
(54, 'Guarani', 'gn'),
(55, 'Gujarati', 'gu'),
(56, 'HaitianCreole', 'ht'),
(57, 'Hausa', 'ha'),
(58, 'Hawaiian', NULL),
(59, 'Hebrew', 'he'),
(60, 'Hindi', 'hi'),
(61, 'Hungarian', 'hu'),
(62, 'Ibibio', NULL),
(63, 'Icelandic', 'is'),
(64, 'Ido', 'io'),
(65, 'Igbo', NULL),
(66, 'Indonesian', 'id'),
(67, 'Interlingua', 'ia'),
(68, 'Interlingue', 'ie'),
(69, 'Inuktitut', 'iu'),
(70, 'Inupiak', 'ik'),
(71, 'Irish', 'ga'),
(72, 'Italian', 'it'),
(73, 'Japanese', 'ja'),
(74, 'Javanese', 'jv'),
(75, 'Kannada', 'kn'),
(76, 'Kanuri', NULL),
(77, 'Kashmiri', 'ks'),
(78, 'Kazakh', 'kk'),
(79, 'Kinyarwanda(Ruanda)', 'rw'),
(80, 'Kirghiz', 'ky'),
(81, 'Kirundi(Rundi)', 'rn'),
(82, 'Konkani', NULL),
(83, 'Korean', 'ko'),
(84, 'Kurdish', 'ku'),
(85, 'Laothian', 'lo'),
(86, 'Latin', 'la'),
(87, 'Latvian(Lettish)', 'lv'),
(88, 'Limburgish(Limburger)', 'li'),
(89, 'Lingala', 'ln'),
(90, 'Lithuanian', 'lt'),
(91, 'Macedonian', 'mk'),
(92, 'Malagasy', 'mg'),
(93, 'Malay', 'ms'),
(94, 'Malayalam', 'ml'),
(95, 'Maltese', 'mt'),
(96, 'Maori', 'mi'),
(97, 'Marathi', 'mr'),
(98, 'Moldavian', 'mo'),
(99, 'Mongolian', 'mn'),
(100, 'Nauru', 'na'),
(101, 'Nepali', 'ne'),
(102, 'Norwegian', 'no'),
(103, 'Occitan', 'oc'),
(104, 'Oriya', 'or'),
(105, 'Oromo(AfaanOromo)', 'om'),
(106, 'Papiamentu', NULL),
(107, 'Pashto(Pushto)', 'ps'),
(108, 'Polish', 'pl'),
(109, 'Portuguese', 'pt'),
(110, 'Punjabi', 'pa'),
(111, 'Quechua', 'qu'),
(112, 'Rhaeto-Romance', 'rm'),
(113, 'Romanian', 'ro'),
(114, 'Russian', 'ru'),
(115, 'Sami(Lappish)', NULL),
(116, 'Samoan', 'sm'),
(117, 'Sangro', 'sg'),
(118, 'Sanskrit', 'sa'),
(119, 'Serbian', 'sr'),
(120, 'Serbo-Croatian', 'sh'),
(121, 'Sesotho', 'st'),
(122, 'Setswana', 'tn'),
(123, 'Shona', 'sn'),
(124, 'SichuanYi', 'ii'),
(125, 'Sindhi', 'sd'),
(126, 'Sinhalese', 'si'),
(127, 'Siswati', 'ss'),
(128, 'Slovak', 'sk'),
(129, 'Slovenian', 'sl'),
(130, 'Somali', 'so'),
(131, 'Spanish', 'es'),
(132, 'Sundanese', 'su'),
(133, 'Swahili(Kiswahili)', 'sw'),
(134, 'Swedish', 'sv'),
(135, 'Syriac', NULL),
(136, 'Tagalog', 'tl'),
(137, 'Tajik', 'tg'),
(138, 'Tamazight', NULL),
(139, 'Tamil', 'ta'),
(140, 'Tatar', 'tt'),
(141, 'Telugu', 'te'),
(142, 'Thai', 'th'),
(143, 'Tibetan', 'bo'),
(144, 'Tigrinya', 'ti'),
(145, 'Tonga', 'to'),
(146, 'Tsonga', 'ts'),
(147, 'Turkish', 'tr'),
(148, 'Turkmen', 'tk'),
(149, 'Twi', 'tw'),
(150, 'Uighur', 'ug'),
(151, 'Ukrainian', 'uk'),
(152, 'Urdu', 'ur'),
(153, 'Uzbek', 'uz'),
(154, 'Venda', NULL),
(155, 'Vietnamese', 'vi'),
(156, 'Volapük', 'vo'),
(157, 'Wallon', 'wa'),
(158, 'Welsh', 'cy'),
(159, 'Wolof', 'wo'),
(160, 'Xhosa', 'xh'),
(161, 'Yi', NULL),
(162, 'Yiddish', 'yi'),
(163, 'Yoruba', 'yo'),
(164, 'Zulu', 'zu');

-- --------------------------------------------------------

--
-- Table structure for table `nlp_words`
--

CREATE TABLE `nlp_words` (
  `id` int(11) unsigned NOT NULL,
  `value` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `language` varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'en'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` int(11) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `created_by_id` int(11) unsigned DEFAULT NULL,
  `created_on` int(11) NOT NULL,
  `project_icon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `recycled` tinyint(1) NOT NULL DEFAULT '0',
  `appstore_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `googleplay_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `name`, `description`, `created_by_id`, `created_on`, `project_icon`, `recycled`, `appstore_url`, `googleplay_url`) VALUES
(1, 'Từ điển Pháp-Anh-Việt', 'Tra cứu từ điển 3 trong 1, hỗ trợ phát âm, tra tu online', NULL, 1465047911, 'https://itunes.apple.com/vn/app/mytour-hanoi/id574151408?mt=8', 1, NULL, NULL),
(2, 'Francais Facile', 'AAAAA', NULL, 1465047917, 'http://web.vicnisoft.local/index.php/project/add', 0, NULL, NULL),
(3, 'Francais Facile', 'sacas', NULL, 1456137124, 'scac', 1, NULL, NULL),
(4, 'sascsa', 'saca', NULL, 1456137144, 'sacasc', 1, NULL, NULL),
(5, 'ascascas', 'I installed and configured the ejabberd2 server on my ubuntu server and i mod_muc is enabled by default, but i can''t able to group chat through the XMPP Client App AstraChat in Android and iOS Group Created successfully, but the group members doesn''t receive the messages I searched google for this, i found that muc_admin mod is required, when i enabled it and restarted the ejabberd it shows ejabberd started, but it is not started\r\n\r\ncan anybody help me to figure out what is the problem with the configuration', NULL, 1456137226, 'cascas', 1, NULL, NULL),
(6, 'Câu lạc bộ Liv', 'Lay thong tin fanpage...', NULL, 1465467103, NULL, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `requests`
--

CREATE TABLE `requests` (
  `id` int(11) unsigned NOT NULL,
  `http_user_agent` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `script_uri` int(255) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `requests`
--

INSERT INTO `requests` (`id`, `http_user_agent`, `script_uri`, `created_on`, `user_id`) VALUES
(2, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/601.6.17 (KHTML, like Gecko) Version/9.1.1 Safari/601.6.17', NULL, 1465455209, 32);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT '1',
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `api_key` varchar(128) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`, `api_key`) VALUES
(26, '127.0.0.1', 'ngocongcan', 'a17fbfd0963cfae4b76283ef397ce30f:vJpZ18g79yTwicKIJiQ8FlbunuBiYvXq', NULL, 'admin@vns.com', NULL, NULL, NULL, NULL, 1464888809, NULL, NULL, 'Can', 'Ngo', 'Symfony', NULL, '016bb1a5-f090-4d4f-8af4-40ba37f49a5b'),
(27, '127.0.0.1', 'admin1', 'd550b163d0c95c8e0db4d64040afdf58:e9emCCTamhnwLVwyaRvcZiwXNANMp8sM', NULL, 'admin@vns.com', NULL, NULL, NULL, NULL, 1465483554, NULL, 1, 'Admin', '1', NULL, NULL, '04cf5c34-8c7a-4487-a9f0-5522bdf763d9'),
(29, '127.0.0.1', 'administrator', 'pwd12345-', NULL, 'admin@vns.com', NULL, NULL, NULL, NULL, 1465442661, NULL, NULL, NULL, NULL, NULL, NULL, 'YWZmOGE1OTZkYjY2NjRlZDA2OTQ5YTRhYzM2ZTEzNTcyMWU5YTRlOA'),
(30, '127.0.0.1', 'aaa', 'pwd12345-', NULL, 'admin@vns.com', NULL, NULL, NULL, NULL, 1465443936, NULL, NULL, NULL, NULL, NULL, NULL, 'ODE4YWI0NmYzOThjOTYxM2YzM2ZlN2Y3NTU0MmRkYmQxNjk1MzlmYQ'),
(31, '127.0.0.1', 'aaaaaa', 'pwd12345-', NULL, 'admin@vns.com', NULL, NULL, NULL, NULL, 1465443978, NULL, NULL, NULL, NULL, NULL, NULL, 'ZGJlMzNmOTgwZmNlMzI5NDA4ODQ3YWFmMWRlZjZjMjQ2ZjQ3NjA0Nw'),
(32, '127.0.0.1', 'bbb', 'pwd12345-', NULL, 'admin@vns.com', NULL, NULL, NULL, NULL, 1465444776, NULL, NULL, NULL, NULL, NULL, NULL, 'N2YyNTljMjYzMWI4NjIxM2UzODhhYzU1Y2JiZTkwMjMzNDk1NDg2ZQ'),
(33, '127.0.0.1', 'bbbss', 'pwd12345-', NULL, 'admin@vns.com', NULL, NULL, NULL, NULL, 1465444880, NULL, NULL, NULL, NULL, NULL, NULL, 'MzVmNDQwZTkyYmY4YTE0OTZiNzVjMTE4YTFhM2ZkYTBmMWE3NzAyMw'),
(34, '127.0.0.1', 'bbbaaaa', 'pwd12345-', NULL, 'admin@vns.com', NULL, NULL, NULL, NULL, 1465447095, NULL, NULL, NULL, NULL, NULL, NULL, 'MDEyYzA3ZTEzNDdkMmVhYjBhMTk2MzQ2NTczMDVhNjE4NTk0M2NjMQ'),
(35, '127.0.0.1', 'bbbaaaaa', '1da7db23498acd4b9569315435b17a85:dxABNc4JcZrQXWixnZK1ozER9V2RV3Y9', NULL, 'admin@vns.com', NULL, NULL, NULL, NULL, 1465447139, NULL, NULL, NULL, NULL, NULL, NULL, 'ZWI2MTVlNTdiY2NmZTI3NzRiOGFlMmRmYTNkMGI5NjBjYzI2MmI2MA'),
(37, '127.0.0.1', 'YWRjNDkxZTBkYjM0Y2ViNzUxNDEwYTk5ZDNiZjQxOTBiODliNWZlNA', 'c11d9c3e6f604a3015d7ae266cf19264:LdeQWxOAwvqmExbitTqBox69PncJfqqZ', NULL, NULL, NULL, NULL, NULL, NULL, 1465457866, NULL, NULL, NULL, NULL, NULL, NULL, 'YWRjNDkxZTBkYjM0Y2ViNzUxNDEwYTk5ZDNiZjQxOTBiODliNWZlNA'),
(38, '127.0.0.1', 'Y2Y1ZjJkMDI0MTM4YmM0M2MwMTQ0YTU5YmNlZmNkMzgxOWM4MzY5Ng', '2866ca01ef5174e7c688ca5f0817e92b:TeEbLXTQJsh9VZedF5oEcJnBYYLIcmSL', NULL, NULL, NULL, NULL, NULL, NULL, 1465457961, NULL, NULL, NULL, NULL, NULL, NULL, 'Y2Y1ZjJkMDI0MTM4YmM0M2MwMTQ0YTU5YmNlZmNkMzgxOWM4MzY5Ng'),
(39, '127.0.0.1', 'NzJjYTZiNjc5N2FjNGYxMThkODEwYmVkM2Q1N2Y3ODJlNzg2MzhiYg', '091fa91f3c9888904d5ab97ad71b136a:oEjwirCEnObcCUSflXd3VxdOAfNQQOEA', NULL, NULL, NULL, NULL, NULL, NULL, 1465457966, NULL, NULL, NULL, NULL, NULL, NULL, 'NzJjYTZiNjc5N2FjNGYxMThkODEwYmVkM2Q1N2Y3ODJlNzg2MzhiYg'),
(40, '127.0.0.1', 'NDU4NzIxMjczYzlkYjZjMmMxNzYwZGI4M2FhZjk4ODE2YzBjMjZjMQ', 'f013be2f6eb3fd25a42c84df670fb056:77PaH4oz5pWS2AZ08AiiAsokoBl8iZaV', NULL, NULL, NULL, NULL, NULL, NULL, 1465458481, NULL, NULL, NULL, NULL, NULL, NULL, 'NDU4NzIxMjczYzlkYjZjMmMxNzYwZGI4M2FhZjk4ODE2YzBjMjZjMQ'),
(41, '127.0.0.1', 'ZDkzZGU3MWM2NDc4NWU3YjgzZmJkY2FlYjE1MTkzZDE2YzMxNmVlZA', 'fe5a0963c5619de7ddfa5ef268f4220e:aGBAf4ij2uxpfHuzJzt6euPzHLYcjJIj', NULL, NULL, NULL, NULL, NULL, NULL, 1465458647, NULL, NULL, NULL, NULL, NULL, NULL, 'ZDkzZGU3MWM2NDc4NWU3YjgzZmJkY2FlYjE1MTkzZDE2YzMxNmVlZA'),
(42, '127.0.0.1', 'MzJlOTcwZWIzMTk1NzU5N2RkYTVjNGMyY2IxYzc3MjU1ZTY0MmU2ZQ', '657c7756e81eac1ca8bd6cdc95186aba:YqRuKj5nGbOAAZpFx5agrwnIX6gLvuwI', NULL, NULL, NULL, NULL, NULL, NULL, 1465458650, NULL, NULL, NULL, NULL, NULL, NULL, 'MzJlOTcwZWIzMTk1NzU5N2RkYTVjNGMyY2IxYzc3MjU1ZTY0MmU2ZQ'),
(43, '127.0.0.1', 'N2UwYzcxY2QxMGI5ZWRlOTcyZmZlYTRlZmZlZmZmOTA3ZjI2OTI3Nw', '4e2fbf0041ecd418d63eeb60b69606ed:yg4qXNpPo0TPjemsvZyMLqZceXH3hvpv', NULL, NULL, NULL, NULL, NULL, NULL, 1465458650, NULL, NULL, NULL, NULL, NULL, NULL, 'N2UwYzcxY2QxMGI5ZWRlOTcyZmZlYTRlZmZlZmZmOTA3ZjI2OTI3Nw'),
(44, '127.0.0.1', 'YzIyOWNlYWQ0MGQxZWYwMmZhYjgzOTk3YWZiY2Q0MWUxMWIxMTAwMw', '3d646ae9b7484753b699b080428ccca9:WJn7rydJbo8507Y6Bacep9dy7slZ7eby', NULL, NULL, NULL, NULL, NULL, NULL, 1465458651, NULL, NULL, NULL, NULL, NULL, NULL, 'YzIyOWNlYWQ0MGQxZWYwMmZhYjgzOTk3YWZiY2Q0MWUxMWIxMTAwMw'),
(45, '127.0.0.1', 'MjVhN2MyOGI3MThhYjk2ODU4NDFlODAwM2Q1NDBiYzY2ZTg0MDFlZQ', '08393e609948a5daa5118bcbeb52787b:t1NxLpGKURW8qqMI9su9wAl3yNxUH4Id', NULL, NULL, NULL, NULL, NULL, NULL, 1465458652, NULL, NULL, NULL, NULL, NULL, NULL, 'MjVhN2MyOGI3MThhYjk2ODU4NDFlODAwM2Q1NDBiYzY2ZTg0MDFlZQ'),
(46, '127.0.0.1', 'YzdhNDk5ODdiMzhhNzZhNWE5YWYyNzkxMTRjOThlYjZhOGRhOWY5Mw', 'f0b9fe4fd82f060c302f50bff743f954:D45T9xFNEnLA00lVk5QmZScKMS6gKysJ', NULL, NULL, NULL, NULL, NULL, NULL, 1465458652, NULL, NULL, NULL, NULL, NULL, NULL, 'YzdhNDk5ODdiMzhhNzZhNWE5YWYyNzkxMTRjOThlYjZhOGRhOWY5Mw'),
(47, '127.0.0.1', 'Y2FiYjc0YzdkNTBlNWY0ZGY2ZGZkNmMwYjNlNzA1ZjZlNTE5YWE2YQ', '7bca2821cca0e86b6ef1a134df151e1f:CJTgzPfo2XNLNIxdMGhSefhzLbCcA5A9', NULL, NULL, NULL, NULL, NULL, NULL, 1465458653, NULL, NULL, NULL, NULL, NULL, NULL, 'Y2FiYjc0YzdkNTBlNWY0ZGY2ZGZkNmMwYjNlNzA1ZjZlNTE5YWE2YQ'),
(48, '127.0.0.1', 'NmI3MWQzOWE0YjI5ZTIyMGU4Y2E2YjZjYzJkNGVmYjM2NjUzMzdkZQ', '556f168d3504d355e9c0161432c59a8e:F0hTRRZ4zzoK3nL0Cm49T0cuPMgDVYtn', NULL, NULL, NULL, NULL, NULL, NULL, 1465458654, NULL, NULL, NULL, NULL, NULL, NULL, 'NmI3MWQzOWE0YjI5ZTIyMGU4Y2E2YjZjYzJkNGVmYjM2NjUzMzdkZQ'),
(49, '127.0.0.1', 'OWVhMWVkMTAwNmM4OTM1ZDI0YmRmZTYzZTE5NTQxNjEwNzE0YWI0Ng', 'beae41fcf83b17b818a4cdb2851090a2:O2Vfa1FKMPFW7QnLd4lQlWKFGZiTbtm1', NULL, NULL, NULL, NULL, NULL, NULL, 1465458654, NULL, NULL, NULL, NULL, NULL, NULL, 'OWVhMWVkMTAwNmM4OTM1ZDI0YmRmZTYzZTE5NTQxNjEwNzE0YWI0Ng'),
(50, '127.0.0.1', 'NjdhOWNjZDE0NTE5MmY0ZDdiNGU0OWUyYjE1ZDkwMGUwNzAzMTU5YQ', '40551e440dea48c2a9f7eadf56b63509:3isdrrPCsZ1VIIT2rcSEAPrw6X3XXUuF', NULL, NULL, NULL, NULL, NULL, NULL, 1465458655, NULL, NULL, NULL, NULL, NULL, NULL, 'NjdhOWNjZDE0NTE5MmY0ZDdiNGU0OWUyYjE1ZDkwMGUwNzAzMTU5YQ'),
(51, '127.0.0.1', 'ZTQ0YzlmZGQ2Y2NlZmY4YzM2ZWJkMWY0Y2RlYWU5ZDUwNjBhZTdjOA', 'd4a7888c2448a3ccf093a2aa013eec90:cXOPqkph7ZKNT2J6SHqkpcPS93djWSid', NULL, NULL, NULL, NULL, NULL, NULL, 1465458655, NULL, NULL, NULL, NULL, NULL, NULL, 'ZTQ0YzlmZGQ2Y2NlZmY4YzM2ZWJkMWY0Y2RlYWU5ZDUwNjBhZTdjOA'),
(52, '127.0.0.1', 'NjNmNmNlZDNmMjNkMWM5ODJhNzk3OGJmNzA2NDRkNDA2NjNlOGYyZQ', '05dba5f7a03dd7b848de6d3aa837b189:aibsBq3xTVV6ExZYxjJOTKBfPbcrUNJ1', NULL, NULL, NULL, NULL, NULL, NULL, 1465458708, NULL, NULL, NULL, NULL, NULL, NULL, 'NjNmNmNlZDNmMjNkMWM5ODJhNzk3OGJmNzA2NDRkNDA2NjNlOGYyZQ'),
(53, '127.0.0.1', 'NzdmMzRlMDcyNTM0NzliZmUxMTMyZGVlY2MyODhkMjg0MzFjN2E1Mg', 'd9bfc09eef49ba26d5a1013843f58727:cY7SjurZmEuKVor0Vhz7uBDXUyKe4cXZ', NULL, NULL, NULL, NULL, NULL, NULL, 1465458709, NULL, NULL, NULL, NULL, NULL, NULL, 'NzdmMzRlMDcyNTM0NzliZmUxMTMyZGVlY2MyODhkMjg0MzFjN2E1Mg'),
(54, '127.0.0.1', 'MTM0MGVmNjE1NmU5ZGFjNDkzYTRlZmFlMDRmOGZmNTBiOWI4NDY4YQ', '1d525cae0a474c37f3a0ae6ea62683ff:uKziF9NNGTbqsGqVdpKY5WrrOuOhFb2s', NULL, NULL, NULL, NULL, NULL, NULL, 1465458710, NULL, NULL, NULL, NULL, NULL, NULL, 'MTM0MGVmNjE1NmU5ZGFjNDkzYTRlZmFlMDRmOGZmNTBiOWI4NDY4YQ'),
(55, '127.0.0.1', 'NzgyYTliZWI4NmUxNmJjMjkzZTU0N2Q5MWU5YmY5ODQ3ZDAwYjhjZg', '8a70233b7e7abf6bf3a571fec806801a:XpAGTZkXugqPOhA6BdlrljYyfTV5O19M', NULL, NULL, NULL, NULL, NULL, NULL, 1465458711, NULL, NULL, NULL, NULL, NULL, NULL, 'NzgyYTliZWI4NmUxNmJjMjkzZTU0N2Q5MWU5YmY5ODQ3ZDAwYjhjZg'),
(56, '127.0.0.1', 'Njc1ODkxYjVhYzhmMTFiMDczYmFmYTMwMjhmZDgwNDQ5YmNkMDk1Ng', '40234766b44aa0e698936dc3d1083f8c:Y4xXEgO8Es40ex6IxGHcomFP9yAxvHen', NULL, NULL, NULL, NULL, NULL, NULL, 1465458714, NULL, NULL, NULL, NULL, NULL, NULL, 'Njc1ODkxYjVhYzhmMTFiMDczYmFmYTMwMjhmZDgwNDQ5YmNkMDk1Ng'),
(57, '127.0.0.1', 'MWM4MWJlMDFlYzAzNTNmZDU1YTVlNTE5YjQwNjQ5M2EyZjQwOGNhNw', 'b343f14e0e7749e5bd742ec68ad3a4fa:pwcvF8c2UoBZIQduiouTlgo4LjfkQPuT', NULL, NULL, NULL, NULL, NULL, NULL, 1465458716, NULL, NULL, NULL, NULL, NULL, NULL, 'MWM4MWJlMDFlYzAzNTNmZDU1YTVlNTE5YjQwNjQ5M2EyZjQwOGNhNw'),
(58, '127.0.0.1', 'ZTk0YmRmYjA3ZGViNGFlOGEwNDZkNTEzYjE2NGIyMmY5MzdlNjFiNg', 'ae3658a4129c9a4485d00b66f6d14e43:wG8aSdkt5IophMTMgT7Dk7Wf6o03Kh0C', NULL, NULL, NULL, NULL, NULL, NULL, 1465458724, NULL, NULL, NULL, NULL, NULL, NULL, 'ZTk0YmRmYjA3ZGViNGFlOGEwNDZkNTEzYjE2NGIyMmY5MzdlNjFiNg'),
(59, '127.0.0.1', 'ZWNjMjI0ZDNlMmU0ZGVlYWMzN2E5MmMwZTUxZTFmNTJlNzNiNDkwNQ', 'bc361c0719b370b1036fb2e14f6ecf05:g1eijaamgpLZ6byz0hDXPCJLFdmHq431', NULL, NULL, NULL, NULL, NULL, NULL, 1465458732, NULL, NULL, NULL, NULL, NULL, NULL, 'ZWNjMjI0ZDNlMmU0ZGVlYWMzN2E5MmMwZTUxZTFmNTJlNzNiNDkwNQ'),
(60, '127.0.0.1', 'ZGU0ZmYyODJkNDljZGJjZmZhYWViZTM0YWZkYjMyY2RiNThkZGJjYw', 'bf9fda56ff13fef36c86e30014b03448:aV8fjc3Y8r7habdm6Pt8CnjWtPz6PhF2', NULL, NULL, NULL, NULL, NULL, NULL, 1465458733, NULL, NULL, NULL, NULL, NULL, NULL, 'ZGU0ZmYyODJkNDljZGJjZmZhYWViZTM0YWZkYjMyY2RiNThkZGJjYw'),
(61, '127.0.0.1', 'ZGRjMDE0ZjU2OGE2MDgxZjUzMGFiMjNjNTFmZGIyOTRiNDRjYzc5ZQ', '2162ad607fddba71e2999f1dd4cdeade:PCUSzOFojMuKLDlEwrziHmsXyntm2VGr', NULL, NULL, NULL, NULL, NULL, NULL, 1465458825, NULL, NULL, NULL, NULL, NULL, NULL, 'ZGRjMDE0ZjU2OGE2MDgxZjUzMGFiMjNjNTFmZGIyOTRiNDRjYzc5ZQ'),
(62, '127.0.0.1', 'ZmU4ZTYwNjcxNTg0NWEzNzk3Mjg0YjhjZThiYzM5Nzg5NjVmNmE5OQ', '4d6d4fb767aef8e4bbbb5cfe1650bc24:5V1CNPMorm8uovhEOETDEYATVLHV7z6J', NULL, NULL, NULL, NULL, NULL, NULL, 1465458873, NULL, NULL, NULL, NULL, NULL, NULL, 'ZmU4ZTYwNjcxNTg0NWEzNzk3Mjg0YjhjZThiYzM5Nzg5NjVmNmE5OQ'),
(63, '127.0.0.1', 'ZWI0NzJjOWIwNzM0OGE2MjQ1OTM1OTBlMDc2ZjlmNDBmZjM5OGQwOA', '2d80d216f5edc2f516a3468345a406fa:MlFQJranCVzG4pxMXFiDPOhlMpDHjX1B', NULL, NULL, NULL, NULL, NULL, NULL, 1465459273, NULL, NULL, NULL, NULL, NULL, NULL, 'ZWI0NzJjOWIwNzM0OGE2MjQ1OTM1OTBlMDc2ZjlmNDBmZjM5OGQwOA'),
(64, '127.0.0.1', 'MDAzNmI1MmNlNzdlZjZkMjhjNmYxZDYyNzgyMjVmYzI5NjZlNjMyYQ', '383b8e1f4d385e23603c11e3e6fab31a:PgB6JtxQJTiiPRm3KAKdmM5Hz41BDe0a', NULL, NULL, NULL, NULL, NULL, NULL, 1465459278, NULL, NULL, NULL, NULL, NULL, NULL, 'MDAzNmI1MmNlNzdlZjZkMjhjNmYxZDYyNzgyMjVmYzI5NjZlNjMyYQ'),
(65, '127.0.0.1', 'scas', 'ca24ff194e3d30493560f14c9f0f6ea6:Z5XRLhFtZu8bngcyawQ68puVp1he1Er5', NULL, 'aaaa', NULL, NULL, NULL, NULL, 1465479706, NULL, NULL, NULL, NULL, NULL, NULL, 'N2FjMWEyNmQxZGE5NGU5ZjQyNjU3NDk4ZWNjNzU2N2FlMDVlOGEzZA'),
(66, '127.0.0.1', 'M2I5MTIxOTU0NjEyYjMwY2IwZTFhODVkZjliYzE4NzdkYjM2ZGM2Mw', '31840c12590113b2496f13d0975dbde9:G5QOKCrRhHRTlOCWkSxZdBfccTpuZnd0', NULL, NULL, NULL, NULL, NULL, NULL, 1465480358, NULL, NULL, NULL, NULL, NULL, NULL, 'M2I5MTIxOTU0NjEyYjMwY2IwZTFhODVkZjliYzE4NzdkYjM2ZGM2Mw'),
(67, '127.0.0.1', 'admin', '716cb97062041a3a563ec14de5e59057:T4lK2SkesFaDbRRBpdukFgXQPOyrcnUt', NULL, 'admin@vns.com', NULL, NULL, NULL, NULL, 1465483537, NULL, NULL, 'Can', 'Ngo', NULL, NULL, 'bc933b6c-3e06-4c18-83f4-485176a576bb');

-- --------------------------------------------------------

--
-- Table structure for table `users_devices`
--

CREATE TABLE `users_devices` (
  `id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `device_id` int(11) unsigned NOT NULL,
  `last_active` int(11) unsigned NOT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `count` int(11) unsigned NOT NULL DEFAULT '0',
  `project_id` int(11) unsigned NOT NULL,
  `device_identifiter` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users_devices`
--

INSERT INTO `users_devices` (`id`, `user_id`, `device_id`, `last_active`, `active`, `count`, `project_id`, `device_identifiter`) VALUES
(1, 38, 1, 1465458711, 1, 19, 1, NULL),
(2, 39, 1, 1465457966, 1, 1, 1, NULL),
(3, 40, 1, 1465458481, 1, 1, 1, NULL),
(4, 56, 2, 1465458716, 1, 2, 1, NULL),
(5, 58, 2, 1465459322, 1, 8, 2, 'aasa'),
(13, 66, 4, 1465480358, 1, 1, 3, 'aasa');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` int(11) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `devices`
--
ALTER TABLE `devices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fb_categories`
--
ALTER TABLE `fb_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fb_categories_videos`
--
ALTER TABLE `fb_categories_videos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_fb_categories_videos_categories` (`category_id`),
  ADD KEY `index_fb_categories_videos_video` (`video_id`);

--
-- Indexes for table `fb_videos`
--
ALTER TABLE `fb_videos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_fb_videos_users` (`created_by_id`),
  ADD KEY `index_fb_videos_users_update` (`updated_by_id`);

--
-- Indexes for table `ff_categories`
--
ALTER TABLE `ff_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_created_by_in` (`created_by_id`);

--
-- Indexes for table `ff_dialogues`
--
ALTER TABLE `ff_dialogues`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_inspector_id` (`inspector_id`),
  ADD KEY `index_updated_by_id` (`updated_by_id`) USING BTREE,
  ADD KEY `index_ff_dialogues_create_by_id` (`created_by_id`),
  ADD KEY `fk_categories_id_idx` (`category_id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nlp_languages`
--
ALTER TABLE `nlp_languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nlp_words`
--
ALTER TABLE `nlp_words`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nlp_words_index_value` (`value`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_created_by_id` (`created_by_id`);

--
-- Indexes for table `requests`
--
ALTER TABLE `requests`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_requests_users` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `index_users_username` (`username`) USING BTREE,
  ADD UNIQUE KEY `index_users_api_key` (`api_key`) USING BTREE;

--
-- Indexes for table `users_devices`
--
ALTER TABLE `users_devices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_users_devices_user` (`user_id`),
  ADD KEY `index_users_devices_device` (`device_id`),
  ADD KEY `index_users_devices_projects` (`project_id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `devices`
--
ALTER TABLE `devices`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `fb_categories`
--
ALTER TABLE `fb_categories`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `fb_categories_videos`
--
ALTER TABLE `fb_categories_videos`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `fb_videos`
--
ALTER TABLE `fb_videos`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ff_categories`
--
ALTER TABLE `ff_categories`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `ff_dialogues`
--
ALTER TABLE `ff_dialogues`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=336;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `nlp_languages`
--
ALTER TABLE `nlp_languages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=165;
--
-- AUTO_INCREMENT for table `nlp_words`
--
ALTER TABLE `nlp_words`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `requests`
--
ALTER TABLE `requests`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=68;
--
-- AUTO_INCREMENT for table `users_devices`
--
ALTER TABLE `users_devices`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `fb_categories_videos`
--
ALTER TABLE `fb_categories_videos`
  ADD CONSTRAINT `fk_fb_categories_videos_video` FOREIGN KEY (`video_id`) REFERENCES `fb_videos` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_fb_categories_videos_catetory` FOREIGN KEY (`category_id`) REFERENCES `fb_categories` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `fb_videos`
--
ALTER TABLE `fb_videos`
  ADD CONSTRAINT `fk_fb_videos_users_update` FOREIGN KEY (`updated_by_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_fb_videos_users_created` FOREIGN KEY (`created_by_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `ff_categories`
--
ALTER TABLE `ff_categories`
  ADD CONSTRAINT `fk_categories_created_by` FOREIGN KEY (`created_by_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `ff_dialogues`
--
ALTER TABLE `ff_dialogues`
  ADD CONSTRAINT `fk_categories_id` FOREIGN KEY (`category_id`) REFERENCES `ff_categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_created_by` FOREIGN KEY (`created_by_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_dialogues_updated_by` FOREIGN KEY (`updated_by_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_inspector_id` FOREIGN KEY (`inspector_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `projects`
--
ALTER TABLE `projects`
  ADD CONSTRAINT `fk_created_by_id` FOREIGN KEY (`created_by_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `requests`
--
ALTER TABLE `requests`
  ADD CONSTRAINT `fk_requests_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users_devices`
--
ALTER TABLE `users_devices`
  ADD CONSTRAINT `fk_users_devices_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_users_devices_devices` FOREIGN KEY (`device_id`) REFERENCES `devices` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_users_devices_projects` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_group` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`),
  ADD CONSTRAINT `fk_users_groups_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
