-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Jun 07, 2016 at 10:13 AM
-- Server version: 5.5.42
-- PHP Version: 7.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dev_apps`
--

-- --------------------------------------------------------

--
-- Table structure for table `ff_categories`
--

CREATE TABLE `ff_categories` (
  `id` int(11) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created_by_id` int(11) unsigned DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `icon_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `recycled` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ff_categories`
--

INSERT INTO `ff_categories` (`id`, `name`, `description`, `created_by_id`, `created_on`, `icon_url`, `recycled`) VALUES
(2, 'Alimentation', NULL, 21, 1465098138, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue1.gif', 0),
(3, 'Premiers contacts :', NULL, 21, 1465098138, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/sejour.gif', 0),
(4, 'Accueil client : ', NULL, 21, 1465098138, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-3.gif', 0),
(5, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-4.gif', NULL, 21, 1465098139, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-4.gif', 0),
(6, 'AIDER SON PROCHAIN', NULL, 21, 1465098139, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-5.gif', 0),
(7, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-6.gif', NULL, 21, 1465098139, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-6.gif', 0),
(8, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-7.gif', NULL, 21, 1465098139, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-7.gif', 0),
(9, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-8.gif', NULL, 21, 1465098139, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-8.gif', 0),
(10, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-9.gif', NULL, 21, 1465098139, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-9.gif', 0),
(11, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-10.gif', NULL, 21, 1465098139, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-10.gif', 0),
(12, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-11.gif', NULL, 21, 1465098139, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-11.gif', 0),
(13, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-12.gif', NULL, 21, 1465098139, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-12.gif', 0),
(14, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-13.gif', NULL, 21, 1465098139, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-13.gif', 0),
(15, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-14.gif', NULL, 21, 1465098139, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-14.gif', 0),
(16, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-15.gif', NULL, 21, 1465098139, 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2014/07/dialogue-15.gif', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ff_dialogues`
--

CREATE TABLE `ff_dialogues` (
  `id` int(11) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `icon_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `audio_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `inspector_id` int(11) unsigned DEFAULT NULL,
  `recycled` tinyint(1) DEFAULT '0',
  `updated_on` int(11) unsigned DEFAULT NULL,
  `updated_by_id` int(11) unsigned DEFAULT NULL,
  `approved` tinyint(1) DEFAULT '0',
  `category_id` int(11) unsigned DEFAULT NULL,
  `content_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by_id` int(11) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=168 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ff_dialogues`
--

INSERT INTO `ff_dialogues` (`id`, `name`, `content`, `icon_url`, `audio_url`, `created_on`, `inspector_id`, `recycled`, `updated_on`, `updated_by_id`, `approved`, `category_id`, `content_url`, `created_by_id`) VALUES
(1, 'à la boulangerie', 'Employé : Monsieur bonjour.\r\n\r\nClient : Bonjour, je voudrais trois croissants au beurre, un pain aux raisins et un pain au chocolat, s’il vous plaît.\r\n\r\nEmployé : Oui, ça sera tout ?\r\n\r\nClient : Qu’est-ce que vous avez comme tartes ?\r\n\r\nEmployé : J’ai des tartes aux pommes ou des tartes aux fraises.\r\n\r\nClient : Je vais prendre une tarte aux fraises.\r\n\r\nEmployé : Oui c’est pour combien de personnes ?\r\n\r\nClient : Pour six personnes.\r\n\r\nEmployé : Voilà monsieur. 25 euros cinquante.\r\n\r\nClient : Voilà.\r\n\r\nEmployé : Merci monsieur, au revoir.\r\n\r\nClient : Merci, bonne journée.', 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2008/03/boulangerie.jpg', '', 1465098138, NULL, 0, 1465145025, 21, 0, 2, 'http://www.podcastfrancaisfacile.com/dialogue/2008/03/la-boulangerie-1.html', 21),
(2, 'à la boulangerie 2/2', 'Client : Bonjour, monsieur.\r\n\r\nEmployé : Bonjour, monsieur, vous désirez ?\r\n\r\nClient : Je vais prendre deux baguettes et quatre pains au chocolat.\r\n\r\nEmployé : Voilà, vous désirez autre chose ?\r\n\r\nClient : Oui, vous avez des croissants aux amandes ?\r\n\r\nEmployé : Oui monsieur, vous en voulez combien ?\r\n\r\nClient : Deux, s’il vous plaît.\r\n\r\nEmployé : Alors, deux baguettes, quatre pains au chocolat et deux croissants aux amandes. 8 euros 20, s’il vous plaît.\r\n\r\nClient : Voilà.\r\n\r\nEmployé : Merci, monsieur.\r\n\r\nClient : Merci, monsieur. Au revoir.', 'http://www.podcastfrancaisfacile.com/wp-content/uploads/2015/02/boulangerie.jpg', 'http://www.podcastfrancaisfacile.com/wp-content/uploads/files/boulangerie2.mp3', 1465098138, NULL, 0, 1465144265, 21, 0, 2, 'http://www.podcastfrancaisfacile.com/dialogue/2015/02/acheter-du-pain-a-la-boulangerie.html', 21),
(3, 'à la boucherie', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 2, 'http://www.podcastfrancaisfacile.com/dialogue/2008/04/bonjour-bienven-1.html', 21),
(4, 'au marché', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 2, 'http://www.podcastfrancaisfacile.com/podcast/2013/05/au-marche-faire-ses-courses-dialogue-fle-francais-apprendre-learn.html', 21),
(5, 'chez le fromager 1/3', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 2, 'http://www.podcastfrancaisfacile.com/podcast/2014/02/chez-le-fromager-13.html', 21),
(6, 'chez le fromager 2/3', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 2, 'http://www.podcastfrancaisfacile.com/podcast/2014/02/chez-le-fromager-23.html', 21),
(7, 'chez le fromager 3/3', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 2, 'http://www.podcastfrancaisfacile.com/communication/2014/02/dialogue-chez-le-fromager-33.html', 21),
(8, 'chez le volailler', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 2, 'http://www.podcastfrancaisfacile.com/dialogue/2015/07/poulet-de-bresse.html', 21),
(9, 'au café', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 2, 'http://www.podcastfrancaisfacile.com/podcast/2008/03/au-caf-1.html', 21),
(10, 'à la poste', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 2, 'http://www.podcastfrancaisfacile.com/podcast/2013/05/dialogue-a-la-poste.html', 21),
(11, 'à la gare', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 2, 'http://www.podcastfrancaisfacile.com/podcast/2013/05/a-la-gare.html', 21),
(12, 'à la pharmacie', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 2, 'http://www.podcastfrancaisfacile.com/podcast/2008/04/la-pharmacie-1.html', 21),
(13, 'prendre le taxi', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 2, 'http://www.podcastfrancaisfacile.com/podcast/2014/11/prendre-le-taxi.html', 21),
(14, 'situations diverses', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 2, 'http://www.podcastfrancaisfacile.com/podcast/2014/09/situations-diverses.html', 21),
(15, 'à la bijouterie (hésiter)', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 2, 'http://www.podcastfrancaisfacile.com/podcast/2013/09/hesiter-dans-une-bijouterie.html', 21),
(16, 'acheter une cravate', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 2, 'http://www.podcastfrancaisfacile.com/podcast/2014/10/marchand-de-vetements-achat-dune-cravate.html', 21),
(17, 'Qui est-ce ? 1/2', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 3, 'http://www.podcastfrancaisfacile.com/podcast/2014/07/stage-linguistique-en-france-qui-est-ce-1.html', 21),
(18, 'Qui est-ce ? 2/2', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 3, 'http://www.podcastfrancaisfacile.com/debutant/2015/03/qui-est-ce-dialogue-2.html', 21),
(19, 'à l’aéroport', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 3, 'http://www.podcastfrancaisfacile.com/podcast/2014/03/sejour-en-france-premiere-rencontre-de-la-famille-daccueil.html', 21),
(20, 'la visite de la chambre 1/3', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 3, 'http://www.podcastfrancaisfacile.com/debutant/2015/08/dialogue-francais-pour-debutant.html', 21),
(21, 'la visite de la chambre 2/3', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 3, 'http://www.podcastfrancaisfacile.com/dialogue/2015/08/apprendre-le-francais-visite-de-la-chambre-2.html', 21),
(22, 'famille d’accueil', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 3, 'http://www.podcastfrancaisfacile.com/podcast/2014/03/chez-la-famille-daccueil-premiere-rencontre.html', 21),
(23, 'à l’école de langue 1/2', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 3, 'http://www.podcastfrancaisfacile.com/podcast/2014/07/rencontre-comment-adresser-la-parole-a-quelque-un.html', 21),
(24, 'à l’école de langue 2/2', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 3, 'http://www.podcastfrancaisfacile.com/podcast/2014/07/rencontre-2.html', 21),
(25, 'questions au prof', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 3, 'http://www.podcastfrancaisfacile.com/podcast/2014/07/poser-des-questions-au-professeur.html', 21),
(26, 'demander un service', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 3, 'http://www.podcastfrancaisfacile.com/podcast/2014/07/demander-un-service-en-classe.html', 21),
(27, '50 phrases pour communiquer en cours de langue', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 3, 'http://www.podcastfrancaisfacile.com/podcast/2013/03/50phrases.html', 21),
(28, 'parler de projets', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 3, 'http://www.podcastfrancaisfacile.com/podcast/2014/08/projet-pour-le-lendemain.html', 21),
(29, 'sortie avec l’école', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 3, 'http://www.podcastfrancaisfacile.com/podcast/2014/08/sortie-avec-lecole.html', 21),
(30, 'après la sortie', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 3, 'http://www.podcastfrancaisfacile.com/podcast/2014/08/apres-une-sortie.html', 21),
(31, 'devoirs (les sigles)', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 3, 'http://www.podcastfrancaisfacile.com/dialogue/2015/09/sigles-et-acronymes.html', 21),
(32, 'mission', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 4, 'http://www.podcastfrancaisfacile.com/dialogue/2015/05/accueil-client-dialogue-1-7.html', 21),
(33, 'conseiller un collègue', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 4, 'http://www.podcastfrancaisfacile.com/dialogue/2015/05/accueil-client-27.html', 21),
(34, 'le programme', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 4, 'http://www.podcastfrancaisfacile.com/dialogue/2015/05/accueil-client-37.html', 21),
(35, ' à l&rsquo;aéroport', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 4, 'http://www.podcastfrancaisfacile.com/dialogue/2015/05/accueil-client-47.html', 21),
(36, 'dans le taxi', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 4, 'http://www.podcastfrancaisfacile.com/dialogue/2015/05/accueil-client-57-francais-des-affaires.html', 21),
(37, 'après la réunion', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 4, 'http://www.podcastfrancaisfacile.com/dialogue/2015/06/accueil-client.html', 21),
(38, 'le compte rendu', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 4, 'http://www.podcastfrancaisfacile.com/dialogue/2015/06/accueil-client-77-francais-des-affaires.html', 21),
(39, 'au fastfood', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 4, 'http://www.podcastfrancaisfacile.com/podcast/2014/11/entretien-dembauche-au-fastfood.html', 21),
(40, 'cours particuliers', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 4, 'http://www.podcastfrancaisfacile.com/podcast/2014/12/entretien-dembauche-par-telephone-pour-des-cours-particuliers.html', 21),
(41, ' job au camping', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 4, 'http://www.podcastfrancaisfacile.com/podcast/2014/12/entretien-dembauche-pour-un-job-dans-un-camping.html', 21),
(42, 'compte rendu', '', '', '', 1465098138, NULL, 0, NULL, NULL, 0, 4, 'http://www.podcastfrancaisfacile.com/podcast/2014/11/au-bureau-compte-rendu.html', 21),
(43, 'vendanges', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 4, 'http://www.podcastfrancaisfacile.com/podcast/2014/01/premier-jour-de-vendanges.html', 21),
(44, 'entre le directeur et la secrétaire', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 4, 'http://www.podcastfrancaisfacile.com/podcast/2014/08/discussion-entre-directeur-et-secretaire-1.html', 21),
(45, 'attente confirmation 1/2 ', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 4, 'http://www.podcastfrancaisfacile.com/podcast/2014/12/remplacement-12-attente-de-confirmation.html', 21),
(46, 'confirmation 2/2', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 4, 'http://www.podcastfrancaisfacile.com/communication/2015/01/nourrice-paris.html', 21),
(47, 'bricoler', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 4, 'http://www.podcastfrancaisfacile.com/podcast/2014/11/brico-macho.html', 21),
(48, 'le pot de départ', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 4, 'http://www.podcastfrancaisfacile.com/dialoguer/2015/01/affaires-pot-de-depart.html', 21),
(49, 'la livraison de boudin', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 4, 'http://www.podcastfrancaisfacile.com/dialogue/2015/01/la-livraison-de-boudin.html', 21),
(50, 'louer une voiture', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 5, 'http://www.podcastfrancaisfacile.com/podcast/2014/01/a-lagence-de-location.html', 21),
(51, 'louer un appartement', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 5, 'http://www.podcastfrancaisfacile.com/podcast/2012/01/dialogue-location.html', 21),
(52, 'visiter une maison', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 5, 'http://www.podcastfrancaisfacile.com/podcast/2014/02/visite-dune-maison.html', 21),
(53, 'louer des vélos 1/3', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 5, 'http://www.podcastfrancaisfacile.com/podcast/2014/09/louer-des-velos-se-renseigner.html', 21),
(54, 'louer des vélos 2/3', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 5, 'http://www.podcastfrancaisfacile.com/podcast/2014/09/louer-un-velo-23-au-moment-de-la-location.html', 21),
(55, 'louer des vélos 3/3', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 5, 'http://www.podcastfrancaisfacile.com/podcast/2014/10/louer-un-velo-33-au-moment-du-retour.html', 21),
(56, 'accueil', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 6, 'http://www.podcastfrancaisfacile.com/podcast/2014/03/recevoir-un-ami-pour-la-premiere-fois-accueil.html', 21),
(57, 'apéritif', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 6, 'http://www.podcastfrancaisfacile.com/podcast/2014/03/dialogue-a-laperitif.html', 21),
(58, 'pendant le repas', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 6, 'http://www.podcastfrancaisfacile.com/podcast/2014/03/inviter-recevoir-des-amis-pendant-le-repas.html', 21),
(59, 'dire au revoir', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 6, 'http://www.podcastfrancaisfacile.com/podcast/2014/03/invitation-chez-des-amis-au-moment-de-dire-au-revoir.html', 21),
(60, 'avant le dîner', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 6, 'http://www.podcastfrancaisfacile.com/dialogue/2015/07/diner-entre-amis.html', 21),
(61, ' la grève &#8211; proposer son aide', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 6, 'http://www.podcastfrancaisfacile.com/dialogue/2015/08/solidarite-dialogue-fle.html', 21),
(62, ' ', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 6, 'http://www.podcastfrancaisfacile.com/dialogue/2015/08/solidarite-dialogue-fle.html', 21),
(63, 'la grève (informer)', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 6, 'http://www.podcastfrancaisfacile.com/dialogue/2015/08/la-greve-systeme-d.html', 21),
(64, ' ', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 6, 'http://www.podcastfrancaisfacile.com/dialogue/2015/08/solidarite-dialogue-fle.html', 21),
(65, 'Système D 2/3 (marchandage)', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 6, 'http://www.podcastfrancaisfacile.com/dialogue/2015/08/dialogue-en-francais-marchandage.html', 21),
(66, 'Système D 3/3', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 6, 'http://www.podcastfrancaisfacile.com/dialogue/2015/08/systeme-d-33-accident.html', 21),
(67, 'à la poste', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 7, 'http://www.podcastfrancaisfacile.com/podcast/2013/11/protester.html', 21),
(68, 'crotte de chien', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 7, 'http://www.podcastfrancaisfacile.com/podcast/2014/01/crotte-de-chien.html', 21),
(69, 'dans un restaurant', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 7, 'http://www.podcastfrancaisfacile.com/dialogue/2015/02/se-plaindre-du-service-dans-un-restaurant-au-moment-de-regler-laddition.html', 21),
(70, 'à propos d&rsquo;un ami', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 7, 'http://www.podcastfrancaisfacile.com/podcast/2006/08/retard.html', 21),
(71, 'au service après-vente (SAV)', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 7, 'http://www.podcastfrancaisfacile.com/dialogue/2015/06/service-apres-vente-sav.html', 21),
(72, 'un voisin bruyant', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 7, 'http://www.podcastfrancaisfacile.com/dialogue/2015/06/voisin-trop-bruyant.html', 21),
(73, 'vandalisme', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 7, 'http://www.podcastfrancaisfacile.com/dialogue/2015/04/vandalisme.html', 21),
(74, 'la grève', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 7, 'http://www.podcastfrancaisfacile.com/dialogue/2015/08/manifester-son-mecontentement-dialogue-fle.html', 21),
(75, 'centre sportif', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 8, 'http://www.podcastfrancaisfacile.com/podcast/2014/02/se-renseigner-aupres-dun-centre-sportif.html', 21),
(76, 'à l&rsquo;office de tourisme', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 8, 'http://www.podcastfrancaisfacile.com/podcast/2014/11/office-de-tourisme-de-rochefort.html', 21),
(77, 'demander son chemin dans la rue', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 8, 'http://www.podcastfrancaisfacile.com/podcast/2013/05/demander-son-chemin-dans-la-rue.html', 21),
(78, 'demander sa route', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 8, 'http://www.podcastfrancaisfacile.com/podcast/2013/05/sur-la-route.html', 21),
(79, 'au commissariat : vol', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 8, 'http://www.podcastfrancaisfacile.com/podcast/2014/11/au-commissariat.html', 21),
(80, 'renseignement cours d&rsquo;anglais', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 8, 'http://www.podcastfrancaisfacile.com/podcast/2014/11/renseignement-apprendre-langlais-cours-danglais.html', 21),
(81, 'le test d&rsquo;évaluation &#8211; cours d&rsquo;anglais', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 8, 'http://www.podcastfrancaisfacile.com/podcast/2014/11/le-test-devaluation-anglais.html', 21),
(82, 'différence entre acheter &amp; s’acheter', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 8, 'http://www.podcastfrancaisfacile.com/podcast/2013/08/difference-entre-acheter-et-sacheter.html', 21),
(83, 'à propos d’un travail', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 8, 'http://www.podcastfrancaisfacile.com/podcast/2013/06/recherche-travail-france.html', 21),
(84, 'demander une aide &#8211; expliquer le fonctionnement d’une machine', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 8, 'http://www.podcastfrancaisfacile.com/podcast/2013/07/demander-une-aider-expliquer-francais-french-dialogue.html', 21),
(85, 'demander une aide &#8211; imprécisions', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 8, 'http://www.podcastfrancaisfacile.com/podcast/2014/12/imprecisions-demander-de-laide.html', 21),
(86, 'la séance photo (féliciter)', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 8, 'http://www.podcastfrancaisfacile.com/podcast/2014/07/seance-photo-imperatif-complimenter.html', 21),
(87, 'horoscope (expression du temps)', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 8, 'http://www.podcastfrancaisfacile.com/podcast/2013/12/-lhoroscope-lexpression-du-temps.html', 21),
(88, 'parler de son appartement', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 8, 'http://www.podcastfrancaisfacile.com/podcast/2013/12/parler-du-lieu-ou-on-vit-expression-de-lieu-en-francais-fle.html', 21),
(89, 'parler de son appartement', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 8, 'http://www.podcastfrancaisfacile.com/podcast/2013/12/parler-du-lieu-ou-on-vit-expression-de-lieu-en-francais-fle.html', 21),
(90, 'astuce pour démouler un cake', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 8, 'http://www.podcastfrancaisfacile.com/dialogue/2015/06/comment-demouler-un-cake-premier-ou-second-degre.html', 21),
(91, 'conseiller un site touristique', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 8, 'http://www.podcastfrancaisfacile.com/dialogue/2015/04/mont-saint-michel.html', 21),
(92, 'la chasse aux champignons', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 8, 'http://www.podcastfrancaisfacile.com/dialogue/2015/10/chasse-aux-champignons.html', 21),
(93, 'laisser un message informel sur répondeur ', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 9, 'http://www.podcastfrancaisfacile.com/texte/2015/11/laisser-un-message-informel-comprendre-un-message.html', 21),
(94, 'laisser un message', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 9, 'http://www.podcastfrancaisfacile.com/podcast/2013/11/comment-repondre-au-telephone-telephoner-laisser-un-message-francais.html', 21),
(95, 'prendre rendez-vous pour une urgence', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 9, 'http://www.podcastfrancaisfacile.com/podcast/2014/10/prendre-un-rendez-vous-pour-une-urgence.html', 21),
(96, 'prendre rendez-vous par téléphone', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 9, 'http://www.podcastfrancaisfacile.com/dialogue/2015/04/prendre-rendez-vous-par-telephone.html', 21),
(97, 'le portefeuille oublié', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 9, 'http://www.podcastfrancaisfacile.com/podcast/2014/12/le-portefeuille-oublie.html', 21),
(98, 'un oubli', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 10, 'http://www.podcastfrancaisfacile.com/podcast/2013/09/dialogue-learn-french-conversation-free.html', 21),
(99, 'étourdi &#8211; demander de l&rsquo;aide', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 10, 'http://www.podcastfrancaisfacile.com/podcast/2014/07/etourdi-humour-chercher-trouver-un-objet-demander.html', 21),
(100, 'dépêchons-nous ! (entre une mère et son fils)', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 10, 'http://www.podcastfrancaisfacile.com/podcast/2013/11/depechons-nous-forme-imperative-francais.html', 21),
(101, 'changement de vêtement', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 10, 'http://www.podcastfrancaisfacile.com/podcast/2014/06/changement-de-vetement.html', 21),
(102, 'la liste de courses 1', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 10, 'http://www.podcastfrancaisfacile.com/podcast/2013/10/la-liste-de-course.html', 21),
(103, 'la liste de courses 2', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 10, 'http://www.podcastfrancaisfacile.com/podcast/2014/03/la-liste-de-courses-2-.html', 21),
(104, 'le retour des courses', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 10, 'http://www.podcastfrancaisfacile.com/podcast/2014/03/retour-des-courses.html', 21),
(105, 'à propos d&rsquo;un changement de comportement (subjonctif)', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 10, 'http://www.podcastfrancaisfacile.com/podcast/2014/02/changement-de-comportement-expression-du-doute-subjonctif.html', 21),
(106, 'malade', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 10, 'http://www.podcastfrancaisfacile.com/podcast/2014/08/malade.html', 21),
(107, 'gueule de bois', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 10, 'http://www.podcastfrancaisfacile.com/podcast/2014/08/gueule-de-bois.html', 21),
(108, 'dispute autour d&rsquo;un film', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 10, 'http://www.podcastfrancaisfacile.com/podcast/2014/02/dispute-autour-dun-film.html', 21),
(109, 'dispute cadeau d&rsquo;anniversaire', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 10, 'http://www.podcastfrancaisfacile.com/podcast/2014/10/donner-son-avis-cadeau-quon-naime-pas.html', 21),
(110, 'les devoirs', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 10, 'http://www.podcastfrancaisfacile.com/podcast/2014/11/les-devoirs-pronom-en-y.html', 21),
(111, 'le cadeau d&rsquo;anniversaire : ironie', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 10, 'http://www.podcastfrancaisfacile.com/podcast/2014/11/cadeau-danniversaire-ironie.html', 21),
(112, 'la leçon de piano', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 10, 'http://www.podcastfrancaisfacile.com/grammaire/2015/01/a-propos-du-piano-pronom-en.html', 21),
(113, 'expliquer comment on s’est blessé', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 11, 'http://www.podcastfrancaisfacile.com/podcast/2013/06/chez-le-docteur.html', 21),
(114, 'problème à la gorge', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 11, 'http://www.podcastfrancaisfacile.com/podcast/2014/09/chez-le-medecin.html', 21),
(115, 'blessure grave', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 11, 'http://www.podcastfrancaisfacile.com/podcast/2014/10/blessure-grave-dialogue-fle.html', 21),
(116, 'problème de tension', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 11, 'http://www.podcastfrancaisfacile.com/podcast/2014/10/probleme-de-tension.html', 21),
(117, 'tour de reins', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 11, 'http://www.podcastfrancaisfacile.com/podcast/2014/10/dialogue-chez-le-docteur-fle-tour-de-reins.html', 21),
(118, 'tour de reins', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 11, 'http://www.podcastfrancaisfacile.com/podcast/2014/10/dialogue-chez-le-docteur-fle-tour-de-reins.html', 21),
(119, 'mal à l&rsquo;estomac', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 11, 'http://www.podcastfrancaisfacile.com/dialogue/2016/05/dialogue-chez-docteur.html', 21),
(120, 'à la pharmacie (champignons)', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 11, 'http://www.podcastfrancaisfacile.com/dialogue/2015/11/verification-champignon-pharmacie.html', 21),
(121, 'réserver une table au restaurant', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 12, 'http://www.podcastfrancaisfacile.com/podcast/2013/08/reserver-une-table-au-restaurant.html', 21),
(122, 'modifier une réservation (au restaurant)', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 12, 'http://www.podcastfrancaisfacile.com/podcast/2013/08/modifier-une-reservation-au-restaurant.html', 21),
(123, 'prendre rendez-vous chez le coiffeur', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 12, 'http://www.podcastfrancaisfacile.com/podcast/2014/03/prendre-rendez-vous-chez-le-coiffeur.html', 21),
(124, 'entre amis (inviter / insister / refuser)', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 12, 'http://www.podcastfrancaisfacile.com/podcast/2013/06/inviter-quelquun-insister-refuser.html', 21),
(125, 'à propos d’une sortie (proposer / refuser / insister)', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 12, 'http://www.podcastfrancaisfacile.com/podcast/2013/06/a-propos-dune-sortie-proposer-refuser-accepter.html', 21),
(126, ' situation informelle', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 12, 'http://www.podcastfrancaisfacile.com/dialogue/2015/07/fixer-un-rendez-vous-2.html', 21),
(127, 'enquête pour donner son avis (adverbes de fréquence)', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 13, 'http://www.podcastfrancaisfacile.com/podcast/2013/12/dialogue-enquete-loisir-adverbes-de-frequence.html', 21),
(128, 'interview d’un joueur de tennis (la durée au présent)', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 13, 'http://www.podcastfrancaisfacile.com/podcast/2013/12/la-duree-au-present-faire-du-sport.html', 21),
(129, 'interview : expatriation 1', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 13, 'http://www.podcastfrancaisfacile.com/podcast/2014/03/expatriation-interview.html', 21),
(130, 'interview : expatriation 2', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 13, 'http://www.podcastfrancaisfacile.com/dialogue/2015/02/travailler-au-canada.html', 21),
(131, 'interview à la brocante', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 13, 'http://www.podcastfrancaisfacile.com/podcast/2011/06/la-brocante-interview.html', 21),
(132, 'manifestation contre l&rsquo;austérité', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 13, 'http://www.podcastfrancaisfacile.com/dialogue/2015/03/manifestation-contre-lausterite.html', 21),
(133, 'rencontre avec le professeur', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 14, 'http://www.podcastfrancaisfacile.com/podcast/2014/02/rencontre-avec-le-professeur-des-ecoles.html', 21),
(134, 'suivi scolaire', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 14, 'http://www.podcastfrancaisfacile.com/podcast/2014/10/avec-le-professeur-des-ecoles-suivi-scolaire.html', 21),
(135, 'problème de comportement en classe', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 14, 'http://www.podcastfrancaisfacile.com/podcast/2014/02/probleme-de-comportement-en-classe.html', 21),
(136, 'interdictions', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 14, 'http://www.podcastfrancaisfacile.com/podcast/2013/11/interdictions.html', 21),
(137, 'entre le directeur et un professeur (les indéfinis 1/2)', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 14, 'http://www.podcastfrancaisfacile.com/podcast/2013/12/au-lycee-entre-le-directeur-et-un-professeur-les-indefinis-1-2.html', 21),
(138, 'discussion avec le professeur de math', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 14, 'http://www.podcastfrancaisfacile.com/non-classe/2015/07/discussion-avec-le-professeur-de-maths.html', 21),
(139, 'parler de sa région', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 15, 'http://www.podcastfrancaisfacile.com/podcast/2013/10/parler-de-sa-region-.html', 21),
(140, 'parler de ses origines', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 15, 'http://www.podcastfrancaisfacile.com/podcast/2013/10/dialogue-en-francais-parler-de-sa-region-1.html', 21),
(141, 'parler de son week-end', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 15, 'http://www.podcastfrancaisfacile.com/podcast/2013/11/parler-de-son-week-end.html', 21),
(142, 'parler des vacances : situation 1', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 15, 'http://www.podcastfrancaisfacile.com/podcast/2013/11/parler-de-ses-vacances.html', 21),
(143, 'parler des vacances : situation 2', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 15, 'http://www.podcastfrancaisfacile.com/podcast/2013/12/parler-de-ses-vacances-2.html', 21),
(144, 'parler de deux personnes (comparer)', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 15, 'http://www.podcastfrancaisfacile.com/podcast/2014/01/comparer-comparaison-dialogue.html', 21),
(145, 'échanges (quelqu&rsquo;un / quelque chose)', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 15, 'http://www.podcastfrancaisfacile.com/podcast/2013/12/echanges-quelquun-personne.html', 21),
(146, 'la nouvelle stagiaire', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 15, 'http://www.podcastfrancaisfacile.com/podcast/2014/01/francais-authentique-dialogue-gratuit-fle-la-nouvelle-stagiaire.html', 21),
(147, 'prise de poids', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 15, 'http://www.podcastfrancaisfacile.com/dialogue/2015/01/faire-un-regime.html', 21),
(148, 'parler de son régime', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 15, 'http://www.podcastfrancaisfacile.com/dialogue/2015/01/reussir-son-regime.html', 21),
(149, 'enquête pour le cours de français', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 16, 'http://www.podcastfrancaisfacile.com/podcast/2013/12/donner-son-avis-adverbe-adjectif.html', 21),
(150, 'parler d’un film', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 16, 'http://www.podcastfrancaisfacile.com/podcast/2013/12/parler-dun-film.html', 21),
(151, 'cinéma (spectateur mécontent)', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 16, 'http://www.podcastfrancaisfacile.com/podcast/2014/02/a-la-sortie-du-film-spectateurs-plutot-mecontents.html', 21),
(152, 'cinéma (spectateur satisfait)', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 16, 'http://www.podcastfrancaisfacile.com/podcast/2014/02/a-la-sortie-du-cinema-spectateurs-satisfaits.html', 21),
(153, 'à propos des élections', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 16, 'http://www.podcastfrancaisfacile.com/podcast/2014/06/parler-de-politique-en-cours-de-fle-francais-langue-etrangere-dialogue.html', 21),
(154, 'le verre de trop', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 16, 'http://www.podcastfrancaisfacile.com/podcast/2014/01/le-verre-de-trop.html', 21),
(155, 'rénovation appartement', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 16, 'http://www.podcastfrancaisfacile.com/podcast/2014/06/renovation-appartement.html', 21),
(156, 'misogyne', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 16, 'http://www.podcastfrancaisfacile.com/podcast/2014/12/misogyne-dialogue-intermediaire-francais.html', 21),
(157, 'parler politique', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 16, 'http://www.podcastfrancaisfacile.com/podcast/2014/11/politique-liberal-vs-socialiste.html', 21),
(158, 'changement de poste', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 16, 'http://www.podcastfrancaisfacile.com/podcast/2014/11/donner-son-opinion-dialogue-fle.html', 21),
(159, 'parler d&rsquo;un roman', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 16, 'http://www.podcastfrancaisfacile.com/dialogue/2015/02/parler-dun-roman-policier.html', 21),
(160, 'parler d&rsquo;un roman 2', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 16, 'http://www.podcastfrancaisfacile.com/dialogue/2015/03/dialogue-fle-parler-dun-livre.html', 21),
(161, 'salaire joueurs de foot', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 16, 'http://www.podcastfrancaisfacile.com/dialogue/2015/06/prix-des-transferts-au-football.html', 21),
(162, 'travail le dimanche 1/3', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 16, 'http://www.podcastfrancaisfacile.com/dialogue/2015/04/travail-le-dimanche.html', 21),
(163, 'travail le dimanche 2/3', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 16, 'http://www.podcastfrancaisfacile.com/dialogue/2015/04/travail-le-dimanche-23.html', 21),
(164, 'travail le dimanche 3/3', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 16, 'http://www.podcastfrancaisfacile.com/dialogue/2015/04/travail-le-dimanche-avis-des-commercants-33.html', 21),
(165, 'intéressé/intéressant', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 16, 'http://www.podcastfrancaisfacile.com/dialogue/2015/04/interesse-interessant-sinteresser-a.html', 21),
(166, 'l&rsquo;uniforme à l&rsquo;école', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 16, 'http://www.podcastfrancaisfacile.com/dialogue/2015/08/uniforme-ecole.html', 21),
(167, 'la réforme de l&rsquo;orthographe', '', '', '', 1465098139, NULL, 0, NULL, NULL, 0, 16, 'http://www.podcastfrancaisfacile.com/dialogue/2016/02/la-reforme-de-lorthographe.html', 21);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` int(11) unsigned NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User');

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) unsigned NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `nlp_languages`
--

CREATE TABLE `nlp_languages` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=165 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `nlp_languages`
--

INSERT INTO `nlp_languages` (`id`, `name`, `code`) VALUES
(1, 'Abkhazian', 'ab'),
(2, 'Afar', 'aa'),
(3, 'Afrikaans', 'af'),
(4, 'Albanian', 'sq'),
(5, 'Amharic', 'am'),
(6, 'Arabic', 'ar'),
(7, 'Aragonese', 'an'),
(8, 'Armenian', 'hy'),
(9, 'Assamese', 'as'),
(10, 'Aymara', 'ay'),
(11, 'Azerbaijani', 'az'),
(12, 'Bashkir', 'ba'),
(13, 'Basque', 'eu'),
(14, 'Bengali(Bangla)', 'bn'),
(15, 'Bhutani', 'dz'),
(16, 'Bihari', 'bh'),
(17, 'Bislama', 'bi'),
(18, 'Breton', 'br'),
(19, 'Bulgarian', 'bg'),
(20, 'Burmese', 'my'),
(21, 'Byelorussian(Belarusian)', 'be'),
(22, 'Cambodian', 'km'),
(23, 'Catalan', 'ca'),
(24, 'Cherokee', NULL),
(25, 'Chewa', NULL),
(26, 'Chinese', 'zh'),
(27, 'Chinese(Simplified)', 'zh-Hans'),
(28, 'Chinese(Traditional)', 'zh-Hant'),
(29, 'Corsican', 'co'),
(30, 'Croatian', 'hr'),
(31, 'Czech', 'cs'),
(32, 'Danish', 'da'),
(33, 'Divehi', NULL),
(34, 'Dutch', 'nl'),
(35, 'Edo', NULL),
(36, 'English', 'en'),
(37, 'Esperanto', 'eo'),
(38, 'Estonian', 'et'),
(39, 'Faeroese', 'fo'),
(40, 'Farsi', 'fa'),
(41, 'Fiji', 'fj'),
(42, 'Finnish', 'fi'),
(43, 'Flemish', NULL),
(44, 'French', 'fr'),
(45, 'Frisian', 'fy'),
(46, 'Fulfulde', NULL),
(47, 'Galician', 'gl'),
(48, 'Gaelic(Scottish)', 'gd'),
(49, 'Gaelic(Manx)', 'gv'),
(50, 'Georgian', 'ka'),
(51, 'German', 'de'),
(52, 'Greek', 'el'),
(53, 'Greenlandic', 'kl'),
(54, 'Guarani', 'gn'),
(55, 'Gujarati', 'gu'),
(56, 'HaitianCreole', 'ht'),
(57, 'Hausa', 'ha'),
(58, 'Hawaiian', NULL),
(59, 'Hebrew', 'he'),
(60, 'Hindi', 'hi'),
(61, 'Hungarian', 'hu'),
(62, 'Ibibio', NULL),
(63, 'Icelandic', 'is'),
(64, 'Ido', 'io'),
(65, 'Igbo', NULL),
(66, 'Indonesian', 'id'),
(67, 'Interlingua', 'ia'),
(68, 'Interlingue', 'ie'),
(69, 'Inuktitut', 'iu'),
(70, 'Inupiak', 'ik'),
(71, 'Irish', 'ga'),
(72, 'Italian', 'it'),
(73, 'Japanese', 'ja'),
(74, 'Javanese', 'jv'),
(75, 'Kannada', 'kn'),
(76, 'Kanuri', NULL),
(77, 'Kashmiri', 'ks'),
(78, 'Kazakh', 'kk'),
(79, 'Kinyarwanda(Ruanda)', 'rw'),
(80, 'Kirghiz', 'ky'),
(81, 'Kirundi(Rundi)', 'rn'),
(82, 'Konkani', NULL),
(83, 'Korean', 'ko'),
(84, 'Kurdish', 'ku'),
(85, 'Laothian', 'lo'),
(86, 'Latin', 'la'),
(87, 'Latvian(Lettish)', 'lv'),
(88, 'Limburgish(Limburger)', 'li'),
(89, 'Lingala', 'ln'),
(90, 'Lithuanian', 'lt'),
(91, 'Macedonian', 'mk'),
(92, 'Malagasy', 'mg'),
(93, 'Malay', 'ms'),
(94, 'Malayalam', 'ml'),
(95, 'Maltese', 'mt'),
(96, 'Maori', 'mi'),
(97, 'Marathi', 'mr'),
(98, 'Moldavian', 'mo'),
(99, 'Mongolian', 'mn'),
(100, 'Nauru', 'na'),
(101, 'Nepali', 'ne'),
(102, 'Norwegian', 'no'),
(103, 'Occitan', 'oc'),
(104, 'Oriya', 'or'),
(105, 'Oromo(AfaanOromo)', 'om'),
(106, 'Papiamentu', NULL),
(107, 'Pashto(Pushto)', 'ps'),
(108, 'Polish', 'pl'),
(109, 'Portuguese', 'pt'),
(110, 'Punjabi', 'pa'),
(111, 'Quechua', 'qu'),
(112, 'Rhaeto-Romance', 'rm'),
(113, 'Romanian', 'ro'),
(114, 'Russian', 'ru'),
(115, 'Sami(Lappish)', NULL),
(116, 'Samoan', 'sm'),
(117, 'Sangro', 'sg'),
(118, 'Sanskrit', 'sa'),
(119, 'Serbian', 'sr'),
(120, 'Serbo-Croatian', 'sh'),
(121, 'Sesotho', 'st'),
(122, 'Setswana', 'tn'),
(123, 'Shona', 'sn'),
(124, 'SichuanYi', 'ii'),
(125, 'Sindhi', 'sd'),
(126, 'Sinhalese', 'si'),
(127, 'Siswati', 'ss'),
(128, 'Slovak', 'sk'),
(129, 'Slovenian', 'sl'),
(130, 'Somali', 'so'),
(131, 'Spanish', 'es'),
(132, 'Sundanese', 'su'),
(133, 'Swahili(Kiswahili)', 'sw'),
(134, 'Swedish', 'sv'),
(135, 'Syriac', NULL),
(136, 'Tagalog', 'tl'),
(137, 'Tajik', 'tg'),
(138, 'Tamazight', NULL),
(139, 'Tamil', 'ta'),
(140, 'Tatar', 'tt'),
(141, 'Telugu', 'te'),
(142, 'Thai', 'th'),
(143, 'Tibetan', 'bo'),
(144, 'Tigrinya', 'ti'),
(145, 'Tonga', 'to'),
(146, 'Tsonga', 'ts'),
(147, 'Turkish', 'tr'),
(148, 'Turkmen', 'tk'),
(149, 'Twi', 'tw'),
(150, 'Uighur', 'ug'),
(151, 'Ukrainian', 'uk'),
(152, 'Urdu', 'ur'),
(153, 'Uzbek', 'uz'),
(154, 'Venda', NULL),
(155, 'Vietnamese', 'vi'),
(156, 'Volapük', 'vo'),
(157, 'Wallon', 'wa'),
(158, 'Welsh', 'cy'),
(159, 'Wolof', 'wo'),
(160, 'Xhosa', 'xh'),
(161, 'Yi', NULL),
(162, 'Yiddish', 'yi'),
(163, 'Yoruba', 'yo'),
(164, 'Zulu', 'zu');

-- --------------------------------------------------------

--
-- Table structure for table `nlp_words`
--

CREATE TABLE `nlp_words` (
  `id` int(11) unsigned NOT NULL,
  `value` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `language` varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'en'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `created_by_id` int(11) unsigned DEFAULT NULL,
  `created_on` int(11) NOT NULL,
  `project_icon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `recycled` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `name`, `description`, `created_by_id`, `created_on`, `project_icon`, `recycled`) VALUES
(1, 'Từ điển Pháp-Anh-Việt', 'Tra cứu từ điển 3 trong 1, hỗ trợ phát âm, tra tu online', 17, 1465047911, 'https://itunes.apple.com/vn/app/mytour-hanoi/id574151408?mt=8', 1),
(2, 'Francais Facile', 'AAAAA', 17, 1465047917, 'http://web.vicnisoft.local/index.php/project/add', 0),
(3, 'Francais Facile', 'sacas', NULL, 1456137124, 'scac', 1),
(4, 'sascsa', 'saca', NULL, 1456137144, 'sacasc', 1),
(5, 'ascascas', 'I installed and configured the ejabberd2 server on my ubuntu server and i mod_muc is enabled by default, but i can''t able to group chat through the XMPP Client App AstraChat in Android and iOS Group Created successfully, but the group members doesn''t receive the messages I searched google for this, i found that muc_admin mod is required, when i enabled it and restarted the ejabberd it shows ejabberd started, but it is not started\r\n\r\ncan anybody help me to figure out what is the problem with the configuration', NULL, 1456137226, 'cascas', 1),
(6, 'Câu lạc bộ Liv', 'Lay thong tin fanpage...', 21, 1465053353, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `api_key` varchar(128) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`, `api_key`) VALUES
(17, '127.0.0.1', 'admin3', 'df1b86a61733f85fc38f7b2041d563ea:ckFJKGPYrcLkmS9kgsfRChibPMIu6QS0', NULL, 'admin@admin.com', NULL, NULL, NULL, NULL, 1465029751, NULL, NULL, 'Victor', 'Ngo', 'Symfony', '0936423778', '8a23d86b-1eff-42ff-b917-33ccae872f67'),
(19, '127.0.0.1', 'ngocon', '21232f297a57a5a743894a0e4a801fc3', NULL, 'admin@admin.com', NULL, NULL, NULL, NULL, 1463028818, NULL, NULL, 'Can', 'Ngo', 'ADMIN', '093333333', NULL),
(20, '127.0.0.1', 'canngo', '87156cc579da80efc7a2d7627f0b7ca6:mZ5E0TaSXOTESrJiWXGrR04hpF5xxVuG', NULL, 'ngocongcan@gmail.com', NULL, NULL, NULL, NULL, 1464710895, NULL, NULL, 'Can', 'Ngo', 'Vicnisoft', '0936423778', NULL),
(21, '127.0.0.1', 'admin', '9351dff9f1074e21807bdcdbeee5e5dc:GIDjOhiVDy3stbKqQ5SSTjYcT5fXnzvN', NULL, 'admin@vns.com', NULL, NULL, NULL, NULL, 1464712730, NULL, NULL, 'Admin', 'Super', 'VNS', '0961177337', NULL),
(24, '127.0.0.1', 'mod', '551729db970507e71f6903a50b47058f:h5hOzeTde2ndMi23wIBA32yhxMu6AyD6', NULL, 'admin@vns.com', NULL, NULL, NULL, NULL, 1464749417, NULL, NULL, 'Can', 'Ngo', 'VNS', '0936423778', NULL),
(26, '127.0.0.1', 'ngocongcan', 'a17fbfd0963cfae4b76283ef397ce30f:vJpZ18g79yTwicKIJiQ8FlbunuBiYvXq', NULL, 'admin@vns.com', NULL, NULL, NULL, NULL, 1464888809, NULL, NULL, 'Can', 'Ngo', 'Symfony', NULL, '016bb1a5-f090-4d4f-8af4-40ba37f49a5b'),
(27, '127.0.0.1', 'admin1', 'd550b163d0c95c8e0db4d64040afdf58:e9emCCTamhnwLVwyaRvcZiwXNANMp8sM', NULL, 'admin@vns.com', NULL, NULL, NULL, NULL, 1464889810, NULL, NULL, 'Admin', '1', NULL, NULL, '04cf5c34-8c7a-4487-a9f0-5522bdf763d9');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` int(11) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ff_categories`
--
ALTER TABLE `ff_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_created_by_in` (`created_by_id`);

--
-- Indexes for table `ff_dialogues`
--
ALTER TABLE `ff_dialogues`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_inspector_id` (`inspector_id`),
  ADD KEY `index_updated_by_id` (`updated_by_id`) USING BTREE,
  ADD KEY `index_ff_dialogues_create_by_id` (`created_by_id`),
  ADD KEY `fk_categories_id_idx` (`category_id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nlp_languages`
--
ALTER TABLE `nlp_languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nlp_words`
--
ALTER TABLE `nlp_words`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nlp_words_index_value` (`value`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_created_by_id` (`created_by_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `index_users_username` (`username`) USING BTREE,
  ADD UNIQUE KEY `index_users_api_key` (`api_key`) USING BTREE;

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ff_categories`
--
ALTER TABLE `ff_categories`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `ff_dialogues`
--
ALTER TABLE `ff_dialogues`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=168;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `nlp_languages`
--
ALTER TABLE `nlp_languages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=165;
--
-- AUTO_INCREMENT for table `nlp_words`
--
ALTER TABLE `nlp_words`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `ff_categories`
--
ALTER TABLE `ff_categories`
  ADD CONSTRAINT `fk_categories_created_by` FOREIGN KEY (`created_by_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `ff_dialogues`
--
ALTER TABLE `ff_dialogues`
  ADD CONSTRAINT `fk_categories_id` FOREIGN KEY (`category_id`) REFERENCES `ff_categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_created_by` FOREIGN KEY (`created_by_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_dialogues_updated_by` FOREIGN KEY (`updated_by_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_inspector_id` FOREIGN KEY (`inspector_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `projects`
--
ALTER TABLE `projects`
  ADD CONSTRAINT `fk_created_by_id` FOREIGN KEY (`created_by_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_group` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`),
  ADD CONSTRAINT `fk_users_groups_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
