-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Jun 01, 2016 at 04:38 AM
-- Server version: 5.5.42
-- PHP Version: 7.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dev_apps`
--

-- --------------------------------------------------------

--
-- Table structure for table `ff_categories`
--

CREATE TABLE `ff_categories` (
  `id` int(11) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created_by_id` int(11) unsigned DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `icon_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `recycled` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ff_categories`
--

INSERT INTO `ff_categories` (`id`, `name`, `description`, `created_by_id`, `created_on`, `icon_url`, `recycled`) VALUES
(2, 'Category 1', 'csascsaca', 0, 1456192045, 'scacacsac ascsa', 1),
(3, 'AAAnn', 'I installed and configured the ejabberd2 server on my ubuntu server and i mod_muc is enabled by default, but i can''t able to group chat through the XMPP Client App AstraChat in Android and iOS Group Created successfully, but the group members doesn''t receive the messages I searched google for this, i found that muc_admin mod is required, when i enabled it and restarted the ejabberd it shows ejabberd started, but it is not started can anybody help me to figure out what is the problem with the configuration.', 0, 1456192192, 'csacaaaaaa', 1),
(4, 'asa', 'ascasc', 0, 1456454534, 'ascasc', 1),
(5, 'asca', 'acacasc', 0, 1456454539, 'casc', 1),
(6, 'sacascacasc', 'csacascacsa', 0, 1456454543, 'ascascasca', 1),
(7, 'sacas', 'sacasc', 0, 1456456638, 'ascasca', 1),
(8, 'BBB', 'sacasca', 0, 1456796025, 'asvas', 1),
(9, 'Les 7 règles de français authentique', 'Les 7 règles de français authentique', 0, 1457401748, '', 0),
(10, 'Le francais avec Victor', '', 0, 1457401748, '', 0),
(11, 'Politique', '', 0, 1457401748, '', 0),
(12, 'Fêtes', '', 0, 1457401748, '', 0),
(13, 'Bande dessinée', '', 0, 1457401748, '', 0),
(14, 'Société', '', 0, 1457401748, '', 0),
(15, 'Éducation', '', 0, 1457401748, '', 0),
(16, 'Livre audio', '', 0, 1457401748, '', 0),
(17, 'Faits divers', '', 0, 1457401748, '', 0),
(18, 'Divers', '', 0, 1457401748, '', 0),
(19, 'Sport', '', 0, 1457401748, '', 0),
(20, 'Technologie', '', 0, 1457401749, '', 0),
(21, 'Culture', '', 0, 1457401749, '', 0),
(22, 'Recettes de cusine', '', 0, 1457401749, '', 0),
(23, 'Alimentation', '', 0, 1457401749, '', 0),
(24, 'Se présenter', '', 0, 1457401749, '', 0),
(25, 'Villes & régions', '', 0, 1457401749, '', 0),
(26, 'Protester - se plaindre', '', 0, 1457401749, '', 0),
(27, 'Recevoir des invités', '', 0, 1457401749, '', 0),
(28, 'Location', '', 0, 1457401749, '', 0),
(29, 'Situations professionnelles', '', 0, 1457401749, '', 0),
(30, 'Entretien d’embauche', '', 0, 1457401749, '', 0),
(31, 'Accueil client', '', 0, 1457401749, '', 0),
(32, 'Avec la famille d’accueil', '', 0, 1457401749, '', 0),
(33, 'En classe', '', 0, 1457401749, '', 0),
(34, 'Premiers contacts', '', 0, 1457401749, '', 0),
(35, 'Vêtements & accessoires', '', 0, 1457401749, '', 0),
(36, 'Services, café, …', '', 0, 1457401749, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ff_dialogues`
--

CREATE TABLE `ff_dialogues` (
  `id` int(11) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `icon_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `audio_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `created_by_id` int(11) unsigned NOT NULL,
  `inspector_id` int(11) unsigned DEFAULT NULL,
  `recycled` tinyint(1) DEFAULT '0',
  `updated_on` int(11) unsigned DEFAULT NULL,
  `updated_by_id` int(11) unsigned DEFAULT NULL,
  `approved` tinyint(1) DEFAULT '0',
  `category_id` int(11) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ff_dialogues`
--

INSERT INTO `ff_dialogues` (`id`, `name`, `content`, `icon_url`, `audio_url`, `created_on`, `created_by_id`, `inspector_id`, `recycled`, `updated_on`, `updated_by_id`, `approved`, `category_id`) VALUES
(1, 'AAABBB', 'casc', 'http://web.vicnisoft.local/index.php/dialogue/add', 'http://web.vicnisoft.local/index.php/dialogue/add', 1456800350, 1, NULL, 1, 1456800815, 1, 0, 8),
(2, 'ACscasa', 'scsac', 'http://web.vicnisoft.local/index.php/dialogue/add', 'http://web.vicnisoft.local/index.php/dialogue/add', 1456800371, 1, NULL, 1, NULL, NULL, 0, 8),
(3, 'knk', 'sacsa', 'as,casc', ',;sa;sc,á', 1456852236, 1, NULL, 0, NULL, NULL, 0, 3),
(4, 'AAABBB', 'áccsa', 'http://web.vicnisoft.local/index.php/dialogue/add', 'http://web.vicnisoft.local/index.php/dialogue/add', 1457016778, 1, NULL, 0, 1457017498, 1, 0, 8);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` int(11) unsigned NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User');

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) unsigned NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `created_by_id` int(11) unsigned DEFAULT NULL,
  `created_on` int(11) NOT NULL,
  `project_icon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `recycled` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `name`, `description`, `created_by_id`, `created_on`, `project_icon`, `recycled`) VALUES
(1, 'Từ điển Pháp-Anh-Việt', 'Tra cứu từ điển 3 trong 1, hỗ trợ phát âm, tra tu online\r\n                                    ', NULL, 23456, 'https://itunes.apple.com/vn/app/mytour-hanoi/id574151408?mt=8', 1),
(2, 'Francais Facile', 'AAAAA', NULL, 1456136958, 'http://web.vicnisoft.local/index.php/project/add', 0),
(3, 'Francais Facile', 'sacas', NULL, 1456137124, 'scac', 1),
(4, 'sascsa', 'saca', NULL, 1456137144, 'sacasc', 1),
(5, 'ascascas', 'I installed and configured the ejabberd2 server on my ubuntu server and i mod_muc is enabled by default, but i can''t able to group chat through the XMPP Client App AstraChat in Android and iOS Group Created successfully, but the group members doesn''t receive the messages I searched google for this, i found that muc_admin mod is required, when i enabled it and restarted the ejabberd it shows ejabberd started, but it is not started\r\n\r\ncan anybody help me to figure out what is the problem with the configuration', NULL, 1456137226, 'cascas', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `api_key` varchar(128) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`, `api_key`) VALUES
(17, '127.0.0.1', 'admin@admin.com', 'password', NULL, 'admin@admin.com', NULL, NULL, NULL, NULL, 1456135905, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(19, '127.0.0.1', 'ngocongcan', '21232f297a57a5a743894a0e4a801fc3', NULL, 'admin@admin.com', NULL, NULL, NULL, NULL, 1463028818, NULL, NULL, 'Can', 'Ngo', 'ADMIN', '093333333', NULL),
(20, '127.0.0.1', 'canngo', '87156cc579da80efc7a2d7627f0b7ca6:mZ5E0TaSXOTESrJiWXGrR04hpF5xxVuG', NULL, 'ngocongcan@gmail.com', NULL, NULL, NULL, NULL, 1464710895, NULL, NULL, 'Can', 'Ngo', 'Vicnisoft', '0936423778', NULL),
(21, '127.0.0.1', 'admin', '9351dff9f1074e21807bdcdbeee5e5dc:GIDjOhiVDy3stbKqQ5SSTjYcT5fXnzvN', NULL, 'admin@vns.com', NULL, NULL, NULL, NULL, 1464712730, NULL, NULL, 'Admin', 'Super', 'VNS', '0961177337', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` int(11) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ff_categories`
--
ALTER TABLE `ff_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_created_by_in` (`created_by_id`);

--
-- Indexes for table `ff_dialogues`
--
ALTER TABLE `ff_dialogues`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_category_id` (`category_id`),
  ADD KEY `index_inspector_id` (`inspector_id`),
  ADD KEY `index_created_by_id` (`created_by_id`) USING BTREE,
  ADD KEY `index_updated_by_id` (`updated_by_id`) USING BTREE;

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_created_by_id` (`created_by_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `index_users_username` (`username`) USING BTREE,
  ADD UNIQUE KEY `index_users_api_key` (`api_key`) USING BTREE;

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ff_categories`
--
ALTER TABLE `ff_categories`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `ff_dialogues`
--
ALTER TABLE `ff_dialogues`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `ff_dialogues`
--
ALTER TABLE `ff_dialogues`
  ADD CONSTRAINT `fk_categories_id` FOREIGN KEY (`category_id`) REFERENCES `ff_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_inspector_id` FOREIGN KEY (`inspector_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `projects`
--
ALTER TABLE `projects`
  ADD CONSTRAINT `fk_created_by_id` FOREIGN KEY (`created_by_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_group` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`),
  ADD CONSTRAINT `fk_users_groups_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
