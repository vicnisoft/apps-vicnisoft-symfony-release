<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160706020941CreateTableSocial_Users extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("CREATE TABLE social_users (id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT, username VARCHAR (255)  NOT NULL, active INT(1) UNSIGNED NOT NULL DEFAULT 1, email VARCHAR (255) , social_id VARCHAR (255), avatar_url VARCHAR (255), social_type VARCHAR (25), user_id INT(11) UNSIGNED ,PRIMARY KEY (id) ) CHARACTER SET utf8 COLLATE utf8_unicode_ci");
        $this->addSql("ALTER TABLE social_users ADD INDEX index_social_users_users (user_id)");
        $this->addSql("ALTER TABLE social_users ADD CONSTRAINT fk_social_users_users FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE SET NULL ON UPDATE SET NULL ");


    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
