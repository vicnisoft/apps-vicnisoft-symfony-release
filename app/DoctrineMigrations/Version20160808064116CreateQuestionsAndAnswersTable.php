<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160808064116CreateQuestionsAndAnswersTable extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("CREATE TABLE questions_answers (id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT, questions TEXT  NOT NULL, active INT(1) UNSIGNED NOT NULL DEFAULT 1, updated_on INT(11) UNSIGNED,  created_on INT(11) UNSIGNED, answer BLOB, created_by INT(11) UNSIGNED, project_id INT(11) UNSIGNED  ,PRIMARY KEY (id) ) CHARACTER SET utf8 COLLATE utf8_unicode_ci");
        $this->addSql("ALTER TABLE questions_answers ADD INDEX index_questions_answers_users (created_by)");
        $this->addSql("ALTER TABLE questions_answers ADD INDEX index_questions_answers_projects (project_id)");
        $this->addSql("ALTER TABLE questions_answers ADD CONSTRAINT fk_questions_answers_users FOREIGN KEY (created_by) REFERENCES users(id) ON DELETE SET NULL ON UPDATE SET NULL ");
        $this->addSql("ALTER TABLE questions_answers ADD CONSTRAINT fk_questions_answers_projects FOREIGN KEY (project_id) REFERENCES projects(id) ON DELETE SET NULL ON UPDATE SET NULL ");

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
