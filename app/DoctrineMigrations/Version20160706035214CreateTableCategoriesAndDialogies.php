<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160706035214CreateTableCategoriesAndDialogies extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("CREATE TABLE categories (id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT, name VARCHAR (255)  NOT NULL, active INT(1) UNSIGNED NOT NULL DEFAULT 1, approved INT(11) UNSIGNED DEFAULT 0, description TEXT , icon_url VARCHAR (255), project_id INT(11) UNSIGNED, created_on INT(11) UNSIGNED, updated_on INT(11) UNSIGNED ,PRIMARY KEY (id) ) CHARACTER SET utf8 COLLATE utf8_unicode_ci");
        $this->addSql("ALTER TABLE categories ADD INDEX index_categories_projects (project_id)");
        $this->addSql("ALTER TABLE categories ADD CONSTRAINT fk_categories_projects FOREIGN KEY (project_id) REFERENCES projects(id) ON DELETE SET NULL ON UPDATE SET NULL ");

        $this->addSql("CREATE TABLE dialogues (id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT, name VARCHAR (255)  NOT NULL, active INT(1) UNSIGNED NOT NULL DEFAULT 1, approved INT(11) UNSIGNED DEFAULT 0, content TEXT , icon_url VARCHAR (255), content_url VARCHAR (255), media_url VARCHAR (255),  type VARCHAR (25), project_id INT(11) UNSIGNED , category_id INT(11) UNSIGNED ,created_on INT(11) UNSIGNED, updated_on INT(11) UNSIGNED,PRIMARY KEY (id) ) CHARACTER SET utf8 COLLATE utf8_unicode_ci");
        $this->addSql("ALTER TABLE dialogues ADD INDEX index_dialogues_projects (project_id)");
        $this->addSql("ALTER TABLE dialogues ADD INDEX index_dialogues_categories (category_id)");
        $this->addSql("ALTER TABLE dialogues ADD CONSTRAINT fk_dialogues_projects FOREIGN KEY (project_id) REFERENCES projects(id) ON DELETE SET NULL ON UPDATE SET NULL ");
        $this->addSql("ALTER TABLE dialogues ADD CONSTRAINT fk_dialogues_categories FOREIGN KEY (category_id) REFERENCES categories(id) ON DELETE CASCADE ON UPDATE CASCADE ");


    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
