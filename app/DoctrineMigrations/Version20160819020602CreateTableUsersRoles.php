<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160819020602CreateTableUsersRoles extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("CREATE TABLE roles (id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT, name VARCHAR (255)  NOT NULL, active INT(1) UNSIGNED DEFAULT FALSE,  created_on INT(11) UNSIGNED, updated_on INT(11) UNSIGNED ,PRIMARY KEY (id) ) CHARACTER SET utf8 COLLATE utf8_unicode_ci");

        $this->addSql("CREATE TABLE users_roles (id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT, user_id INT(11) UNSIGNED NOT NULL , role_id INT(11) UNSIGNED NOT NULL,  created_on INT(11) UNSIGNED, updated_on INT(11) UNSIGNED ,PRIMARY KEY (id) ) CHARACTER SET utf8 COLLATE utf8_unicode_ci");
        $this->addSql("ALTER TABLE users_roles ADD INDEX index_users_roles_users (user_id)");
        $this->addSql("ALTER TABLE users_roles ADD INDEX index_users_roles_roles (role_id)");

        $this->addSql("ALTER TABLE users_roles ADD CONSTRAINT fk_users_roles_users FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE ON UPDATE CASCADE ");
        $this->addSql("ALTER TABLE users_roles ADD CONSTRAINT fk_users_roles_roles FOREIGN KEY (role_id) REFERENCES roles(id) ON DELETE CASCADE ON UPDATE CASCADE ");


    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
