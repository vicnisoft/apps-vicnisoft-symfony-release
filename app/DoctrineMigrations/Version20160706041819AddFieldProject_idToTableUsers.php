<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160706041819AddFieldProject_idToTableUsers extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("ALTER TABLE users  ADD COLUMN project_id INT(11) UNSIGNED ");
        $this->addSql("ALTER TABLE users ADD INDEX index_users_projects (project_id)");
        $this->addSql("ALTER TABLE users ADD CONSTRAINT fk_users_projects FOREIGN KEY (project_id) REFERENCES projects(id) ON DELETE SET NULL ON UPDATE SET NULL ");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
