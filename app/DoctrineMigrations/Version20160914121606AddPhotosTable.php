<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160914121606AddPhotosTable extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("CREATE TABLE photos (id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT, path VARCHAR (255), name VARCHAR (255) ,target_id INT(11) UNSIGNED NOT NULL, target_table VARCHAR (255)  NOT NULL , active INT(1) UNSIGNED DEFAULT TRUE,  created_on INT(11) UNSIGNED, updated_on INT(11) UNSIGNED ,PRIMARY KEY (id) ) CHARACTER SET utf8 COLLATE utf8_unicode_ci");


    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
