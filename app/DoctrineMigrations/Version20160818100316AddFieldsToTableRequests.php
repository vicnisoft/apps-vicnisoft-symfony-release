<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160818100316AddFieldsToTableRequests extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("ALTER TABLE requests  ADD COLUMN country_code VARCHAR(255) ");
        $this->addSql("ALTER TABLE requests  ADD COLUMN country_name VARCHAR(255) ");
        $this->addSql("ALTER TABLE requests  ADD COLUMN region_code VARCHAR(255) ");
        $this->addSql("ALTER TABLE requests  ADD COLUMN region_name VARCHAR(255) ");
        $this->addSql("ALTER TABLE requests  ADD COLUMN zip_code VARCHAR(255) ");
        $this->addSql("ALTER TABLE requests  ADD COLUMN city VARCHAR(255) ");
        $this->addSql("ALTER TABLE requests  ADD COLUMN time_zone VARCHAR(255) ");
        $this->addSql("ALTER TABLE requests  ADD COLUMN latitude VARCHAR(255) ");
        $this->addSql("ALTER TABLE requests  ADD COLUMN longitude VARCHAR(255) ");
        $this->addSql("ALTER TABLE requests  ADD COLUMN ip_checked int(1) ");


    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
