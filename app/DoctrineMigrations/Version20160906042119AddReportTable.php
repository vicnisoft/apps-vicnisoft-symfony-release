<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160906042119AddReportTable extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("CREATE TABLE dialogues_reports (id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT, dialogue_id INT(11) UNSIGNED NOT NULL, user_id INT(11) UNSIGNED NOT NULL, reason VARCHAR (512) , active INT(1) UNSIGNED DEFAULT TRUE,  created_on INT(11) UNSIGNED, updated_on INT(11) UNSIGNED ,PRIMARY KEY (id) ) CHARACTER SET utf8 COLLATE utf8_unicode_ci");
        $this->addSql("ALTER TABLE dialogues_reports ADD INDEX index_dialogues_reports_users (user_id)");
        $this->addSql("ALTER TABLE dialogues_reports ADD INDEX index_dialogues_reports_dialogues (dialogue_id)");

        $this->addSql("ALTER TABLE dialogues_reports ADD CONSTRAINT fk_dialogues_reports_users FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE ON UPDATE CASCADE ");
        $this->addSql("ALTER TABLE dialogues_reports ADD CONSTRAINT fk_dialogues_reports_dialogues FOREIGN KEY (dialogue_id) REFERENCES dialogues(id) ON DELETE CASCADE ON UPDATE CASCADE ");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
