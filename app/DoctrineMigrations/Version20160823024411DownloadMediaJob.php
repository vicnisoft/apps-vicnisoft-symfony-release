<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160823024411DownloadMediaJob extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("CREATE TABLE download_media_job (id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT, target_id INT(11) UNSIGNED NOT NULL, target_table VARCHAR (255)  NOT NULL, name VARCHAR (255)  NOT NULL, local_url VARCHAR (255)  NOT NULL, remote_url VARCHAR (255)  NOT NULL, active INT(1) UNSIGNED DEFAULT TRUE,  created_on INT(11) UNSIGNED, updated_on INT(11) UNSIGNED ,PRIMARY KEY (id) ) CHARACTER SET utf8 COLLATE utf8_unicode_ci");

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
