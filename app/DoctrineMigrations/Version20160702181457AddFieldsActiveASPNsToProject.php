<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160702181457AddFieldsActiveASPNsToProject extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("ALTER TABLE projects  ADD COLUMN active tinyint (1) NOT NULL  DEFAULT 1");
        $this->addSql("ALTER TABLE projects  ADD COLUMN  pem_url_aspn VARCHAR (255)");
        $this->addSql("ALTER TABLE projects  ADD COLUMN  pass_pharse_aspn  VARCHAR (25)");

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
