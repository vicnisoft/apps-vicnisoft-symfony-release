<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160818082450AddFieldsToRequestTable extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("ALTER TABLE requests  ADD COLUMN request_uri VARCHAR(255) ");
        $this->addSql("ALTER TABLE requests ADD COLUMN client_ip VARCHAR(255)");
        $this->addSql("ALTER TABLE requests ADD COLUMN content TEXT");
        $this->addSql("ALTER TABLE requests  DROP COLUMN script_uri ");

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {


        // this down() migration is auto-generated, please modify it to your needs

    }
}
